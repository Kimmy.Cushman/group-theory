import numpy as np
import itertools
import os
from basis_change import change_basis

# FIXME this isnt really necessary. Writing the square roots nicely can be done in a neater way
OVERALL_FACTOR_ROUND = 5
factors_dict = {1: ''}
for denom in range(2, 1000):
    factor = 1/np.sqrt(denom)
    factors_dict[round(factor,OVERALL_FACTOR_ROUND)] = '\\frac{1}{\sqrt{' + str(denom) + '}}'
factors_dict[0.04767] = '\\frac{1}{\sqrt{440}}'
factors_dict[0.03740] = '\\frac{1}{\sqrt{715}}'

LATTICE_REP_DIMS = {'G1g_rep': 2,
                    'Hg_rep': 4,
                    'A1g_rep': 1,
                    'Eg_rep': 2,
                    'T1g_rep': 3,
                    'T2g_rep': 3}


def open_latex(f):
    f.write("\\documentclass[12pt]{article}\n")
    f.write("\\usepackage[margin=0.5in]{geometry}")
    f.write("\\usepackage{amsmath,amssymb,color,makeidx,url,wrapfig}\n")
    f.write("\\setlength{\\tabcolsep}{10pt} \n \\renewcommand{\\arraystretch}{1.5} \n ")
    f.write("\\begin{document}\n")
    f.write("\\begin{table}[] \\centering \\begin{tabular}{|c | c | c | c |}\n")
    f.write("\\hline\n")
    f.write("Irrep & row & choice & Operator \\\ \n")
    f.write("\\hline\n")


def close_latex(f):
    f.write("\\end{tabular}")
    f.write("\\end{table}")
    f.write("\\end{document}")


def check_consistent_size(rep, op_dict):
    nominal_num_rows = LATTICE_REP_DIMS[rep]
    fill_matrix = []
    for row_choice in op_dict.keys():
        row = int(row_choice.split(' ')[1])
        choice = int(row_choice.split(' ')[-1])
        try:
            fill_matrix[row].append(choice)
        except IndexError:
            fill_matrix.append([choice])
    num_rows = len(fill_matrix)
    if num_rows != nominal_num_rows:
        print('unexpected number of rows')

    num_choices = list(set([len(row) for row in fill_matrix]))
    if len(num_choices) != 1:
        print('inconsistent number of choices per row')
    return num_rows, max(num_choices)


def write_by_rows_latex_pdf(rep, ops_dict, isospin_rep, elemental_states, basis,
                            George_compare=False, basis_change='', filename='row_ops'):
    n_rows, n_choices = check_consistent_size(rep, ops_dict)
    print(n_rows)
    print(n_choices)
    with open(f'LaTex/Output/{filename}{basis}.tex', 'w+') as f:
        open_latex(f)
        for row in range(n_rows):
            for choice in range(n_choices):
                try:
                    op = ops_dict[f'row {row} choice {choice}']
                except KeyError:
                    op = {}
                if George_compare:  # compare to paper
                    op = get_George_op(op, isospin_rep)
                    # op = get_notes_op(op)  # compare to notes
                op = change_basis(op, isospin_rep, elemental_states, basis_change)
                print(f'row {row} choice {choice}')
                print(op)
                print()
                if row == 0 and choice == 0:
                    rep_string = rep.split('_')[0]
                else:
                    rep_string = ''
                if choice == 0:
                    row_string = f'row {row}'
                else:
                    row_string = ''
                f.write(f"{rep_string} & {row_string} &  {choice} & ${get_operator_latex(op)}$\\\ \n")
            f.write("\\hline\n")
        close_latex(f)
    os.system(f"open LaTex/Output/{filename}{basis}.tex -a TexShop")


def write_by_choice_latex_pdf(rep, ops_dict, filename):
    n_rows, n_choices = check_consistent_size(rep, ops_dict)
    with open(f'LaTex/Output/{filename}', 'w+') as f:
        open_latex(f)
        for choice in range(n_choices):
            for row in range(n_rows):
                op = ops_dict[f'row {row} choice {choice}']
                # op = get_notes_op(op)
                if choice == 0 and row == 0:
                    rep_string = rep.split('_')[0]
                else:
                    rep_string = ''
                if row == 0:
                    choice_string = f'choice {choice}'
                    print(choice_string)
                else:
                    choice_string = ''
                f.write(f"{rep_string} & {choice_string} &  row {row} & ${get_operator_latex(op)}$\\\ \n")
            f.write("\\hline\n")
        close_latex(f)
    os.system(f"open LaTex/Output/{filename} -a TexShop")


def get_George_op(op, isospin, add1=False):
    George_op = {}
    for elem, val in op.items():
        # (udu - duu)123 -> (udu - duu)(123 - 213)
        # (12)(udu - duu)(123 - 213) = (duu - udu)(213 - 123) GOOD
        # (13)(udu - duu)(123 - 213) = (udu - uud)(321 - 321) != (udu - duu)(123 - 213)
        # (13)(23)(13) = (12)
        if isospin == '123' or isospin == '1234':
            elem_symm_list = list(itertools.permutations(elem))  # with the repeats, want all permutations
            vals_list = [val]*len(elem_symm_list)
        elif isospin == '13_2':
            # (1 - 12)(1 + 13)
            a, b, c = elem
            elem_symm_list = [
                 # udu         - duu
                f'{a}{b}{c}', f'{b}{a}{c}',
                # sym uu of above
                f'{c}{b}{a}', f'{b}{c}{a}'
            ]
            vals_list = [val, -val, val, -val]
        elif isospin == '12_3':
            # (1 + 12)(1 - 13)
            a, b, c = elem
            elem_symm_list = [
                f'{a}{b}{c}', f'{a}{c}{b}', f'{c}{a}{b}',   # uud - udu - duu
                f'{b}{a}{c}', f'{b}{c}{a}', f'{c}{b}{a}']   # sym uu of above
            vals_list = [2*val, -val, -val, 2*val, -val, -val]

        elif isospin == '12_34':
            a, b, c, d = elem
            elem_symm_list = [
                # 2uudd              + 2dduu         - udud           - uddu         - duud           - dudu
                f'{a}{b}{c}{d}', f'{c}{d}{a}{b}', f'{a}{c}{b}{d}', f'{a}{c}{d}{b}', f'{c}{a}{b}{d}', f'{c}{a}{d}{b}',
                # symmetrize us (switch a's and b's)
                f'{b}{a}{c}{d}', f'{c}{d}{b}{a}', f'{b}{c}{a}{d}', f'{b}{c}{d}{a}', f'{c}{b}{a}{d}', f'{c}{b}{d}{a}',
                # symmetrize ds (switch c's and d's)
                f'{a}{b}{d}{c}', f'{d}{c}{a}{b}', f'{a}{d}{b}{c}', f'{a}{d}{c}{b}', f'{d}{a}{b}{c}', f'{d}{a}{c}{b}',
                # symmetrize us and ds (switch both)
                f'{b}{a}{d}{c}', f'{d}{c}{b}{a}', f'{b}{d}{a}{c}', f'{b}{d}{c}{a}', f'{d}{b}{a}{c}', f'{d}{b}{c}{a}',
            ]
            vals_list = [2, 2, -1, -1, -1, -1,
                         2, 2, -1, -1, -1, -1,
                         2, 2, -1, -1, -1, -1,
                         2, 2, -1, -1, -1, -1]
            vals_list = np.array(vals_list)*val
        elif isospin == '13_24':
            a, b, c, d = elem
            elem_symm_list = [
                # udud               - uddu          - duud            + dudu
                f'{a}{b}{c}{d}', f'{a}{b}{d}{c}', f'{b}{a}{c}{d}', f'{b}{a}{d}{c}',
                # symmetrize us (switch a's and c's)
                f'{c}{b}{a}{d}', f'{c}{b}{d}{a}', f'{b}{c}{a}{d}', f'{b}{c}{d}{a}',
                # symmetrize ds (switch b's and d's)
                f'{a}{d}{c}{b}', f'{a}{d}{b}{c}', f'{d}{a}{c}{b}', f'{d}{a}{b}{c}',
                # symmetrize us and ds (switch both)
                f'{c}{d}{a}{b}', f'{c}{d}{b}{a}', f'{d}{c}{a}{b}', f'{d}{c}{b}{a}',
            ]
            vals_list = [1, -1, -1, 1,
                         1, -1, -1, 1,
                         1, -1, -1, 1,
                         1, -1, -1, 1]
            vals_list = np.array(vals_list)*val
        else:
            return op
        for elem_symm, value in zip(elem_symm_list, vals_list):
            if add1:
                elem_george = ''.join([str(int(spin) + 1) for spin in elem_symm])
            elem_george = ''.join(elem_symm)
            if abs(val) > 1e-8:
                try:
                    George_op[elem_george] += value
                except KeyError:
                    George_op[elem_george] = value
    George_op_nonzero = {}
    for elem, val in George_op.items():
        if abs(val) > 1e-8:
            George_op_nonzero[elem] = val
    return George_op_nonzero


def get_notes_op(operator):
    notes_op = {}
    for elem, val in operator.items():
        new_elem = ''.join(str(int(s) + 1) for s in elem)
        notes_op[new_elem] = val
    return notes_op

def get_common_factor(operator):
    operator_normalized = {}
    norm = np.linalg.norm(list(operator.values()))
    for key, val in operator.items():
        if abs(val) > 1e-7:
            operator_normalized[key] = val/norm

    values_real = np.real(list(operator_normalized.values()))
    if len(values_real) == 0:
        return 0, 0
    if np.allclose(values_real, np.zeros(len(values_real))):
        values_real = np.imag(list(operator_normalized.values()))
    if values_real[0] < 0:
        negative = True
    else:
        negative = False
    overall_factor = min([abs(val) for val in values_real if abs(val) > 1e-8])
    factored_values = []
    for term, val in operator_normalized.items():
        if negative:
            val = - val
        operator_normalized[term] = val/overall_factor
        factored_values.append(val/overall_factor)
    factored_values_set = [1, 1j, 1+1j, 1-1j]
    common_factors = {}
    for factor in factored_values_set:
        common_factors[factor] = []
        for term, val in operator_normalized.items():
            final_factor = val/factor
            if final_factor == final_factor.real:
                common_factors[factor].append([term, final_factor])
    return overall_factor, common_factors


def get_value_string(value, first_term):
    if first_term:
        plus_string = ''
    else:
        plus_string = '+'

    val_real, val_imag = np.real(value), np.imag(value)
    round_real = round(val_real, 2)
    round_imag = round(val_imag, 2)

    if int(round_real) == round_real:
        round_real = int(round_real)

    if int(round_imag) == round_imag:
        round_imag = int(round_imag)

    if abs(round_real) == 1:
        real_string = '' # FIXME?
    else:
        real_string = str(abs(round_real))
    if abs(round_imag) == 1:
        imag_string = ''
    else:
        imag_string = str(abs(round_imag))


    # add real and imaginary
    if round_real == 1:
        if round_imag > 1e-8:
            val_string = f'{plus_string}({real_string} + {imag_string}i)'
        elif round_imag == 0.0:
            val_string = f'{plus_string}'
        else:
            val_string = f'{plus_string}({real_string} - {imag_string}i)'
    elif round_real > 1e-8:
        if round_imag > 1e-8:
            val_string = f'{plus_string}({real_string} + {imag_string}i)'
        elif round_imag == 0.0:
            val_string = f'{plus_string}{real_string}'
        else:
            val_string = f'{plus_string}({real_string} - {imag_string}i)'
    elif round_real == 0.0:
        if round_imag > 1e-8:
            val_string = f'{plus_string}{imag_string}i'
        elif round_imag == 0.0:
            val_string = f'{plus_string}0'
        else:
            val_string = f'- {imag_string}i'
    else:
        if round_imag > 1e-8:
            val_string = f'{plus_string}(- {real_string} + {imag_string}i)'
        elif round_imag == 0.0:
            val_string = f'- {real_string}'
        else:
            val_string = f'{plus_string}(- {real_string} - {imag_string}i)'

    return val_string


def get_operator_latex(operator):
    overall_factor, common_factors = get_common_factor(operator)
    if overall_factor == 0:
        return ''
    try:
        overall_factor_string = factors_dict[abs(round(overall_factor,OVERALL_FACTOR_ROUND))]
    except KeyError:
        overall_factor_string = str(round(overall_factor, 3))
    if overall_factor > 0:
        line = overall_factor_string
    else:
        line = '-' + overall_factor_string
    # if abs(overall_factor) > 1e-8:
    #     line += '\\Bigg('

    first_term_outer = True
    for factor, terms_list in common_factors.items():
        if len(terms_list) == 0:
            continue
        line += get_value_string(factor, first_term=first_term_outer)

        sub_line = ''
        first_term_inner = True
        for j, (term, value) in enumerate(terms_list):
            new_term = get_value_string(value, first_term=first_term_inner) + '\\mathcal{O}_{' + term + '}'
            sub_line += new_term
            first_term_inner = False

        if len(terms_list) > 1:
            sub_line  = '\\Big(' + sub_line + '\\Big)'
        line += sub_line

        first_term_outer = False
    # line += '\\Bigg)'

    return line


def main():
    operator ={'0033': -0.1, '0123': 0.2, '0213': 0.8, '0303': 0.2, '0312': -1.0, '1122': -0.1, '1203': -1.0, '1212': 0.2, '1302': 0.8, '2211': -0.1, '2301': 0.2, '3300': -0.1}
    op_latex = get_operator_latex(operator)
    print(op_latex)
    with open('try.tex', 'w+') as f:
        f.write("\\documentclass[12pt]{article}\n")
        f.write("\\usepackage{amsmath,amssymb,color,makeidx,url,wrapfig}\n")
        f.write("\\setlength{\\tabcolsep}{10pt} \n \\renewcommand{\\arraystretch}{1.5} \n ")
        f.write("\\begin{document}\n")
        f.write("\\begin{table}[] \\centering \\begin{tabular}{ | c | c |}\n")
        f.write("\\hline\n")
        f.write(f"main & ${op_latex}$\\\ \n")
        f.write("\\hline\n")
        f.write("\\end{tabular}")
        f.write("\\end{table}")
        f.write("\\end{document}")
    os.system("open try.tex -a TexShop")

if __name__ == "__main__":
    main()
