import itertools
import numpy as np
import sys
import glob
from rearrange_quarks import get_primtive_op_from_elemenal_op
from Group_theory_definitions.DR_Euclidean_basis import get_DeGrand_Rossi_Euclidean_basis
Nd = 4


def get_Buchoff_ops():
    gammas = get_DeGrand_Rossi_Euclidean_basis()
    C = gammas['C']
    g5 = gammas['g5']
    g1 = gammas['g1']
    g2 = gammas['g2']
    g3 = gammas['g3']

    Cg5 = np.matmul(C, g5)
    Cg1 = np.matmul(C, g1)
    Cg2 = np.matmul(C, g2)
    Cg3 = np.matmul(C, g3)

    spin0_Gamma = {}
    spin1a_Gamma = {}
    spin1b_Gamma = {}
    spin1c_Gamma = {}
    spin2a_Gamma = {}
    spin2b_Gamma = {}
    spin2c_Gamma = {}

    for alpha in range(4):
        for beta in range(4):
            for sigma in range(4):
                for delta in range(4):
                    val = Cg5[alpha, beta] * Cg5[sigma, delta]
                    if abs(val) > 1e-8:
                        spin0_Gamma[f'{alpha}{beta}{sigma}{delta}'] = val

                    val = Cg1[alpha, beta] * Cg5[sigma, delta]
                    if abs(val) > 1e-8:
                        spin1a_Gamma[f'{alpha}{beta}{sigma}{delta}'] = val
                    val = Cg2[alpha, beta] * Cg5[sigma, delta]
                    if abs(val) > 1e-8:
                        spin1b_Gamma[f'{alpha}{beta}{sigma}{delta}'] = val
                    val = Cg3[alpha, beta] * Cg5[sigma, delta]
                    if abs(val) > 1e-8:
                        spin1c_Gamma[f'{alpha}{beta}{sigma}{delta}'] = val

                    val = Cg1[alpha, beta] * Cg2[sigma, delta]
                    if abs(val) > 1e-8:
                        spin2a_Gamma[f'{alpha}{beta}{sigma}{delta}'] = val
                    val = Cg2[alpha, beta] * Cg3[sigma, delta]
                    if abs(val) > 1e-8:
                        spin2b_Gamma[f'{alpha}{beta}{sigma}{delta}'] = val
                    val = Cg3[alpha, beta] * Cg1[sigma, delta]
                    if abs(val) > 1e-8:
                        spin2c_Gamma[f'{alpha}{beta}{sigma}{delta}'] = val
    with open(f'Operators/DR_Euclidean/gamma_matrix_Nc4_Buchoff_S0_choice4.txt', 'w+') as f:
        for key, val in spin0_Gamma.items():
            line = ' '.join(key) + ' ' + str(np.real(-val)) + ' ' + str(np.imag(-val)) + '\n'
            f.write(line)
    with open(f'Operators/DR_Euclidean/gamma_matrix_Nc4_Buchoff_S2_choice12.txt', 'w+') as f:
        for key, val in spin2a_Gamma.items():
            line = ' '.join(key) + ' ' + str(np.real(-val)) + ' ' + str(np.imag(-val)) + '\n'
            f.write(line)
    return spin0_Gamma, spin1a_Gamma, spin1b_Gamma, spin1c_Gamma, spin2a_Gamma, spin2b_Gamma, spin2c_Gamma


def get_base_Gamma(Nc, data_file):
    shape = [Nd] * Nc
    Gamma = np.zeros(shape, dtype=np.cdouble)
    spins_list = []
    with open(data_file, 'r') as f:
        for line in f:
            print(line)
            alpha, beta, sigma, delta, real, imag, _ = line.split(' ')
            alpha, beta, sigma, delta = int(alpha), int(beta), int(sigma), int(delta)
            spins_list.append([alpha, beta, sigma, delta])
            real, imag = float(real), float(imag)
            Gamma[alpha, beta, sigma, delta] = real + 1j * imag
    return Gamma, spins_list


def get_Gamma_symmetrized(Nc, isospin, Gamma, spins_list):
    shape = [Nd] * Nc
    Gamma_symmetrized = np.zeros(shape, dtype=np.cdouble)
    for spins in spins_list:
        perms = itertools.permutations(spins)
        perms = list(set(list(perms)))
        for perm in perms:
            dict = {''.join([str(s) for s in perm]): 1}
            op_simple = get_primtive_op_from_elemenal_op(dict, isospin)
            print(op_simple)
        print()
    exit()


FLAVOR_TO_NUM = {
    'u': 1/2,
    'd': -1/2
}

NUM_TO_FLAVOR = {
    0.5: 'u',
    -0.5: 'd'
}


def get_J_squared_term(Nc, flavors):
    # J^2 = J_J+ + Jz^2 + Jz
    term_original = np.array([FLAVOR_TO_NUM[f] for f in flavors])
    Jz = sum(term_original)
    flips = np.identity(len(flavors)) # [1,0,0], [0,1,0], [0,0,1]
    J_plus_term_initial = [term_original + flip for flip in flips]
    J_plus_term = []
    for term in J_plus_term_initial:
        if 1.5 not in term: # spin +0.5 flipped up (add 1) not possible
            J_plus_term.append(term)
    J_plus_term = np.array(J_plus_term)
    J_minus_J_plus_term_initial = [term - flip for flip in flips for term in J_plus_term]
    J_minus_J_plus_term = []
    for term in J_minus_J_plus_term_initial:
        if -1.5 not in term: # spin -0.5 flipped down (subtract 1) not possible
            J_minus_J_plus_term.append(term)

    J_sqrd_term = {}
    for term in J_minus_J_plus_term:
        term_flavors = ''.join([NUM_TO_FLAVOR[f] for f in term])
        try:
            J_sqrd_term[term_flavors] += 1
        except:
            J_sqrd_term[term_flavors] = 1
    try:
        J_sqrd_term[flavors] += Jz**2 + Jz
    except:
        J_sqrd_term[flavors] = Jz ** 2 + Jz
    return J_sqrd_term


def normalized_operator(operator):
    norm = np.sqrt(sum([val**2 for val in list(operator.values())]))
    if abs(norm) < 1e-8:
        return operator
    op_normed = {}
    for key, val in operator.items():
        op_normed[key] = val/norm
    return op_normed


def get_I_squared(Nc, operator_i, operator_j):
    # < O_i | I^2 | O_j >
    operator_i = normalized_operator(operator_i)
    operator_j = normalized_operator(operator_j)
    J_squared_val = 0
    for term_i, val_i in operator_i.items():
        for term_j, val_j in operator_j.items():
            flavor_j, spin_j = term_j.split(' ')
            I_squared_term_j = get_J_squared_term(Nc, flavor_j)
            for flavor, factor in I_squared_term_j.items():
                if f'{flavor} {spin_j}' == term_i:
                    J_squared_val += val_i * val_j * factor
    return J_squared_val

def get_op_nonzero(op):
    op_nonzero = {}
    for key, val in op.items():
        if abs(val) > 1e-8:
            op_nonzero[key] = val
    return op_nonzero


def main():
    Nc = 4
    I_ops = [
    #     {'uudd': 2, 'dduu': 2, 'udud': -1, 'uddu': -1, 'duud': -1, 'dudu': -1}, # 12_34
    #     # {'udud': 1, 'uddu': -1, 'duud': -1, 'dudu': 1},  # 13_24
    #     {'udud': 1, 'uddu': 1, 'duud': -1, 'dudu': -1},  # 134_2 (ud -du)(ud + du)
        {'uuuu': 1},  # I = 2, Iz = 2
        {'uuud': 1, 'uudu': 1, 'uduu': 1, 'duuu': 1},  # I = 2, Iz = 1
        {'uudd': 1, 'udud': 1, 'uddu': 1, 'duud': 1, 'dudu': 1, 'dduu': 1}  # I = 2, Iz = 0
        #     # {'uuuu': 1}
    ]
    S_ops = [
    # #     {'0123': 1, '0213': 2}, # 12_34 A1g choice 0
    #     {'0101': 1, '2323': 1}, # 13_24 A1g choice 0
    #     # {'0101': 1, '2323': 1}, # 134_2 T1g choice 0
        {'0000': 1, '1111': -1, '2222': 1, '3333': -1} # 1234 T2g Sz2 choice 0
    ]


    # I_ops = [
    #     {'uudd': 1},
    # ]
    S0, S1a, S1b, S1c, S2a, S2b, S2c = get_Buchoff_ops()
    #                    udud               uuuu         uuud       uudd
    # S_ops = [S0, S1a] # 0,2 :)             NO OPS      NO,2       NO OPS
    # S_ops = [S0, S1b] # 0,2 :)             NO OPS      NO,2       NO OPS
    # S_ops = [S0, S1c] # 0,2 :)             NO OPS      NO,2       NO OPS
    # S_ops = [S0, S2a] # 0,16/3   :(        NO,6 :)     NO,14/3    NO,3?
    # S_ops = [S0, S2b] # 0,16/3   :(        NO,6 :)     NO,14/3    NO,3?
    # S_ops = [S0, S2c] # 0,16/3   :(        NO,6 :)     NO,14/3    NO,3?
    # S_ops = [S1a, S1b] # 2,2 :)            NO OPS      2,2        NO,NO
    # S_ops = [S1a, S2a] # 2,16/3  :(        NO,6 :)     2,14/3     NO,3
    # S_ops = [S2a, S2b] #16/3,16/3 :(        6,6 :)     14/3,14/3  3,3

    ops = [[{} for each in S_ops] for each in I_ops]
    ops_symmetrized = [[{} for each in S_ops] for each in I_ops]

    print('operators')
    for f, flavor in enumerate(I_ops):
        for s, spin in enumerate(S_ops):
            for flavor_term, flavor_val in flavor.items():
                for spin_term, spin_val in spin.items():
                    try:
                        ops[f][s][f'{flavor_term} {spin_term}'] += flavor_val*spin_val
                    except:
                        ops[f][s][f'{flavor_term} {spin_term}'] = flavor_val*spin_val

    print('symmetrized operators')
    for f, flavor in enumerate(I_ops):
        for s, spin in enumerate(S_ops):
            perms = itertools.permutations(np.arange(Nc))
            for perm in perms:
                for flavor_term, flavor_val in flavor.items():
                    for spin_term, spin_val in spin.items():
                        flavor_term_permuted = ''.join([flavor_term[p] for p in perm])
                        spin_term_permuted = ''.join([spin_term[p] for p in perm])
                        try:
                            ops_symmetrized[f][s][f'{flavor_term_permuted} {spin_term_permuted}'] += flavor_val*spin_val
                        except:
                            ops_symmetrized[f][s][f'{flavor_term_permuted} {spin_term_permuted}'] = flavor_val*spin_val
            print(ops_symmetrized[f][s])
            print()

    ops_symmetrized = [get_op_nonzero(ops_symmetrized[i][j]) for i in range(len(I_ops)) for j in range(len(S_ops))]
    num_ops = len(ops_symmetrized)
    I_squared = np.zeros((num_ops, num_ops), dtype=np.cdouble)
    for i in range(num_ops):
        for j in range(num_ops):
            I_squared[i,j] = get_I_squared(Nc, ops_symmetrized[i], ops_symmetrized[j])

    print(np.around(I_squared,3))
    w, v = np.linalg.eigh(I_squared)
    print(np.around(w,3))
    print(np.around(v,3))



    exit()
    Nc = int(sys.argv[1])
    isospin = sys.argv[2]
    irrep = sys.argv[3]

    filename_base = f'Operators/gamma_matrix_Nc{Nc}_isospin{isospin}_{irrep}_rep'
    local_data_file_list = glob.glob(f"{filename_base}*")
    for data_file in local_data_file_list:
        Gamma, spins_list = get_base_Gamma(Nc, data_file)
        get_Gamma_symmetrized(Nc, isospin, Gamma, spins_list)


if __name__ == '__main__':

    list1 = ['a0','b0','c1','d1']
    list2 = ['']

    main()