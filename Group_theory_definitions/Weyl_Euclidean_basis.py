import numpy as np

# basis as given in notes
Weyl_g1 = np.array([
    [0,0,0,-1j],
    [0,0,-1j,0],
    [0,1j,0,0],
    [1j,0,0,0]
])

Weyl_g2 = np.array([
    [0,0,0,-1],
    [0,0,1,0],
    [0,1,0,0],
    [-1,0,0,0]
])

Weyl_g3 = np.array([
    [0, 0, -1j, 0],
    [0, 0, 0, 1j],
    [1j, 0, 0, 0],
    [0, -1j, 0, 0]
])

Weyl_g4 = np.array([
    [0,0,1,0],
    [0,0,0,1],
    [1,0,0,0],
    [0,1,0,0]
])

Weyl_g5 = -np.matmul(np.matmul(np.matmul(Weyl_g1, Weyl_g2), Weyl_g3), Weyl_g4)
# need some signs to use basis transformation from Dirac to Weyl according to paper
Weyl_g1 = - Weyl_g1
Weyl_g2 = - Weyl_g2
Weyl_g3 = - Weyl_g3

Weyl_C = np.matmul(Weyl_g2, Weyl_g4)

IDENTITY = np.array([
    [1,0,0,0],
    [0,1,0,0],
    [0,0,1,0],
    [0,0,0,1]
])


def check_Clifford():
    Clifford = True
    if not np.allclose(np.matmul(Weyl_g1, Weyl_g1), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(Weyl_g2, Weyl_g2), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(Weyl_g3, Weyl_g3), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(Weyl_g4, Weyl_g4), IDENTITY):
        Clifford = False
    return Clifford


def check_g5():
    g5_definition = True
    # g5 = g1 g2 g3 g4
    g5_mult = np.matmul(Weyl_g1,  Weyl_g2)
    g5_mult = np.matmul(g5_mult, Weyl_g3)
    g5_mult = np.matmul(g5_mult, Weyl_g4)
    # Need this line if using basis from notes
    # g5_mult = -g5_mult
    if not np.allclose(g5_mult,  Weyl_g5):
        g5_definition = False
    return g5_definition


def CC_gamma(C, gamma):
    # C is defined via C gamma_mu C^-1 = -gamma_mu^T
    C_inv = np.linalg.inv(C)
    prod = np.matmul(np.matmul(C, gamma), C_inv)
    return prod


def check_charge_conjugation_rule():
    CC_rule = True
    if not np.allclose(CC_gamma(Weyl_C, Weyl_g1), -Weyl_g1.T):
        CC_rule = False
    if not np.allclose(CC_gamma(Weyl_C, Weyl_g2), -Weyl_g2.T):
        CC_rule = False
    if not np.allclose(CC_gamma(Weyl_C, Weyl_g3), -Weyl_g3.T):
        CC_rule = False
    if not np.allclose(CC_gamma(Weyl_C, Weyl_g4), -Weyl_g4.T):
        CC_rule = False
    return CC_rule


def get_Weyl_Euclidean_basis():
    if check_Clifford():
        if check_g5():
            if check_charge_conjugation_rule():
                basis = {}
                basis["g1"] = Weyl_g1
                basis["g2"] = Weyl_g2
                basis["g3"] = Weyl_g3
                basis["g4"] = Weyl_g4
                basis["g5"] = Weyl_g5
                basis["C"] =  Weyl_C
                return basis
            else:
                print("problem with charge conjugation matrix")
                exit()
        else:
            print("problem with gamma5")
            exit()
    else:
        print("problem with Clifford algebra")
        exit()


if __name__ == '__main__':
    basis = get_Weyl_Euclidean_basis()
    print(basis['g5'])

    # from Dirac_Euclidean_basis import get_Dirac_Euclidean_basis
    # dirac_basis = get_Dirac_Euclidean_basis()
    #
    # U = np.array(1/np.sqrt(2)*(np.identity(4) + np.matmul(dirac_basis['g5'], dirac_basis['g4'])))
    # U_dag = np.conj(U.transpose())
    # check = np.matmul(U, U_dag)
    # print(np.around(check,8))
    #
    # g1_D_to_W = np.matmul(U, np.matmul(dirac_basis['g1'], U_dag))
    # g2_D_to_W = np.matmul(U, np.matmul(dirac_basis['g2'], U_dag))
    # g3_D_to_W = np.matmul(U, np.matmul(dirac_basis['g3'], U_dag))
    # g4_D_to_W = np.matmul(U, np.matmul(dirac_basis['g4'], U_dag))
    # g5_D_to_W = np.matmul(U, np.matmul(dirac_basis['g5'], U_dag))
    #
    # print(np.allclose(basis['g1'], g1_D_to_W))
    # print(np.allclose(basis['g2'], g2_D_to_W))
    # print(np.allclose(basis['g3'], g3_D_to_W))
    # print(np.allclose(basis['g4'], g4_D_to_W))
    # print(np.allclose(basis['g5'], g5_D_to_W))
    #
    # print('Weyl g1')
    # print(np.around(basis['g1'],2))
    # print()
    # print('Dirac to Weyl g1')
    # print(np.around(g1_D_to_W,2))
    # print('\n\n')
    # print('Weyl g2')
    # print(np.around(basis['g2'],2))
    # print()
    # print('Dirac to Weyl g2')
    # print(np.around(g2_D_to_W,2))
    #
    # print('\n\n')
    # print('Weyl g3')
    # print(np.around(basis['g3'],2))
    # print()
    # print('Dirac to Weyl g3')
    # print(np.around(g3_D_to_W,2))
    #
    # print('\n\n')
    # print('Weyl g4')
    # print(np.around(basis['g4'],2))
    # print()
    # print('Dirac to Weyl g4')
    # print(np.around(g4_D_to_W,2))
    #
    # print('\n\n')
    # print('Weyl g4')
    # print(np.around(basis['g4'],2))
    # print()
    # print('Dirac to Weyl g4')
    # print(np.around(g4_D_to_W,2))
