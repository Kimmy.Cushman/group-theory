import numpy as np


# UKQCD Twisted Dirac as listed in https://github.com/JeffersonLab/chroma/blob/master/docs/notes/gamma_conventions.tex
DE_g4 = np.array([
    [1,0,0,0],
    [0,1,0,0],
    [0,0,-1,0],
    [0,0,0,-1]
])

DE_g1 = np.array([
    [0,0,0,1j],
    [0,0,1j,0],
    [0,-1j,0,0],
    [-1j,0,0,0]
])

DE_g2 = np.array([
    [0,0,0,1],
    [0,0,-1,0],
    [0,-1,0,0],
    [1,0,0,0]
])

DE_g3 = np.array([
    [0,0,1j,0],
    [0,0,0,-1j],
    [-1j,0,0,0],
    [0,1j,0,0]
])

DE_g5 = np.matmul(np.matmul(np.matmul(DE_g1, DE_g2), DE_g3), DE_g4)

DE_C = np.matmul(DE_g2, DE_g4)

IDENTITY = np.array([
    [1,0,0,0],
    [0,1,0,0],
    [0,0,1,0],
    [0,0,0,1]
])


def check_Clifford():
    Clifford = True
    if not np.allclose(np.matmul(DE_g1, DE_g1), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(DE_g2, DE_g2), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(DE_g3, DE_g3), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(DE_g4, DE_g4), IDENTITY):
        Clifford = False
    return Clifford


def check_g5():
    g5_definition = True
    # g5 = g1 g2 g3 g4
    g5_mult = np.matmul(DE_g1,  DE_g2)
    g5_mult = np.matmul(g5_mult, DE_g3)
    g5_mult = np.matmul(g5_mult, DE_g4)
    if not np.allclose(g5_mult,  DE_g5):
        g5_definition = False
    return g5_definition


def CC_gamma(C, gamma):
    C_inv = np.linalg.inv(C)
    prod = np.matmul(C, gamma)
    prod = np.matmul(prod, C_inv)
    return prod


def check_charge_conjugation_rule():
    CC_rule = True
    if not np.allclose(CC_gamma(DE_C, DE_g1), -DE_g1.T):
        CC_rule = False
    if not np.allclose(CC_gamma(DE_C, DE_g2), -DE_g2.T):
        CC_rule = False
    if not np.allclose(CC_gamma(DE_C, DE_g3), -DE_g3.T):
        CC_rule = False
    if not np.allclose(CC_gamma(DE_C, DE_g4), -DE_g4.T):
        CC_rule = False
    return CC_rule


def get_Dirac_Euclidean_basis():
    if check_Clifford() and check_g5() and check_charge_conjugation_rule():
        basis = {}
        basis["g1"] = DE_g1
        basis["g2"] = DE_g2
        basis["g3"] = DE_g3
        basis["g4"] = DE_g4
        basis["g5"] = DE_g5
        basis["C"] =  DE_C
        return basis
    else:
        print("problem with basis")
        return


def main():
    basis = get_Dirac_Euclidean_basis()
    C = basis['C']
    g5 = basis['g5']
    g4 = basis['g4']
    g3 = basis['g3']
    g2 = basis['g2']
    g1 = basis['g1']
    I = np.identity(4)
    U = -np.matmul(np.matmul(g1, g3), g4) + np.matmul(g4, g2)
    print(U)


if __name__ == '__main__':
    main()





