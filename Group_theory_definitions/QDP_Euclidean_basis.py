import numpy as np

# https://usqcd.jlab.org/usqcd-docs/qdp++/manual.pdf

'''
NOTE! QDP documentation uses g0, g1, g2, g3, with g3 corresponding to temporal. 
Here, we relabel: matrices are same order as listed in the manual, but here 
they are renamed g1 g2 g3 g4
'''

QDP_g1 = np.array([
    [0,0,0,1j],
    [0,0,1j,0],
    [0,-1j,0,0],
    [-1j,0,0,0]
])

QDP_g2 = np.array([
    [0,0,0,-1],
    [0,0,1,0],
    [0,1,0,0],
    [-1,0,0,0]
])

QDP_g3 = np.array([
    [0,0,1j,0],
    [0,0,0,-1j],
    [-1j,0,0,0],
    [0,1j,0,0]
])

QDP_g4 = np.array([
    [0,0,1,0],
    [0,0,0,1],
    [1,0,0,0],
    [0,1,0,0]
])

QDP_g5 = np.matmul(np.matmul(np.matmul(QDP_g1, QDP_g2), QDP_g3), QDP_g4)

QDP_C = np.matmul(QDP_g2, QDP_g4)

IDENTITY = np.array([
    [1,0,0,0],
    [0,1,0,0],
    [0,0,1,0],
    [0,0,0,1]
])


def check_Clifford():
    Clifford = True
    if not np.allclose(np.matmul(QDP_g1, QDP_g1), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(QDP_g2, QDP_g2), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(QDP_g3, QDP_g3), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(QDP_g4, QDP_g4), IDENTITY):
        Clifford = False
    return Clifford


def check_g5():
    g5_definition = True
    # g5 = g1 g2 g3 g4
    g5_mult = np.matmul(QDP_g1,  QDP_g2)
    g5_mult = np.matmul(g5_mult, QDP_g3)
    g5_mult = np.matmul(g5_mult, QDP_g4)
    if not np.allclose(g5_mult,  QDP_g5):
        g5_definition = False
    return g5_definition


def CC_gamma(C, gamma):
    # C is defined via C gamma_mu C^-1 = -gamma_mu^T
    C_inv = np.linalg.inv(C)
    prod = np.matmul(np.matmul(C, gamma), C_inv)
    return prod


def check_charge_conjugation_rule():
    CC_rule = True
    if not np.allclose(CC_gamma(QDP_C, QDP_g1), -QDP_g1.T):
        CC_rule = False
    if not np.allclose(CC_gamma(QDP_C, QDP_g2), -QDP_g2.T):
        CC_rule = False
    if not np.allclose(CC_gamma(QDP_C, QDP_g3), -QDP_g3.T):
        CC_rule = False
    if not np.allclose(CC_gamma(QDP_C, QDP_g4), -QDP_g4.T):
        CC_rule = False
    return CC_rule


def get_QDP_Euclidean_basis():
    if check_Clifford():
        if check_g5():
            if check_charge_conjugation_rule():
                basis = {}
                basis["g1"] = QDP_g1
                basis["g2"] = QDP_g2
                basis["g3"] = QDP_g3
                basis["g4"] = QDP_g4
                basis["g5"] = QDP_g5
                basis["C"] =  QDP_C
                return basis
            else:
                print("problem with charge conjugation matrix")
                exit()
        else:
            print("problem with gamma5")
            exit()
    else:
        print("problem with Clifford algebra")
        exit()


if __name__ == '__main__':
    basis = get_QDP_Euclidean_basis()



