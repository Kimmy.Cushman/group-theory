import numpy as np
from Group_theory_definitions.octahedral import \
    octahedral_group_multiplication_rules, \
    get_octahedral_representations, \
    get_octahedral_character_table, \
    get_octahedral_group_spinor_rotations, \
    OCTAHEDRAL_MULTIPLICATION_TABLE
"""
48 elements 
_X is used to differentiate from octahedral group element used in double_octahedral_group_multiplication_rules function
_bG = bar{G}
Hardwired double octahedral group classes
C1 = {_E}
C2 = {_C3alpha, _C3beta, _C3gamma, _C3delta, _C3alpha_inv, _C3beta_inv, _C3gamma_inv, _C3delta_inv} 
C3 = {_C2x, _C2y, _C2z, _bC2x, _bC2y, _bC2z}
C4 = {_C4x, _C4y, _C4z, _C4x_inv, _C4y_inv, _C4z_inv}
C5 = {_C2a, _C2b, _C2c, _C2d, _C2e, _C2f, _bC2a, _bC2b, _bC2c, _bC2d, _bC2e, _bC2f}
C6 = {_bE}
C7 = {_bC3alpha, _bC3beta, _bC3gamma, _bC3delta, _bC3alpha_inv, _bC3beta_inv, _bC3gamma_inv, _bC3delta_inv} 
C8 = {_bC4x, _bC4y, _bC4z, _bC4x_inv, _bC4y_inv, _bC4z_inv}

Reps:
A1, A2, E, T1, T2 (regular)
G1, G2, H (extra)

Regular reps Gamma(bar{E}) = + Gamma(E)  ->  Gamma(bar{G}) = + Gamma(G) 
Extra   reps Gamma(bar{E}) = - Gamma(E)  ->  Gamma(bar{G}) = - Gamma(G)
"""

DOUBLE_OCTAHEDRAL_MULTIPLICATION_TABLE = {
    '_C2x': 'C2x',
    '_C2y': 'C2y',
    '_C2z': 'C2z',
    '_C3alpha': 'C3alpha',
    '_C3beta': 'C3beta',
    '_C3gamma': 'C3gamma',
    '_C3delta': 'C3delta',
    '_C3alpha_inv': 'C3alpha_inv',
    '_C3beta_inv': 'C3beta_inv',
    '_C3gamma_inv': 'C3gamma_inv',
    '_C3delta_inv': 'C3delta_inv',
    '_C2a': 'C2a',
    '_C2b': 'C2b',
    '_C2c': 'C2c',
    '_C2d': 'C2d',
    '_C2e': 'C2e',
    '_C2f': 'C2f',
    '_bC4y_inv': 'C2y C4y',
    '_bE': '_bC4y_inv C4y',
    '_bC4y': '_bE C4y',
    '_bC2y':'_bC4y C4y',
    '_C4y_inv': '_bC2y C4y',
    '_bC4z_inv': 'C2z C4z',
    '_bC4z': '_bE C4z',
    '_bC2z': '_bC4z C4z',
    '_C4z_inv': '_bC2z C4z',
    '_C4x': '_C4z_inv C3delta',
    '_bC4x_inv': 'C2x _C4x',
    '_bC4x': '_bE _C4x',
    '_bC2x': '_bC4x _C4x',
    '_C4x_inv': '_bC2x _C4x',
    '_bC3alpha': '_bE C3alpha',
    '_bC3beta': '_bE C3beta',
    '_bC3gamma': '_bE C3gamma',
    '_bC3delta': '_bE C3delta',
    '_bC3alpha_inv': '_bE C3alpha_inv',
    '_bC3beta_inv': '_bE C3beta_inv',
    '_bC3gamma_inv': '_bE C3gamma_inv',
    '_bC3delta_inv': '_bE C3delta_inv',
    '_bC2a': '_bE C2a',
    '_bC2b': '_bE C2b',
    '_bC2c': '_bE C2c',
    '_bC2d': '_bE C2d',
    '_bC2e': '_bE C2e',
    '_bC2f': '_bE C2f'}
DOUBLE_OCTAHEDRAL_MULTIPLICATION_TABLE.update(OCTAHEDRAL_MULTIPLICATION_TABLE.copy())

DOUBLE_OCTAHEDRAL_REPS = ['A1', 'A2', 'E', 'T1', 'T2', 'G1', 'G2', 'H']

####################################################


def double_octahedral_group_multiplication_rules(C4y, C4z):
    octahedral_mult_rules = octahedral_group_multiplication_rules(C4y, C4z)
    # A1, A2, E, T1, T2 representations are the same as octahedral group
    # use Gamma(_bX) = Gamma(_X)
    # have to be careful with extra representations G1, G2, H
    # Gamma(_bX) = - Gamma(_X)
    # This is automatically satisfied with these rules --
    # FIXME - yes it is, I checked printing _bE with regular and extra, but I don't know why it is true?
    #  Is it hard wired somewhere?

    def multiplication_rules(element):
        # table on page
        if element == '_E':
            return np.identity(len(C4y))
        elif element == '_C4y':
            return C4y
        elif element == '_C4z':
            return C4z
        else:
            factors = DOUBLE_OCTAHEDRAL_MULTIPLICATION_TABLE[element].split(' ')
            if len(factors) == 1:
                a = factors[0]
                return octahedral_mult_rules(a)
            else:
                a, b = factors
                if a[0] == '_':
                    A = multiplication_rules(a)
                else:
                    A = octahedral_mult_rules(a)
                if b[0] == '_':
                    B = multiplication_rules(b)
                else:
                    B = octahedral_mult_rules(b)
                return np.matmul(A, B)

    return multiplication_rules


def get_double_octahedral_conjugacy_classes():
    C1 = ['_E']

    C2 = [f'_C3{alpha}' for alpha in ['alpha', 'beta', 'gamma', 'delta']]
    C2.extend([f'{element}_inv' for element in C2])

    C3 = [f'C2{x}' for x in ['x', 'y', 'z']]
    C3.extend([f'b{element}' for element in C3])
    C3 = [f'_{element}' for element in C3]

    C4 = [f'_C4{x}' for x in ['x', 'y', 'z']]
    C4.extend([f'{element}_inv' for element in C4])

    C5 = [f'C2{a}' for a in ['a', 'b', 'c', 'd', 'e', 'f']]
    C5.extend([f'b{element}' for element in C5])
    C5 = [f'_{element}' for element in C5]

    C6 = ['_bE']

    C7 = [f'_bC3{alpha}' for alpha in ['alpha', 'beta', 'gamma', 'delta']]
    C7.extend([f'{element}_inv' for element in C7])

    C8 = [f'_bC4{x}' for x in ['x', 'y', 'z']]
    C8.extend([f'{element}_inv' for element in C8])

    conjugacy_classes = {
        'C1': C1,
        'C2': C2,
        'C3': C3,
        'C4': C4,
        'C5': C5,
        'C6': C6,
        'C7': C7,
        'C8': C8,
    }
    return conjugacy_classes


def get_double_octahedral_representation_generators():
    octahedral_representations = get_octahedral_representations()
    # A1, A2, E, T1, T2 representations are the same as octahedral group
    # use Gamma(_bX) = Gamma(_X), applied in function above
    # have to be careful with extra representations,
    # Gamma(_bX) = - Gamma(_X), turns out to be true in function above - # FIXME WHY?
    representations = octahedral_representations.copy()
    a = np.sqrt(2)
    b = np.sqrt(3)
    representations['G1_rep'] = {'C4y': 1/a * np.array([[1, -1], [1, 1]]),
                                 'C4z': 1/a * np.array([[1 - 1j, 0], [0, 1 + 1j]])}
    representations['G2_rep'] = {'C4y': -1/a * np.array([[1, -1], [1, 1]]),
                                 'C4z': -1/a * np.array([[1 - 1j, 0], [0, 1 + 1j]])}
    representations['H_rep'] = {'C4y': 1/(2 * a) * np.array([
        [1, -b, b, -1],
        [b, -1, -1, b],
        [b, 1, -1, -b],
        [1, b, b, 1]
    ]),
                                'C4z': 1/a * np.array([
                                    [-1 - 1j, 0, 0, 0],
                                    [0, 1 - 1j, 0, 0],
                                    [0, 0, 1 + 1j, 0],
                                    [0, 0, 0, -1 + 1j]
                                ])}
    return representations


def get_double_octahedral_character_table():
    # page 81
    octahedral_character_table = get_octahedral_character_table().copy()
    double_octaheral_character_table = {'G1_rep': {},
                                        'G2_rep': {},
                                        'H_rep': {}}
    for rep in octahedral_character_table.keys():
        double_octaheral_character_table[rep] = {}
        for class_name, character in octahedral_character_table[rep].items():
            if class_name == 'C1':
                # C1_D = C1
                # C6_D = bC6
                double_octaheral_character_table[rep]['C1'] = character
                double_octaheral_character_table[rep]['C6'] = character
                double_octaheral_character_table['G1_rep']['C1'] = +2
                double_octaheral_character_table['G1_rep']['C6'] = -2
                double_octaheral_character_table['G2_rep']['C1'] = +2
                double_octaheral_character_table['G2_rep']['C6'] = -2
                double_octaheral_character_table['H_rep']['C1'] = +4
                double_octaheral_character_table['H_rep']['C6'] = -4

            if class_name == 'C2':
                # C2_D = C2
                # C7_D = bC2
                double_octaheral_character_table[rep]['C2'] = character
                double_octaheral_character_table[rep]['C7'] = character
                double_octaheral_character_table['G1_rep']['C2'] = +1
                double_octaheral_character_table['G1_rep']['C7'] = -1
                double_octaheral_character_table['G2_rep']['C2'] = +1
                double_octaheral_character_table['G2_rep']['C7'] = -1
                double_octaheral_character_table['H_rep']['C2'] = -1
                double_octaheral_character_table['H_rep']['C7'] = +1

            if class_name == 'C3':
                # C3_D = C3 + bC3
                # -> character C3 = 0 for extra reps
                double_octaheral_character_table[rep]['C3'] = character
                double_octaheral_character_table['G1_rep']['C3'] = 0
                double_octaheral_character_table['G2_rep']['C3'] = 0
                double_octaheral_character_table['H_rep']['C3'] = 0

            if class_name == 'C4':
                # C4_D = C4
                # C8_D = bC4
                double_octaheral_character_table[rep]['C4'] = character
                double_octaheral_character_table[rep]['C8'] = character
                double_octaheral_character_table['G1_rep']['C4'] = +np.sqrt(2)
                double_octaheral_character_table['G1_rep']['C8'] = -np.sqrt(2)
                double_octaheral_character_table['G2_rep']['C4'] = -np.sqrt(2)
                double_octaheral_character_table['G2_rep']['C8'] = +np.sqrt(2)
                double_octaheral_character_table['H_rep']['C4'] = 0
                double_octaheral_character_table['H_rep']['C8'] = 0

            if class_name == 'C5':
                # C5_D = C5 + bC5
                # -> character C5 = 0 for extra reps
                double_octaheral_character_table[rep]['C5'] = character
                double_octaheral_character_table['G1_rep']['C5'] = 0
                double_octaheral_character_table['G2_rep']['C5'] = 0
                double_octaheral_character_table['H_rep']['C5'] = 0

    return double_octaheral_character_table


