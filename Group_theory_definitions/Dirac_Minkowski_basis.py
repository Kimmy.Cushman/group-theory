import numpy as np

# Dirac Minkowski
DM_g0 = np.array([
    [1,0,0,0],
    [0,1,0,0],
    [0,0,-1,0],
    [0,0,0,-1]
])

DM_g1 = np.array([
    [0,0,0,1],
    [0,0,1,0],
    [0,-1,0,0],
    [-1,0,0,0]
])

DM_g2 = np.array([
    [0,0,0,-1j],
    [0,0,1j,0],
    [0,1j,0,0],
    [-1j,0,0,0]
])

DM_g3 = np.array([
    [0,0,1,0],
    [0,0,0,-1],
    [-1,0,0,0],
    [0,1,0,0]
])

DM_g5 = np.array([
    [0,0,1,0],
    [0,0,0,1],
    [1,0,0,0],
    [0,1,0,0]
])

DM_C = np.array([
    [0,0,0,-1],
    [0,0,1,0],
    [0,-1,0,0],
    [1,0,0,0]
])

IDENTITY = np.array([
    [1, 0, 0, 0],
    [0, 1, 0, 0],
    [0, 0, 1, 0],
    [0, 0, 0, 1]
])


def check_Clifford():
    Clifford = True
    if not np.allclose(np.matmul(DM_g1, DM_g1), -IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(DM_g2, DM_g2), -IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(DM_g3, DM_g3), -IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(DM_g0, DM_g0), IDENTITY):
        Clifford = False
    return Clifford


def check_g5():
    g5_definition = True
    # g5 = i g0 g1 g2 g3
    g5_mult = 1j * np.matmul(DM_g0, DM_g1)
    g5_mult = np.matmul(g5_mult, DM_g2)
    g5_mult = np.matmul(g5_mult, DM_g3)
    if not np.allclose(g5_mult, DM_g5):
        g5_definition = False
    return g5_definition


def CC_gamma(C, gamma):
    C_inv = np.linalg.inv(C)
    prod = np.matmul(C, gamma)
    prod = np.matmul(prod, C_inv)
    return prod


def check_charge_conjugation_rule():
    CC_rule = True
    if not np.allclose(CC_gamma(DM_C, DM_g1), -DM_g1.T):
        CC_rule = False
    if not np.allclose(CC_gamma(DM_C, DM_g2), -DM_g2.T):
        CC_rule = False
    if not np.allclose(CC_gamma(DM_C, DM_g3), -DM_g3.T):
        CC_rule = False
    if not np.allclose(CC_gamma(DM_C, DM_g0), -DM_g0.T):
        CC_rule = False
    return CC_rule


def get_Dirac_Minkowski_basis():
    if check_Clifford() and check_g5() and check_charge_conjugation_rule():
        basis = {}
        basis["g1"] = DM_g1
        basis["g2"] = DM_g2
        basis["g3"] = DM_g3
        basis["g0"] = DM_g0
        basis["g5"] = DM_g5
        basis["C"] = DM_C
        return basis
    else:
        print("problem with basis")
        return


def main():
    basis = get_Dirac_Minkowski_basis()
    C = basis['C']
    g5 = basis['g5']
    print(np.matmul(C,g5))


if __name__ == '__main__':
    main()





