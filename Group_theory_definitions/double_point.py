import numpy as np
# multiplication rules for double octahedral group using generators C4y and C4z
from Group_theory_definitions.octahedral import get_octahedral_group_spinor_rotations, get_gamma
from Group_theory_definitions.double_octahedral import \
    double_octahedral_group_multiplication_rules, \
    get_double_octahedral_conjugacy_classes, \
    get_double_octahedral_representation_generators, \
    get_double_octahedral_character_table, \
    DOUBLE_OCTAHEDRAL_MULTIPLICATION_TABLE
from Group_theory_definitions.point import get_point_representations
"""
96 elements 

Same conventions as double octahedral below 
_X is used to differentiate from octahedral group element used in double_octahedral_group_multiplication_rules function
_bG = bar{G}

Hardwired double point group classes
C1 = {_E}
C2 = {_C3alpha, _C3beta, _C3gamma, _C3delta, _C3alpha_inv, _C3beta_inv, _C3gamma_inv, _C3delta_inv} 
C3 = {_C2x, _C2y, _C2z, _bC2x, _bC2y, _bC2z}
C4 = {_C4x, _C4y, _C4z, _C4x_inv, _C4y_inv, _C4z_inv}
C5 = {_C2a, _C2b, _C2c, _C2d, _C2e, _C2f, _bC2a, _bC2b, _bC2c, _bC2d, _bC2e, _bC2f}
C6 = {_bE}
C7 = {_bC3alpha, _bC3beta, _bC3gamma, _bC3delta, _bC3alpha_inv, _bC3beta_inv, _bC3gamma_inv, _bC3delta_inv} 
C8 = {_bC4x, _bC4y, _bC4z, _bC4x_inv, _bC4y_inv, _bC4z_inv}
C9 = {Is}
C10 = {Is_C3alpha, Is_C3beta, Is_C3gamma, Is_C3delta, Is_C3alpha_inv, Is_C3beta_inv, Is_C3gamma_inv, Is_C3delta_inv} 
C11 = {Is_C2x, Is_C2y, Is_C2z, Is_bC2x, Is_bC2y, Is_bC2z}
C12 = {Is_C4x, Is_C4y, Is_C4z, Is_C4x_inv, Is_C4y_inv, Is_C4z_inv}
C13 = {Is_C2a, Is_C2b, Is_C2c, Is_C2d, Is_C2e, Is_C2f, Is_bC2a, Is_bC2b, Is_bC2c, Is_bC2d, Is_bC2e, Is_bC2f}
C14 = {Is_bE}
C15 = {Is_bC3alpha, Is_bC3beta, Is_bC3gamma, Is_bC3delta, Is_bC3alpha_inv, Is_bC3beta_inv, Is_bC3gamma_inv, Is_bC3delta_inv} 
C16 = {Is_bC4x, Is_bC4y, Is_bC4z, Is_bC4x_inv, Is_bC4y_inv, Is_bC4z_inv}

Reps:
A1g, A2g, Eg, T1g, T2g (regular, 'gerade' AKA even)
A1u, A2u, Eu, T1u, T2u (regular, 'ungerade' AKA odd)
G1g, G2g, Hg (extra, 'gerade' AKA even)
G1u, G2u, Hu (extra, 'ungerade' AKA odd)

Inhereted from double octahedral group
Regular reps Gamma(bar{E}) = + Gamma(E)  ->  Gamma(bar{G}) = + Gamma(G) 
Extra   reps Gamma(bar{E}) = - Gamma(E)  ->  Gamma(bar{G}) = - Gamma(G) 

Inhereted from point group
rep g reps Gamma(Is) = + Gamma(E)  ->  Gamma(Is_G) = + Gamma(G) 
rep u reps Gamma(Is) = - Gamma(E)  ->  Gamma(Is_G) = - Gamma(G) 

To summarize: 

Rep       E     bar{E}        Is        Is bar{E}
Reg_g     +       +            +         +
Reg_u     +       +            -         -
Extra_g   +       -            +         -
Extra_u   +       -            -         +      
where Reg representations are those associated with integer spin
and Extra representations are those associated with half integer spin
"""
DOUBLE_POINT_MULTIPLICATION_TABLE = DOUBLE_OCTAHEDRAL_MULTIPLICATION_TABLE.copy()
for key in DOUBLE_OCTAHEDRAL_MULTIPLICATION_TABLE.keys():
    DOUBLE_POINT_MULTIPLICATION_TABLE[f'Is_{key}'] = f'Is {key}'

DOUBLE_POINT_REPS = ['A1g', 'A2g', 'Eg', 'T1g', 'T2g', 'G1g', 'G2g', 'Hg',
                     'A1u', 'A2u', 'Eu', 'T1u', 'T2u', 'G1u', 'G2u', 'Hu']


def double_point_group_multiplication_rules(Is, C4y, C4z):
    double_octahedral_mult_rules = double_octahedral_group_multiplication_rules(C4y, C4z)

    def multiplication_rules(element):
        if element == 'Is':
            return Is
        elif 'Is' in element:
            base_element_name = element[2:]  # eg, Is_C4x, Is_bC4z_inv
            base_element = double_octahedral_mult_rules(base_element_name)
            return np.matmul(Is, base_element)
        else:
            return double_octahedral_mult_rules(element)

    return multiplication_rules


def get_double_point_conjugacy_classes():
    double_octahedral_conjugacy_classes = get_double_octahedral_conjugacy_classes()
    C1 = double_octahedral_conjugacy_classes['C1']
    C2 = double_octahedral_conjugacy_classes['C2']
    C3 = double_octahedral_conjugacy_classes['C3']
    C4 = double_octahedral_conjugacy_classes['C4']
    C5 = double_octahedral_conjugacy_classes['C5']
    C6 = double_octahedral_conjugacy_classes['C6']
    C7 = double_octahedral_conjugacy_classes['C7']
    C8 = double_octahedral_conjugacy_classes['C8']

    C9 = ['Is']
    C10 = ['Is' + element for element in C2]
    C11 = ['Is' + element for element in C3]
    C12 = ['Is' + element for element in C4]
    C13 = ['Is' + element for element in C5]
    C14 = ['Is' + element for element in C6]
    C15 = ['Is' + element for element in C7]
    C16 = ['Is' + element for element in C8]

    conjugacy_classes = {
        'C1': C1,
        'C2': C2,
        'C3': C3,
        'C4': C4,
        'C5': C5,
        'C6': C6,
        'C7': C7,
        'C8': C8,
        'C9': C9,
        'C10': C10,
        'C11': C11,
        'C12': C12,
        'C13': C13,
        'C14': C14,
        'C15': C15,
        'C16': C16,
    }

    return conjugacy_classes


def get_double_point_representation_generators():
    # A1, A2, E, T1, T2 exactly the same as point group
    point_reps = get_point_representations()
    representations = point_reps.copy()

    double_octahedral_reps = get_double_octahedral_representation_generators()
    G1_identity = np.identity(len(double_octahedral_reps['G1_rep']['C4y']))
    G2_identity = np.identity(len(double_octahedral_reps['G2_rep']['C4y']))
    H_identity = np.identity(len(double_octahedral_reps['H_rep']['C4y']))

    # g reps with Is = E
    representations['G1g_rep'] = {'C4y': double_octahedral_reps['G1_rep']['C4y'],
                                  'C4z': double_octahedral_reps['G1_rep']['C4z'],
                                  'Is': G1_identity}
    representations['G2g_rep'] = {'C4y': double_octahedral_reps['G2_rep']['C4y'],
                                  'C4z': double_octahedral_reps['G2_rep']['C4z'],
                                  'Is': G2_identity}
    representations['Hg_rep'] = {'C4y': double_octahedral_reps['H_rep']['C4y'],
                                 'C4z': double_octahedral_reps['H_rep']['C4z'],
                                 'Is': H_identity}

    # u reps with Is = -E
    representations['G1u_rep'] = {'C4y': double_octahedral_reps['G1_rep']['C4y'],
                                  'C4z': double_octahedral_reps['G1_rep']['C4z'],
                                  'Is': -G1_identity}
    representations['G2u_rep'] = {'C4y': double_octahedral_reps['G2_rep']['C4y'],
                                  'C4z': double_octahedral_reps['G2_rep']['C4z'],
                                  'Is': -G2_identity}
    representations['Hu_rep'] = {'C4y': double_octahedral_reps['H_rep']['C4y'],
                                 'C4z': double_octahedral_reps['H_rep']['C4z'],
                                 'Is': -H_identity}

    return representations


def get_double_point_character_table():
    double_octahedral_table = get_double_octahedral_character_table().copy()
    character_table = {}
    num_double_octahedral_classes = len(list(double_octahedral_table.keys()))
    for rep_name in double_octahedral_table.keys():
        g_rep_name = 'g_'.join(rep_name.split('_'))
        u_rep_name = 'u_'.join(rep_name.split('_'))
        # first 5 classes are same as octahedral
        character_table[g_rep_name] = double_octahedral_table[rep_name].copy()
        character_table[u_rep_name] = double_octahedral_table[rep_name].copy()
        # last 5 classes are Is x element
        # for g reps, Is = E, so elements have same character
        # for u reps, Is = -E, so elements have negative character
        for class_num in np.arange(1,num_double_octahedral_classes + 1):
            double_point_class = f'C{class_num + num_double_octahedral_classes}'
            double_character = double_octahedral_table[rep_name][f'C{class_num}']
            character_table[g_rep_name][double_point_class] = double_character
            character_table[u_rep_name][double_point_class] = -double_character

    return character_table

