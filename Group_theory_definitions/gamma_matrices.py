import numpy as np
import os
import sys

current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)

from Dirac_Minkowski_basis import get_Dirac_Minkowski_basis
from DR_Euclidean_basis import get_DeGrand_Rossi_Euclidean_basis
from Dirac_Euclidean_basis import get_Dirac_Euclidean_basis

DRE_basis = get_DeGrand_Rossi_Euclidean_basis()

DM_basis = get_Dirac_Minkowski_basis()

DE_basis = get_Dirac_Euclidean_basis()

g0 = DM_basis["g0"]
g1 = DM_basis["g1"]
g2 = DM_basis["g2"]
g3 = DM_basis["g3"]
g5 = DM_basis["g5"]
gC = DM_basis["C"]

g1E = DE_basis["g1"]
g2E = DE_basis["g2"]
g3E = DE_basis["g3"]
g4E = DE_basis["g4"]
g5E = DE_basis["g5"]
gCE = DE_basis["C"]



identity = np.identity(4)
g1g2 = np.matmul(g1E, g2E)
print()
print(g1g2 + identity)




