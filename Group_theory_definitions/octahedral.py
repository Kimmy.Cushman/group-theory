import numpy as np
from Group_theory_definitions.Dirac_Euclidean_basis import get_Dirac_Euclidean_basis
from Group_theory_definitions.DR_Euclidean_basis import get_DeGrand_Rossi_Euclidean_basis
from Group_theory_definitions.Dirac_Minkowski_basis import get_Dirac_Minkowski_basis
from Group_theory_definitions.QDP_Euclidean_basis import get_QDP_Euclidean_basis
from Group_theory_definitions.Weyl_Euclidean_basis import get_Weyl_Euclidean_basis
"""
24 elements 
Hardwired octahedral group classes
C1 = {E}
C2 = {C3alpha, C3beta, C3gamma, C3delta, C3alpha_inv, C3beta_inv, C3gamma_inv, C3delta_inv}
C3 = {C2x, C2y, C2z}
C4 = {C4x, C4y, C4z, C4x_inv, C4y_inv, C4z_inv}
C5 = {C2a, C2b, C2c, C2d, C2e, C2f}

Reps:
A1, A2, E, T1, T2
"""

OCTAHEDRAL_MULTIPLICATION_TABLE = {
    'E': 'E',
    'C4y': 'C4y',
    'C4z': 'C4z',
    'C4x': 'C4x',
    'C2x': 'C4x C4x',
    'C2y': 'C4y C4y',
    'C2z': 'C4z C4z',
    'C3alpha': 'C4y_inv C4z',
    'C3beta': 'C4y C4z_inv',
    'C3gamma': 'C4y_inv C4z_inv',
    'C3delta': 'C4y C4z',
    'C2a': 'C2y C4z',
    'C2b': 'C2x C4z',
    'C2c': 'C4y C2z',
    'C2d': 'C2z C4y',
    'C2e': 'C2z C4x',
    'C2f': 'C2y C4x'
}

OCTAHEDRAL_REPS = ['A1', 'A2', 'E', 'T1', 'T2']


def octahedral_group_multiplication_rules(C4y, C4z):
    # multiplication rules for Octahedral group using generators C4y and C4z

    def multiplication_rules(element):
        # table on page
        if element == 'E':
            return np.identity(len(C4y))
        elif element == 'C4y':
            return C4y
        elif element == 'C4z':
            return C4z
        elif element == 'C4x':
            A = np.linalg.inv(C4z)
            B = C4y
            C = C4z
            return np.matmul(np.matmul(A, B), C)
        elif 'inv' in element:
            return np.linalg.inv(multiplication_rules(element[:-4]))
        else:
            a, b = OCTAHEDRAL_MULTIPLICATION_TABLE[element].split(' ')
            A = multiplication_rules(a)
            B = multiplication_rules(b)
            return np.matmul(A, B)

    return multiplication_rules


def get_octahedral_conjugacy_classes():
    C1 = ['E']

    C2 = [f'C3{alpha}' for alpha in ['alpha', 'beta', 'gamma', 'delta']]
    C2.extend([f'{element}_inv' for element in C2])

    C3 = [f'C2{x}' for x in ['x', 'y', 'z']]

    C4 = [f'C4{x}' for x in ['x', 'y', 'z']]
    C4.extend([f'{element}_inv' for element in C4])

    C5 = [f'C2{a}' for a in ['a', 'b', 'c', 'd', 'e', 'f']]

    conjugacy_classes = {
        'C1': C1,
        'C2': C2,
        'C3': C3,
        'C4': C4,
        'C5': C5,
    }
    return conjugacy_classes.copy()


def get_octahedral_representations():
    b = np.sqrt(3)
    representations = {}
    representations['A1_rep'] = {'C4y': np.array([[1]]), 'C4z': np.array([[1]])}
    representations['A2_rep'] = {'C4y': np.array([[-1]]), 'C4z': np.array([[-1]])}
    representations['E_rep'] = {'C4y': 0.5 * np.array([[1, b], [b, -1]]),
                                'C4z': np.array([[-1, 0], [0, 1]])}
    representations['T1_rep'] = {'C4y': np.array([[0, 0, 1], [0, 1, 0], [-1, 0, 0]]),
                                 'C4z': np.array([[0, -1, 0], [1, 0, 0], [0, 0, 1]])}
    representations['T2_rep'] = {'C4y': np.array([[0, 0, -1,], [0, -1, 0], [1, 0, 0]]),
                                 'C4z': np.array([[0, 1, 0], [-1, 0, 0], [0, 0, -1]])}
    return representations.copy()


def get_octahedral_character_table():
    character_table = {}
    character_table['A1_rep'] = {'C1': 1, 'C2': 1, 'C3': 1, 'C4': 1, 'C5': 1}
    character_table['A2_rep'] = {'C1': 1, 'C2': 1, 'C3': 1, 'C4': -1, 'C5': -1}
    character_table['E_rep'] = {'C1': 2, 'C2': -1, 'C3': 2, 'C4': 0, 'C5': 0}
    character_table['T1_rep'] = {'C1': 3, 'C2': 0, 'C3': -1, 'C4': 1, 'C5': -1}
    character_table['T2_rep'] = {'C1': 3, 'C2': 0, 'C3': -1, 'C4': -1, 'C5': 1}
    return character_table.copy()


def get_gamma(basis):
    if basis == 'Dirac_Euclidean':
        gamma_matrices = get_Dirac_Euclidean_basis()
    elif basis == 'DR_Euclidean':
        gamma_matrices = get_DeGrand_Rossi_Euclidean_basis()
    elif basis == 'Dirac_Minkowski':
        gamma_matrices = get_Dirac_Minkowski_basis()
    elif basis == 'QDP_Euclidean':
        gamma_matrices = get_QDP_Euclidean_basis()
    elif basis == 'Weyl_Euclidean':
        gamma_matrices = get_Weyl_Euclidean_basis()
    else:
        exit_code = "Don't recognize name of gamma matrix basis -- " \
                    "try 'Dirac Euclidean', 'DR_Euclidean', 'Weyl_Euclidean', or 'Dirac_Minkowski'"
        print(exit_code)
        exit()
        return
    return gamma_matrices.copy()


def get_octahedral_group_spinor_rotations(basis):
    gamma_matrices = get_gamma(basis)
    I = np.identity(4)
    g1 = gamma_matrices['g1']
    g2 = gamma_matrices['g2']
    g3 = gamma_matrices['g3']
    g4 = gamma_matrices['g4']

    g2g3 = np.matmul(g2, g3)
    g3g1 = np.matmul(g3, g1)
    g1g2 = np.matmul(g1, g2)

    spinor_rotations = {}
    spinor_rotations['E'] = I

    spinor_rotations['C4x'] = 1/np.sqrt(2) * (I + g2g3)
    spinor_rotations['C4y'] = 1/np.sqrt(2) * (I + g3g1)
    spinor_rotations['C4z'] = 1/np.sqrt(2) * (I + g1g2)

    # inverses are needed to check C3's because according to Morningstar,
    # Gamma(R)^-1 != Gamma(R^-1) for spinor reps
    spinor_rotations['C4x_inv'] = 1/np.sqrt(2) * (I - g2g3)
    spinor_rotations['C4y_inv'] = 1/np.sqrt(2) * (I - g3g1)
    spinor_rotations['C4z_inv'] = 1/np.sqrt(2) * (I - g1g2)

    spinor_rotations['C2x'] = g2g3
    spinor_rotations['C2y'] = g3g1
    spinor_rotations['C2z'] = g1g2

    spinor_rotations['C3alpha']     = 1/2 * (I - g2g3 - g3g1 + g1g2)
    spinor_rotations['C3alpha_inv'] = 1/2 * (I + g2g3 + g3g1 - g1g2)

    spinor_rotations['C3beta']     = 1/2 * (I - g2g3 + g3g1 - g1g2)
    spinor_rotations['C3beta_inv'] = 1/2 * (I + g2g3 - g3g1 + g1g2)

    spinor_rotations['C3gamma']     = 1/2 * (I + g2g3 - g3g1 - g1g2)
    spinor_rotations['C3gamma_inv'] = 1/2 * (I - g2g3 + g3g1 + g1g2)

    spinor_rotations['C3delta']     = 1/2 * (I + g2g3 + g3g1 + g1g2)
    spinor_rotations['C3delta_inv'] = 1/2 * (I - g2g3 - g3g1 - g1g2)

    spinor_rotations['C2a'] = 1/np.sqrt(2) * (g2g3 + g3g1)
    spinor_rotations['C2b'] = 1/np.sqrt(2) * (g2g3 - g3g1)
    spinor_rotations['C2c'] = 1/np.sqrt(2) * (g2g3 + g1g2)
    spinor_rotations['C2d'] = -1/np.sqrt(2) * (g2g3 - g1g2)
    spinor_rotations['C2e'] = 1/np.sqrt(2) * (g3g1 + g1g2)
    spinor_rotations['C2f'] = 1/np.sqrt(2) * (g3g1 - g1g2)

    # Gamma(Is) = +/- Gamma(E) - this is already true, checked in Checks/test_double_octahedral_group.py
    # here we are talking about S^-1(G), given in equation 3.109 as g4
    spinor_rotations['Is'] = g4

    return spinor_rotations.copy()


def main():
    C4y = np.array([[2,1],[2,2]])
    C4z = np.array([[3,1],[2,4]])

    octahedral_mult_rules = octahedral_group_multiplication_rules(C4y, C4z)
    C2f = octahedral_mult_rules('C2f')

    C2f_by_hand = np.matmul(C4y, C4y)
    C2f_by_hand = np.matmul(C2f_by_hand, np.linalg.inv(C4z))
    C2f_by_hand = np.matmul(C2f_by_hand, C4y)
    C2f_by_hand = np.matmul(C2f_by_hand, C4z)
    print(C2f_by_hand)

    print(C2f)

if __name__ == "__main__":
    main()