import numpy as np
from Group_theory_definitions.octahedral import \
    octahedral_group_multiplication_rules, \
    get_octahedral_conjugacy_classes, \
    get_octahedral_representations, \
    get_octahedral_character_table, \
    OCTAHEDRAL_MULTIPLICATION_TABLE

"""
48 elements 
Hardwired point group classes
C1 = {E}
C2 = {C3alpha, C3beta, C3gamma, C3delta} + inverses calculated with numpy
C3 = {C2x, C2y, C2z}
C4 = {C4x, C4y, C4z} + inverses calculated with numpy 
C5 = {C2a, C2b, C2c, C2d, C2e, C2f}
C6 = {Is}
C7 = {Is_C3alpha, Is_C3beta, Is_C3gamma, Is_C3delta} + inverses calculated with numpy
C8 = {Is_C2x, Is_C2y, Is_C2z}
C9 = {Is_C4x, Is_C4y, Is_C4z} + inverses calculated with numpy 
C10 = {Is_C2a, Is_C2b, Is_C2c, Is_C2d, Is_C2e, Is_C2f}

Reps:
A1g, A2g, Eg, T1g, T2g ('gerade' AKA even)
A1u, A2u, Eu, T1u, T2u ('ungerade' AKA odd)

rep g reps Gamma(Is) = + Gamma(E)  ->  Gamma(Is_G) = + Gamma(G) 
rep u reps Gamma(Is) = - Gamma(E)  ->  Gamma(Is_G) = - Gamma(G) 
"""

POINT_MULTIPLICATION_TABLE = OCTAHEDRAL_MULTIPLICATION_TABLE.copy()
for key in OCTAHEDRAL_MULTIPLICATION_TABLE.keys():
    POINT_MULTIPLICATION_TABLE[f'Is_{key}'] = f'Is {key}'

POINT_REPS = ['A1g', 'A2g', 'Eg', 'T1g', 'T2g',
              'A1u', 'A2u', 'Eu', 'T1u', 'T2u']

def point_group_multiplication_rules(Is, C4y, C4z):
    # multiplication rules for point group using generators Is, C4y and C4z
    octahedral_mult_rules = octahedral_group_multiplication_rules(C4y, C4z)

    def multiplication_rules(element):
        if element == 'E':
            return np.identity(len(C4y))
        elif element == 'Is':
            return Is
        elif element == 'C4y':
            return C4y
        elif element == 'C4z':
            return C4z
        elif element == 'C4x':
            A = np.linalg.inv(C4z)
            B = C4y
            C = C4z
            return np.matmul(np.matmul(A, B), C)
        elif 'inv' in element:
            return np.linalg.inv(multiplication_rules(element[:-4]))
        else:
            a, b = POINT_MULTIPLICATION_TABLE[element].split(' ')
            A = multiplication_rules(a)
            B = multiplication_rules(b)
            return np.matmul(A, B)

    return multiplication_rules


def get_point_conjugacy_classes():
    octahedral_conjugacy_classes = get_octahedral_conjugacy_classes()
    C1 = octahedral_conjugacy_classes['C1']
    C2 = octahedral_conjugacy_classes['C2']
    C3 = octahedral_conjugacy_classes['C3']
    C4 = octahedral_conjugacy_classes['C4']
    C5 = octahedral_conjugacy_classes['C5']

    C6 = ['Is']
    C7 = ['Is_' + element for element in C2]
    C8 = ['Is_' + element for element in C3]
    C9 = ['Is_' + element for element in C4]
    C10 = ['Is_' + element for element in C5]

    conjugacy_classes = {
        'C1': C1,
        'C2': C2,
        'C3': C3,
        'C4': C4,
        'C5': C5,
        'C6': C6,
        'C7': C7,
        'C8': C8,
        'C9': C9,
        'C10': C10
    }
    return conjugacy_classes


def get_point_representations():
    octahedral_reps = get_octahedral_representations()
    d_A1 = len(octahedral_reps['A1_rep']['C4y'])
    d_A2 = len(octahedral_reps['A2_rep']['C4y'])
    d_E = len(octahedral_reps['E_rep']['C4y'])
    d_T1 = len(octahedral_reps['T1_rep']['C4y'])
    d_T2 = len(octahedral_reps['T2_rep']['C4y'])
    A1_identity = np.identity(d_A1)
    A2_identity = np.identity(d_A2)
    E_identity = np.identity(d_E)
    T1_identity = np.identity(d_T1)
    T2_identity = np.identity(d_T2)

    representations = {}
    # g reps with Is = E
    representations['A1g_rep'] = {'C4y': octahedral_reps['A1_rep']['C4y'],
                                  'C4z': octahedral_reps['A1_rep']['C4z'],
                                  'Is': A1_identity}
    representations['A2g_rep'] = {'C4y': octahedral_reps['A2_rep']['C4y'],
                                  'C4z': octahedral_reps['A2_rep']['C4z'],
                                  'Is': A2_identity}
    representations['Eg_rep'] = {'C4y': octahedral_reps['E_rep']['C4y'],
                                 'C4z': octahedral_reps['E_rep']['C4z'],
                                 'Is': E_identity}
    representations['T1g_rep'] = {'C4y': octahedral_reps['T1_rep']['C4y'],
                                  'C4z': octahedral_reps['T1_rep']['C4z'],
                                  'Is': T1_identity}
    representations['T2g_rep'] = {'C4y': octahedral_reps['T2_rep']['C4y'],
                                  'C4z': octahedral_reps['T2_rep']['C4z'],
                                  'Is': T2_identity}

    # u reps with Is = -E
    representations['A1u_rep'] = {'C4y': octahedral_reps['A1_rep']['C4y'],
                                  'C4z': octahedral_reps['A1_rep']['C4z'],
                                  'Is': -A1_identity}
    representations['A2u_rep'] = {'C4y': octahedral_reps['A2_rep']['C4y'],
                                  'C4z': octahedral_reps['A2_rep']['C4z'],
                                  'Is': -A2_identity}
    representations['Eu_rep'] = {'C4y': octahedral_reps['E_rep']['C4y'],
                                 'C4z': octahedral_reps['E_rep']['C4z'],
                                 'Is': -E_identity}
    representations['T1u_rep'] = {'C4y': octahedral_reps['T1_rep']['C4y'],
                                  'C4z': octahedral_reps['T1_rep']['C4z'],
                                  'Is': -T1_identity}
    representations['T2u_rep'] = {'C4y': octahedral_reps['T2_rep']['C4y'],
                                  'C4z': octahedral_reps['T2_rep']['C4z'],
                                  'Is': -T2_identity}

    return representations


def get_point_character_table():
    octahedral_table = get_octahedral_character_table().copy()
    character_table = {}
    for rep_name in octahedral_table.keys():
        g_rep_name = 'g_'.join(rep_name.split('_'))
        u_rep_name = 'u_'.join(rep_name.split('_'))

        # first 5 classes are same as octahedral
        character_table[g_rep_name] = octahedral_table[rep_name].copy()
        character_table[u_rep_name] = octahedral_table[rep_name].copy()

        # last 5 classes are Is x element
        for class_num in np.arange(1,6):
            octahedral_character = octahedral_table[rep_name][f'C{class_num}']
            # for g reps, Is = E, so elements have same character
            character_table[g_rep_name][f'C{class_num + 5}'] = octahedral_character
            # for u reps, Is = -E, so elements have negative character
            character_table[u_rep_name][f'C{class_num + 5}'] = -octahedral_character
    return character_table

