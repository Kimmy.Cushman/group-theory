import numpy as np


# DeGrand-Rossi Euclidean
DRE_g4 = np.array([
    [0,0,1,0],
    [0,0,0,1],
    [1,0,0,0],
    [0,1,0,0]
])

DRE_g1 = np.array([
    [0,0,0,1j],
    [0,0,1j,0],
    [0,-1j,0,0],
    [-1j,0,0,0]
])

DRE_g2 = np.array([
    [0,0,0,-1],
    [0,0,1,0],
    [0,1,0,0],
    [-1,0,0,0]
])

DRE_g3 = np.array([
    [0,0,1j,0],
    [0,0,0,-1j],
    [-1j,0,0,0],
    [0,1j,0,0]
])

DRE_g5 = np.array([
    [1,0,0,0],
    [0,1,0,0],
    [0,0,-1,0],
    [0,0,0,-1]
])

DRE_C = np.array([
    [0,-1,0,0],
    [1,0,0,0],
    [0,0,0,1],
    [0,0,-1,0]
])

IDENTITY = np.array([
    [1,0,0,0],
    [0,1,0,0],
    [0,0,1,0],
    [0,0,0,1]
])



def check_Clifford():
    Clifford = True
    if not np.allclose(np.matmul(DRE_g1, DRE_g1), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(DRE_g2, DRE_g2), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(DRE_g3, DRE_g3), IDENTITY):
        Clifford = False
    if not np.allclose(np.matmul(DRE_g4, DRE_g4), IDENTITY):
        Clifford = False
    return Clifford


def check_g5():
    g5_definition = True
    # g5 = g1 g2 g3 g4
    g5_mult = np.matmul(DRE_g1, DRE_g2)
    g5_mult = np.matmul(g5_mult, DRE_g3)
    g5_mult = np.matmul(g5_mult, DRE_g4)
    if not np.allclose(g5_mult, DRE_g5):
        g5_definition = False
    return g5_definition


def CC_gamma(C, gamma):
    C_inv = np.linalg.inv(C)
    prod = np.matmul(C, gamma)
    prod = np.matmul(prod, C_inv)
    return prod


def check_charge_conjugation_rule():
    CC_rule = True
    if not np.allclose(CC_gamma(DRE_C, DRE_g1), -DRE_g1.T):
        CC_rule = False
    if not np.allclose(CC_gamma(DRE_C, DRE_g2), -DRE_g2.T):
        CC_rule = False
    if not np.allclose(CC_gamma(DRE_C, DRE_g3), -DRE_g3.T):
        CC_rule = False
    if not np.allclose(CC_gamma(DRE_C, DRE_g4), -DRE_g4.T):
        CC_rule = False
    return CC_rule


def get_DeGrand_Rossi_Euclidean_basis():
    if check_Clifford() and check_g5() and check_charge_conjugation_rule():
        basis = {}
        basis["g1"] = DRE_g1
        basis["g2"] = DRE_g2
        basis["g3"] = DRE_g3
        basis["g4"] = DRE_g4
        basis["g5"] = DRE_g5
        basis["C"] = DRE_C
        return basis
    else:
        print("problem with basis")
        return




def main():
    basis = get_DeGrand_Rossi_Euclidean_basis()
    C = basis['C']
    g5 = basis['g5']
    print('g5')
    print(g5)
    print('Cg5')
    print(np.matmul(C,g5))
    print('g4')
    print(basis['g4'])


if __name__ == '__main__':
    main()


