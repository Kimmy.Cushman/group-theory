import pickle
from baryon_oo import save_matrix_from_dict


def get_operators_1234_T2g(basis, choice):
    ops = []
    if basis == 'DR_Euclidean':
        if choice == 0:
            ops = {
                # as is
                # {'0001': 0.5, '0111': 0.5, '2223': 0.5, '2333': 0.5},
                # {'0001': 0.5, '0111': -0.5, '2223': 0.5, '2333': -0.5},
                # {'0000': 0.5, '1111': -0.5, '2222': 0.5, '3333': -0.5}
                # diagonalized
                1: {'0111': 1, '2333': 1},
                -1: {'0001': 1, '2223': 1},
                2: {'0000': 1, '1111': -1, '2222': 1, '3333': -1}
            }
        if choice == 1:
            ops = {
                # as is
                # {'0003': 0.15811388300841897, '0012': 0.4743416490252569, '0113': 0.4743416490252569,
                #  '0223': 0.4743416490252569, '0333': 0.15811388300841897, '1112': 0.15811388300841897,
                #  '1222': 0.15811388300841897, '1233': 0.4743416490252569},
                # {'0003': 0.15811388300841897, '0012': 0.4743416490252569, '0113': -0.4743416490252569,
                #  '0223': 0.4743416490252569, '0333': -0.15811388300841897, '1112': -0.15811388300841897,
                #  '1222': 0.15811388300841897, '1233': -0.4743416490252569},
                # {'0002': 0.5, '0222': 0.5, '1113': -0.5, '1333': -0.5},
                # diagonalized
                1: {'0113': 3, '0333': 1, '1112': 1, '1233': 3},
                -1: {'0003': 1, '0012': 3, '0223': 3, '1222': 1},
                2: {'0002': 1, '0222': 1, '1113': -1, '1333': -1}
            }
        if choice == 2:
            ops = {
                # as is
                # {'0023': 0.5, '0122': 0.5, '0133': 0.5, '1123': 0.5},
                # {'0023': 0.5, '0122': 0.5, '0133': -0.5, '1123': -0.5},
                # {'0022': 0.7071067811865475, '1133': -0.7071067811865475}
                # diagonalized
                1: {'0133': 1, '1123': 1},
                -1: {'0023': 1, '0122': 1},
                2: {'0022': 1, '1133': -1}
            }
    return ops


def main():
    Nc = 4
    isospin_rep = '1234'
    rep = 'T2g'
    basis = 'DR_Euclidean'
    Sz_list = [1, -1, 2]  # note the order is specific to above, can be checked with diagonalize.py
    ops_dict = {}
    for choice in [0, 1, 2]:
        ops_dict[choice] = {}
        for Sz in Sz_list:
            ops_dict[choice][Sz] = get_operators_1234_T2g(basis, choice)[Sz]
            filename = f'Operators/{basis}/gamma_matrix_Nc{Nc}_isospin{isospin_rep}_{rep}_Sz{Sz}_choice{choice}.txt'
            save_matrix_from_dict(ops_dict[choice][Sz], filename)
    with open(f'Operators/{basis}/diagonalized_1234_T2g.pkl', 'wb') as f:
        pickle.dump(ops_dict, f)


if __name__ == '__main__':
    main()
