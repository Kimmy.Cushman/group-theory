import numpy as np
import sys
import itertools
import scipy
import sympy
import scipy.linalg
from Group_theory_definitions.octahedral import get_gamma
from rearrange_quarks import get_primitives_from_elemental

def get_bE(Gamma):
    mult = np.matmul(np.matmul(np.matmul(Gamma, Gamma), Gamma), Gamma)
    return np.around(mult, 8)

a = np.sqrt(2)
b = np.sqrt(3)

#
E_C4y = 1/2 * np.array([[1, b], [b, -1]])
T1_C4y = np.array([[0,0,1],[0,1,0],[-1,0,0]])
T2_C4y = np.array([[0,0,-1],[0,-1,0],[1,0,0]])



# Extra reps
G1_C4y = 1/a * np.array([[1, -1], [1, 1]])
G2_C4y = - 1/a * np.array([[1, -1], [1, 1]])
H_C4y = 1/(2*a) * np.array([
    [1, -b, b, -1],
    [b, -1, -1, b],
    [b, 1, -1, -b],
    [1, b, b, 1]
])

print('Regular reps - should be + identity')
print('E')
print(get_bE(E_C4y))
print('T1')
print(get_bE(T1_C4y))
print('T2')
print(get_bE(T2_C4y))


print('Extra reps - should be - identity')
print('G1')
print(get_bE(G1_C4y))
print('G2')
print(get_bE(G2_C4y))
print('H')
print(get_bE(H_C4y))
