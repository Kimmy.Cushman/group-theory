import numpy as np
from Group_theory_definitions.octahedral import \
    octahedral_group_multiplication_rules, \
    get_octahedral_conjugacy_classes,\
    get_octahedral_representations, \
    get_octahedral_character_table, \
    get_octahedral_group_spinor_rotations, \
    OCTAHEDRAL_MULTIPLICATION_TABLE


def test_pass_message(pass_test):
    if pass_test:
        print(':)  :)  :)  :)  :)  :)  :)  :)  :)  :)  :)')
        print('             P A S S E D                  ')
        print(':)  :)  :)  :)  :)  :)  :)  :)  :)  :)  :)')
    else:
        print(':(  :(  :(  :(  :(  :(  :(  :(  :(  :(  :(')
        print('             F A I L E D                  ')
        print(':(  :(  :(  :(  :(  :(  :(  :(  :(  :(  :(')
    print()
    print()


def test_character_table(octahedral_representations, conjugacy_classes, octahedral_character_table, tol):
    print()
    print()
    print('??????????????????????????????????????????')
    print('          TEST CHARACTER TABLE            ')
    print('??????????????????????????????????????????')
    pass_test = True

    for rep in octahedral_representations.keys():
        C4y_rep = octahedral_representations[rep]['C4y']
        C4z_rep = octahedral_representations[rep]['C4z']
        octahedral_mult_rules = octahedral_group_multiplication_rules(C4y_rep, C4z_rep)
        print(rep)
        print('---------------')
        for class_name, conjugacy_class in conjugacy_classes.items():
            character = octahedral_character_table[rep][class_name]
            for element in conjugacy_class:
                element_rep = octahedral_mult_rules(element)

                character_diff = character - element_rep.trace()
                if abs(character_diff) < tol:
                    pass
                    # print(f'{element} GOOD')
                else:
                    print(f'Class:{class_name}, element:{element} ----!!  FAIL !! ---- '
                          f'got: {element_rep.trace()}, should be: {character}')
                    pass_test = False

    test_pass_message(pass_test)


def test_octahedral_spinor_homomorphism():
    """
    psi' = [S(R1)]^-1 psi
    psi'' = [S(R2)]^-1 [S(R1)]^-1 psi
    But [S(R2)]^-1 [S(R1)]^-1 = [S(R1) S(R2)]^-1
    Is this the same as applying [S(R1 R2)]^-1 ?
    Want to show
    [S(R2)]^-1 [S(R1)]^-1 = [S(R1 R2)]^-1
    below, use shorthand S instead of S^-1
    So, want to show
    S(R2) S(R1) = S(R1 R2)
    using multiplication rules
    :return: test message
    """
    print()
    print()
    print('??????????????????????????????????????????')
    print('        TEST SPINOR HOMOMORPHISM          ')
    print('??????????????????????????????????????????')
    pass_test = True

    basis = 'Dirac_Euclidean'
    spinor_rotations = get_octahedral_group_spinor_rotations(basis)

    for element, multiplication in OCTAHEDRAL_MULTIPLICATION_TABLE.items():
        if len(multiplication.split(' ')) == 1:
            continue
        else:
            S_R1R2 = spinor_rotations[element]
            R1, R2 = multiplication.split(' ')
            S_R1 = spinor_rotations[R1]
            S_R2 = spinor_rotations[R2]

            # note the ordering here is correct, see comment string above
            SR1_SR2 = np.matmul(S_R2, S_R1)
            if np.allclose(S_R1R2, SR1_SR2, atol=1e-8, rtol=1e-8):
                pass
            else:
                pass_test = False
                print(f'problem with {element} = {R1} x {R2}')

    test_pass_message(pass_test)
    return pass_test


def test_spinor_table():
    """
    Discussion after 3.109 on page 68. Gamma(R)^-1 = Gamma(R^-1) only true for double octahedral,
    but here it is shown that they are all equal?
    :return:
    """
    print()
    print()
    print('??????????????????????????????????????????')
    print('           TEST SPINOR TABLE              ')
    print('??????????????????????????????????????????')
    pass_test = True

    basis = 'Dirac_Euclidean'
    spinor_rotations = get_octahedral_group_spinor_rotations(basis)
    for element_name, matrix in spinor_rotations.items():
        if 'inv' in element_name or '2' in element_name:
            pass
        elif element_name == 'E' or element_name == 'Is':
            pass
        else:
            matrix_inv = np.linalg.inv(matrix)
            table_matrix_inv = spinor_rotations[element_name + '_inv']
            if np.allclose(matrix_inv, table_matrix_inv):
                pass
            else:
                pass_test = False
                print(f'problem with {element_name} = {element_name}_inv')

    test_pass_message(pass_test)


def main():
    tol = 1e-8
    octahedral_representations = get_octahedral_representations()
    octahedral_character_table = get_octahedral_character_table()
    conjugacy_classes = get_octahedral_conjugacy_classes()
    test_character_table(octahedral_representations, conjugacy_classes, octahedral_character_table, tol)
    test_octahedral_spinor_homomorphism()
    test_spinor_table()


if __name__ == "__main__":
    main()



