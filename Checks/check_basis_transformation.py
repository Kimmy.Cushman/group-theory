import numpy as np
import pickle
import sys
sys.path.append('../group-theory')
from baryon_oo import find_kimmy_aaron_linear_combination


def main():
    # reconstruct basis1 ops from linear combination of basis2 ops
    basis1 = 'Weyl_to_Dirac'
    basis2 = 'Dirac_Euclidean'

    for Nc in [3, 4]:
        if Nc == 3:
            isospin_rep_list = [
                '123',
                '13_2',
                '12_3'
            ]
            rep_list = [
                'G1g_rep',
                'Hg_rep'
            ]
        else:
            isospin_rep_list = [
                '1234',
                '134_2',
                '124_3',
                '123_4',
                '12_34',
                '13_24'
            ]
            rep_list = [
                'A1g_rep',
                'Eg_rep',
                'T1g_rep',
                'T2g_rep',
            ]
        for isospin_rep in isospin_rep_list:
            for rep in rep_list:
                base_filename = f'Nc{Nc}_isospin{isospin_rep}'
                ops_file_basis1 = f'Operators/{basis1}/{base_filename}_{rep}_elemental_operator.pkl'
                ops_file_basis2 = f'Operators/{basis2}/{base_filename}_{rep}_elemental_operator.pkl'

                ops_basis1 = {}
                with open(ops_file_basis1, 'rb') as f:
                    ops_basis1.update(pickle.load(f))
                ops_basis2 = {}
                with open(ops_file_basis2, 'rb') as f:
                    ops_basis2.update(pickle.load(f))

                # reconstruct basis1 ops from linear combination of basis2 ops
                pass_check = find_kimmy_aaron_linear_combination(ops_basis1, ops_basis2, printing=False)
                if pass_check:
                    print(f'Isospon {isospin_rep} {rep} {basis1} {len(ops_basis1)}')
                else:
                    print(f'Isospin {isospin_rep} {rep} FAIL ')




if __name__ == '__main__':
    main()