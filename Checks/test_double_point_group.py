import numpy as np
import sys
sys.path.append('../group-theory')
from Group_theory_definitions.double_point import \
    double_point_group_multiplication_rules, \
    get_double_point_conjugacy_classes,\
    get_double_point_representation_generators, \
    get_double_point_character_table

from test_octahedral_group import test_pass_message
from test_double_octahedral_group import test_spinorial_reps_bar_plus_or_minus
from test_point_group import test_parity_reps


def test_characters(point_representations, conjugacy_classes, point_character_table, tol):
    print()
    print()
    print('??????????????????????????????????????????')
    print('          TEST CHARACTER TABLE            ')
    print('??????????????????????????????????????????')
    pass_test = True
    representation_dict = {}
    for rep in point_representations.keys():
        representation_dict[rep] = {}
        C4y_rep = point_representations[rep]['C4y']
        C4z_rep = point_representations[rep]['C4z']
        Is_rep = point_representations[rep]['Is']
        double_point_mult_rules = double_point_group_multiplication_rules(Is_rep, C4y_rep, C4z_rep)
        print(rep)
        print('---------------')
        for class_name, conjugacy_class in conjugacy_classes.items():
            character = point_character_table[rep][class_name]
            for element in conjugacy_class:
                element_rep = double_point_mult_rules(element)
                representation_dict[rep][element] = element_rep
                character_diff = character - element_rep.trace()
                if abs(character_diff) < tol:
                    pass
                    # print(f'Class:{class_name}, element:{element} GOOD')
                else:
                    print(f'Class:{class_name}, element:{element} ----!!  FAIL !! ---- '
                          f'got: {element_rep.trace()}, should be: {character}')
                    pass_test = False

    test_pass_message(pass_test)
    return representation_dict




def main():
    tol = 1e-8
    # reps: A1 A2 E T1 T2 and G1 G2 H all with u and g versions
    # elements: 96 total (octahedral + bar{octahedral})(1 and Is)
    point_representations = get_double_point_representation_generators()
    point_character_table = get_double_point_character_table()
    conjugacy_classes = get_double_point_conjugacy_classes()

    representation_dict = test_characters(point_representations,
                                          conjugacy_classes,
                                          point_character_table,
                                          tol)
    # Test to make sure Gamma(G) = +\- Gamma(bG), + for regular reps, - for extra reps
    test_spinorial_reps_bar_plus_or_minus(representation_dict,
                                          conjugacy_classes,
                                          tol, group='double_point')

    # test Gamma(IsG) = +/- Gamma(Is) for g/u reps
    test_parity_reps(representation_dict, point_representations, conjugacy_classes, tol, group='double_point')


if __name__ == "__main__":
    main()