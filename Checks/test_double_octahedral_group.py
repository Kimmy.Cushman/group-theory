import numpy as np
import sys
sys.path.append('../group-theory')
from Group_theory_definitions.double_octahedral import \
    double_octahedral_group_multiplication_rules, \
    get_double_octahedral_conjugacy_classes,\
    get_double_octahedral_representation_generators, \
    get_double_octahedral_character_table
from Checks.test_octahedral_group import test_pass_message


def test_characters(octahedral_representations, conjugacy_classes, octahedral_character_table, tol):
    print()
    print()
    print('??????????????????????????????????????????')
    print('          TEST CHARACTER TABLE            ')
    print('??????????????????????????????????????????')
    pass_test = True
    representation_dict = {}
    for rep in octahedral_representations.keys():
        representation_dict[rep] = {}
        C4y_rep = octahedral_representations[rep]['C4y']
        C4z_rep = octahedral_representations[rep]['C4z']
        double_octahedral_mult_rules = double_octahedral_group_multiplication_rules(C4y_rep, C4z_rep)
        print(rep)
        print('---------------')
        for class_name, conjugacy_class in conjugacy_classes.items():
            character = octahedral_character_table[rep][class_name]
            for element in conjugacy_class:
                element_rep = double_octahedral_mult_rules(element)
                representation_dict[rep][element] = element_rep
                character_diff = character - element_rep.trace()
                if abs(character_diff) < tol:
                    pass
                else:
                    print(f'Class:{class_name}, element:{element} ----!!  FAIL !! ---- '
                          f'got: {element_rep.trace()}, should be: {character}')
                    pass_test = False

    test_pass_message(pass_test)
    return representation_dict


def test_spinorial_reps_bar_plus_or_minus(representation_dict, conjugacy_classes, tol, group):
    '''
    Goal is to test Gamma(\bar{G}) = +/- Gamma(G) for regular/extra reps
    :param representation_dict: dictionary where dict[rep][element] is the Gamma
    representation matrix of ALL rotation elements for a given rep
    :param conjugacy_classes: as given in get_double_octahedral_conjugacy_classes()
    :param tol: float
    :param group: string, "double_octahedral" or "double_point"
    :return: pass message
    '''
    regular_reps = ['A1', 'A2', 'E', 'T1', 'T2']
    extra_reps = ['G1', 'G2', 'H']
    if group == 'double_octahedral':
        regular_reps = [regular + '_rep' for regular in regular_reps]
        extra_reps = [extra + '_rep' for extra in extra_reps]
    if group == 'double_point':
        regular_reps = [regular + 'g_rep' for regular in regular_reps] + \
                       [regular + 'u_rep' for regular in regular_reps]
        extra_reps = [extra + 'g_rep' for extra in extra_reps] + \
                       [extra + 'u_rep' for extra in extra_reps]

    print()
    print()
    print('??????????????????????????????????????????')
    print('        TEST R(G) = +/- R(bG) REPS        ')
    print('??????????????????????????????????????????')
    pass_test = True
    for rep in representation_dict.keys():
        print('------------------------------------------')
        print(rep)
        print('------------------------------------------')
        for class_name, conjugacy_class in conjugacy_classes.items():
            for element in conjugacy_class:
                # print(element)
                if group == 'double_octahedral' and '_b' in element:
                    continue
                if group == 'double_point' and ('_b' in element or 'Is' in element):
                    # already checking Gamma(IsG) = +/- Gamma(G) in parity_reps function
                    continue

                bar_element = '_b' + element[1:]

                element_rep = np.array(representation_dict[rep][element])
                bar_element_rep = np.array(representation_dict[rep][bar_element])

                # regular reps should have Gamma(bar{G}) = Gamma(G)
                if rep in regular_reps:
                    diff = element_rep - bar_element_rep

                # extra reps should have Gamma(bar{G}) = -Gamma(G)
                elif rep in extra_reps:
                    diff = element_rep + bar_element_rep
                else:
                    print(f'cant find rep {rep}')
                    exit()
                    return

                d = len(diff)
                zero = np.zeros((d, d))
                if not np.allclose(diff, zero, rtol=tol, atol=tol):
                    pass_test = False
                    print(f'rep: {rep}, class: {class_name}, element: {element} ---- FAIL ----')
                    print('regular')
                    print(element_rep)
                    print('bar')
                    print(bar_element_rep)

    test_pass_message(pass_test)


def test_gamma_bar(double_octahedral_representations, conjugacy_classes):
    C4y_reg = double_octahedral_representations['T1_rep']['C4y']
    C4z_reg = double_octahedral_representations['T1_rep']['C4z']
    reg_mult_rules = double_octahedral_group_multiplication_rules(C4y_reg, C4z_reg)

    C4y_ext = double_octahedral_representations['G1_rep']['C4y']
    C4z_ext = double_octahedral_representations['G1_rep']['C4z']
    ext_mult_rules = double_octahedral_group_multiplication_rules(C4y_ext, C4z_ext)
    for class_name, conjugacy_class in conjugacy_classes.items():
        for element in conjugacy_class:
            if '_b' in element:
                reg_b = reg_mult_rules(element)
                reg = reg_mult_rules('_'.join(element.split('_b')))

                ext_b = ext_mult_rules(element)
                ext = ext_mult_rules('_'.join(element.split('_b')))

                if np.allclose(reg_b, reg) and np.allclose(ext, -ext_b):
                    pass
                else:
                    print(f'{element} FAIL')


def main():
    tol = 1e-8
    # reps: A1 A2 E T1 T2 and G1 G2 H
    # elements: 48 total - octahedral + bar{octahedral}
    double_octahedral_representation_generators = get_double_octahedral_representation_generators()
    double_octahedral_character_table = get_double_octahedral_character_table()
    conjugacy_classes = get_double_octahedral_conjugacy_classes()

    representation_dict = test_characters(double_octahedral_representation_generators,
                                          conjugacy_classes,
                                          double_octahedral_character_table,
                                          tol)

    # Test to make sure Gamma(G) = +\- Gamma(bG), + for regular reps, - for extra reps
    test_spinorial_reps_bar_plus_or_minus(representation_dict,
                                          conjugacy_classes,
                                          tol,
                                          group='double_octahedral')



if __name__ == "__main__":
    main()