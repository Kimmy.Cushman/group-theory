import numpy as np
from Group_theory_definitions.double_point import \
    get_double_point_conjugacy_classes, \
    double_point_group_multiplication_rules
from Group_theory_definitions.octahedral import get_octahedral_group_spinor_rotations

Nd = 4
SPIN_REPS = {0: 'A1',
             1: 'T1',
             2: 'E T2',
             0.5: 'G1',
             1.5: 'H'}


def check_M(elemental_states, basis, get_W_function):
    spinor_rotations = get_octahedral_group_spinor_rotations(basis).copy()
    S_C4y = spinor_rotations['C4y']
    S_C4z = spinor_rotations['C4z']
    S_Is =  spinor_rotations['Is']
    multiplication_rules = double_point_group_multiplication_rules(get_W_function(S_Is),
                                                                   get_W_function(S_C4y),
                                                                   get_W_function(S_C4z))
    conjugacy_classes = get_double_point_conjugacy_classes()
    all_elements = []
    for class_name, conjugacy_class in conjugacy_classes.items():
        all_elements.extend(conjugacy_class)
    N_elemental_states = len(elemental_states)
    # to find M, page 138, multiplication rules for double point group given on page 86
    print('??????????????????????????????????????????')
    print('                TEST M                    ')
    print('??????????????????????????????????????????')
    M = np.zeros((N_elemental_states, N_elemental_states), dtype=np.cdouble)
    g_size_group = len(all_elements)
    print(f'g (size of group) = {g_size_group}')

    for element in all_elements:
        W_element_explicit = multiplication_rules(element)
        M += 1 / g_size_group * np.matmul(W_element_explicit.conjugate().T, W_element_explicit)

    if np.allclose(M, M.real):
        M = M.real
    print(np.around(3 * M, 3).astype(int))
