import numpy as np
import sys
sys.path.append('../group-theory')
from Group_theory_definitions.octahedral import \
    get_octahedral_group_spinor_rotations, \
    OCTAHEDRAL_MULTIPLICATION_TABLE
from Group_theory_definitions.point import POINT_MULTIPLICATION_TABLE
from get_W_helper import unitarize_W
from general_tools import readable_row
from projections import projection_setup
from Checks.test_octahedral_group import test_pass_message
from contextlib import contextmanager
import sys, os

@contextmanager
def suppress_stdout():
    with open(os.devnull, "w") as devnull:
        old_stdout = sys.stdout
        sys.stdout = devnull
        try:
            yield
        finally:
            sys.stdout = old_stdout


def check_W_base_elements(get_W_function):
    factors = {'C4y': 2*np.sqrt(2), 'C4z': np.sqrt(2), 'Is': 1}
    basis = 'Dirac_Euclidean'
    spinor_rotations = get_octahedral_group_spinor_rotations(basis)
    for rotation in ['C4y']: #, 'C4z', 'Is']:
        spinor_rep = spinor_rotations[rotation]
        W = get_W_function(spinor_rep)
        print(rotation)
        if np.allclose(W, W.real):
            W = W.real
        # W = np.matmul(W, W)
        print(np.around(factors[rotation] * W[:,:-3], 3))


def get_W_from_R_multiplication(R, get_W_function, spinor_rotations):
    if 'inv' in R:
        W_R = np.linalg.inv(get_W_function(spinor_rotations[R[:-4]]))
    elif '2' in R:
        Ra = '4'.join(R.split('2'))
        W_Ra = get_W_function(spinor_rotations[Ra])
        W_R = np.matmul(W_Ra, W_Ra)
    else:
        W_R = get_W_function(spinor_rotations[R])
    return W_R


def test_spinor_rotations_unitarity(basis):
    spinor_rotations = get_octahedral_group_spinor_rotations(basis).copy()
    for rotation, matrix in spinor_rotations.items():
        M_dag = np.array(matrix).transpose().conj()
        M_dag_M = np.matmul(M_dag, matrix)
        unitary = np.allclose(np.identity(len(matrix)), M_dag_M)
        if not unitary:
            print(f'{rotation} NOT UNITARY')


def test_spinor_octahedral_rotations_W_homomorphism(get_W_function, basis, rep, elemental_states, unitarize):
    _, _, _, _, _, _, M = projection_setup(basis, rep, get_W_function, elemental_states)
    print()
    print('??????????????????????????????????????????')
    print('           TEST W HOMOMORPHISM            ')
    print('??????????????????????????????????????????')
    pass_test = True
    spinor_rotations = get_octahedral_group_spinor_rotations(basis).copy()
    multiplication_table = OCTAHEDRAL_MULTIPLICATION_TABLE.copy()
    for element, multiplication in multiplication_table.items():
        if len(multiplication.split(' ')) == 1:
            continue
        else:
            W_R1R2 = get_W_function(spinor_rotations[element])
            # compute WR1 * WR2
            R1, R2 = multiplication.split(' ')
            W_R1 = get_W_function(spinor_rotations[R1])  # all necessary inverses are given in spinor_rotations dict
            W_R2 = get_W_function(spinor_rotations[R2])
            if unitarize:
                W_R1R2 = unitarize_W(W_R1R2, M)
                W_R1 = unitarize_W(W_R1, M)
                W_R2 = unitarize_W(W_R2, M)
            WR1_WR2 = np.matmul(W_R1, W_R2)
            if np.allclose(W_R1R2, WR1_WR2, atol=1e-8, rtol=1e-8):
                print(f'{element} = {R1} x {R2} passed :)')
            else:
                pass_test = False
                print(f'problem with {element} = {R1} x {R2}')
    test_pass_message(pass_test)


def test_spinor_homomorphism(basis):
    # THIS SHOULD FAIL
    spinor_rotations = get_octahedral_group_spinor_rotations(basis).copy()
    multiplication_table = OCTAHEDRAL_MULTIPLICATION_TABLE.copy()
    print('??????????????????????????????????????????')
    print('          TEST S^-1 HOMOMORPHISM          ')
    print('??????????????????????????????????????????')
    pass_test = True
    for element, multiplication in multiplication_table.items():
        if len(multiplication.split(' ')) == 1:
            continue
        else:
            S_R1R2 = spinor_rotations[element]
            # compute SR1 * SR2
            R1, R2 = multiplication.split(' ')
            S_R1 = spinor_rotations[R1]
            S_R2 = spinor_rotations[R2]
            SR1_SR2 = np.matmul(S_R1, S_R2)
            if np.allclose(S_R1R2, SR1_SR2, atol=1e-8, rtol=1e-8):
                print(f'{element} = {R1} x {R2} passed :)')
            else:
                pass_test = False
                print(f'problem with {element} = {R1} x {R2}')
    test_pass_message(pass_test)


def main():
    test_spinor_homomorphism('Dirac_Euclidean')  # THIS SHOULD FAIL


if __name__ == '__main__':
    main()
