import numpy as np
import sys
sys.path.append('../group-theory')
from Group_theory_definitions.point import \
    point_group_multiplication_rules, \
    get_point_conjugacy_classes,\
    get_point_representations, \
    get_point_character_table
from test_octahedral_group import test_pass_message


def test_character_table(point_representations, point_character_table, conjugacy_classes, tol):
    print()
    print()
    print('??????????????????????????????????????????')
    print('          TEST CHARACTER TABLE            ')
    print('??????????????????????????????????????????')
    pass_test = True

    representation_dict = {}
    for rep in point_representations.keys():
        representation_dict[rep] = {}
        C4y_rep = point_representations[rep]['C4y']
        C4z_rep = point_representations[rep]['C4z']
        Is_rep = point_representations[rep]['Is']
        point_mult_rules = point_group_multiplication_rules(Is_rep, C4y_rep, C4z_rep)
        print(rep)
        print('---------------')
        for class_name, conjugacy_class in conjugacy_classes.items():
            character = point_character_table[rep][class_name]
            for element in conjugacy_class:
                element_rep = point_mult_rules(element)

                representation_dict[rep][element] = element_rep

                character_diff = character - element_rep.trace()
                if abs(character_diff) < tol:
                    pass
                    # print(f'{element} GOOD')
                else:
                    print(f'Class:{class_name}, element:{element} ----!!  FAIL !! ---- '
                          f'got: {element_rep.trace()}, should be: {character}')
                    pass_test = False

    test_pass_message(pass_test)
    return representation_dict


def test_parity_reps(representation_dict, representations, conjugacy_classes, tol, group):
    # test Gamma(IsG) = +/- Gamma(Is) for g/u reps
    reps = ['A1', 'A2', 'E', 'T1', 'T2']
    if group == 'double_point':
       reps = reps + ['G1', 'G2', 'H']
    even_reps = [rep + 'g_rep' for rep in reps]

    print()
    print()
    print('??????????????????????????????????????????')
    print('            TEST PARITY REPS              ')
    print('??????????????????????????????????????????')
    pass_test = True
    for rep in representations.keys():
        print('------------------------------------------')
        print(rep)
        print('------------------------------------------')
        for class_name, conjugacy_class in conjugacy_classes.items():
            for element in conjugacy_class:
                if group == 'point' and 'Is' in element:
                    continue
                if group == 'double_point' and ('_b' in element or 'Is' in element):
                    # already checking Gamma(bar{G}) = +/- Gamma(G) in spinor_reps function
                    continue
                if group == 'point':
                    # point group looks like G and Is_G
                    if element == 'E':
                        Is_element = 'Is'
                    else:
                        Is_element = 'Is_' + element
                else:
                    # double point looks like _G and Is_G
                    if element == '_E':
                        Is_element = 'Is'
                    else:
                        Is_element = 'Is' + element

                element_rep = np.array(representation_dict[rep][element])
                Is_element_rep = np.array(representation_dict[rep][Is_element])

                # even reps should have Gamma(Is G) = Gamma(G)
                if rep in even_reps:
                    diff = element_rep - Is_element_rep
                else:
                    # odd reps should have Gamma(Is G) = -Gamma(G)
                    diff = element_rep + Is_element_rep
                d = len(diff)
                zero = np.zeros((d, d))
                if not np.allclose(diff, zero, rtol=tol, atol=tol):
                    pass_test = False
                    print(f'rep: {rep}, class: {class_name}, element: {element} ---- FAIL ----')
                    print('regular')
                    print(element_rep)
                    print('bar')
                    print(Is_element_rep)

    test_pass_message(pass_test)


def main():
    tol = 1e-8
    point_representations = get_point_representations()
    point_character_table = get_point_character_table()
    conjugacy_classes = get_point_conjugacy_classes()
    representation_dict = test_character_table(point_representations, point_character_table, conjugacy_classes, tol)
    test_parity_reps(representation_dict, point_representations, conjugacy_classes, tol, group='point')


if __name__ == "__main__":
    main()



