import numpy as np



def readable_row(row, factor, precision=2):
    row = list(np.around(factor * row, precision))
    row = [elem.real if elem.imag == 0 else elem for elem in row]
    row = [abs(elem) if elem == 0.0 else elem for elem in row]  # avoids -0.0 terms
    row = [int(elem) if abs(elem - int(elem)) < 1e-8 else elem for elem in row]
    row = [str(elem) for elem in row]
    row = '   '.join([elem for elem in row])
    return row

def readable_row_nonzero(row):
    row = ['X' if abs(elem) > 1e-8 else '0' for elem in row]
    row = '   '.join([elem for elem in row])
    return row


def negative_op(operator):
    neg_op = {}
    for key, val in operator.items():
        neg_op[key] = -val
    return neg_op


