import numpy as np
import sympy
from Group_theory_definitions.double_point import \
    get_double_point_conjugacy_classes, \
    double_point_group_multiplication_rules, \
    get_double_point_representation_generators
from Group_theory_definitions.octahedral import get_octahedral_group_spinor_rotations
from get_W_helper import unitarize_W

ROUNDING = 15


def projection_setup(basis, rep, get_W_function, elemental_states):
    # W_{ji}(R)
    # get 4x4 spinor rotation matrices, applying Is=+/- if rep is even/odd
    octahedral_spinor_rotations = get_octahedral_group_spinor_rotations(basis).copy()
    S_C4y = octahedral_spinor_rotations['C4y']
    S_C4z = octahedral_spinor_rotations['C4z']
    S_Is =  octahedral_spinor_rotations['Is']
    W_mult_rules = double_point_group_multiplication_rules(get_W_function(S_Is),
                                                           get_W_function(S_C4y),
                                                           get_W_function(S_C4z))
    # \Gamma(R)
    Gamma_C4y = get_double_point_representation_generators()[rep]['C4y']
    Gamma_C4z = get_double_point_representation_generators()[rep]['C4z']
    Gamma_Is = get_double_point_representation_generators()[rep]['Is']
    Gamma_mult_rules = double_point_group_multiplication_rules(Gamma_Is, Gamma_C4y, Gamma_C4z)
    d_Lambda = len(Gamma_C4y)  # d_\Lambda
    all_elements = []
    conjugacy_classes = get_double_point_conjugacy_classes()
    for class_name, conjugacy_class in conjugacy_classes.items():
        all_elements.extend(conjugacy_class)
    g_OhD = len(all_elements)  # g_{O_h^D}
    N_elemental_states = len(elemental_states)

    M = np.zeros((N_elemental_states, N_elemental_states), dtype=np.cdouble)
    for element in all_elements:
        W = W_mult_rules(element)
        M += np.matmul(W.transpose().conj(), W)
    M = M / g_OhD

    return N_elemental_states, all_elements, Gamma_mult_rules, W_mult_rules, d_Lambda, g_OhD, M


def get_irrep_operators(basis, rep, get_W_function, elemental_states, unitarize):
    """
    calculating 1st row of irrep
    P_{ij}^{\Lambda \lambda} = d_\Lambda/g_{O_h^D} \sum_{R \in O_h^D} \Gamma_{\lambda\lambda}^\Lambda(R) W_{ji}(R)

    calculating other rows of irrep Equation 4.59 c -> P
    P_{ik}^\mu = \sum_j P_{ij}^\lambda d_\Lambda/g_{O_h^D} \sum_R \Gamma_{\mu \lambda}(R) W_{kj}(R),
    where P_{ij}^{\lambda=0} is the projection we have already done.
    Simplifying
    P_{ik}^\mu = d_\Lmabda/g_{O_h^D} \sum_R \Gamma_{\mu\lambda} (P^\lambda  W(R)^T)_{ik}
    """
    N_elemental_states, all_elements, Gamma_mult_rules, W_mult_rules, d_Lambda, g_OhD, M \
        = projection_setup(basis, rep, get_W_function, elemental_states)

    projection_0 = np.zeros((N_elemental_states, N_elemental_states), dtype=np.cdouble)
    for element in all_elements:
        Gamma_element = Gamma_mult_rules(element)[0, 0]  # \Gamma_{\lambda\lambda}, with \lambda=0
        W_element = W_mult_rules(element)
        if unitarize:
            W_element = unitarize_W(W_element, M)
        element_contribution = d_Lambda / g_OhD * Gamma_element * W_element.T
        projection_0 += element_contribution

    # # FIXME - W(R) != W(bar(R))
    # for element in all_elements:
    #     if '_b' not in element:
    #         continue
    #     bar_element = element
    #     original_element = '_'.join(element.split('_b'))
    #     W_element = W_mult_rules(original_element)
    #     W_bar_element = W_mult_rules(bar_element)
    #     if np.allclose(W_element, W_bar_element):
    #         print(f'W({bar_element}) = W({original_element})')
    #     else:
    #         print(f'W({bar_element})  NOT EQUAL TO  W({original_element})')
    # # exit()


    projection_matrices = [projection_0]
    for mu in range(1, d_Lambda):
        projection_mu = np.zeros((N_elemental_states, N_elemental_states), dtype=np.cdouble)
        for element in all_elements:
            Gamma_element = Gamma_mult_rules(element)[mu, 0]  # \Gamma_{\lambda\lambda}, with \lambda=0
            W_element = W_mult_rules(element)
            element_contribution = d_Lambda / g_OhD * Gamma_element * np.matmul(projection_0, W_element.T)
            projection_mu += element_contribution
        projection_matrices.append(projection_mu)
    all_irrep_rows = {}
    for mu in range(d_Lambda):
        all_irrep_rows = get_states_from_projection(all_irrep_rows,
                                                    mu,
                                                    projection_matrices[mu],
                                                    elemental_states)
    return all_irrep_rows


def get_integer_projection(projection):
    projection = np.around(projection, 10)
    vals_real = []
    vals_imag = []
    for row in projection:
        for val in row:
            if abs(val.real) > 1e-8:
                vals_real.append(val.real)
            if abs(val.imag) > 1e-8:
                vals_imag.append(val.imag)
    if len(vals_real) != 0:
        factor = 1 / np.min(vals_real)
    else:
        factor = 1 / np.min(vals_imag)
    return np.around(factor * projection, 8)


def gram_schmidt(A):
    A = np.array(A).transpose()
    (n, m) = A.shape
    for i in range(m):
        q = A[:, i]  # i-th column of A
        for j in range(i):
            q = q - np.dot(A[:, j], A[:, i]) * A[:, j]
        if np.array_equal(q, np.zeros(q.shape)):
            raise np.linalg.LinAlgError("The column vectors are not linearly independent")
        # normalize q
        q = q / np.sqrt(np.dot(q, q))
        # write the vector back in the matrix
        A[:, i] = q
    return A.transpose()


def get_states_from_projection(all_irrep_states, mu, projection, elemental_states):
    N_elemental_states = len(elemental_states)
    zero_operator = np.zeros(N_elemental_states)
    reduce = 1
    if reduce:
        projection = get_integer_projection(projection)
        rr_projection, independent_row_indices = sympy.Matrix(projection).rref()
        independent_row_indices = list(independent_row_indices)
        rr_projection = np.array(rr_projection).astype(np.cdouble)
        rr_projection = rr_projection[:len(independent_row_indices)]
    else:
        projection = np.around(projection, ROUNDING)
        rr_projection = []
        for row in projection:
            if not np.allclose(row, np.zeros(len(row), dtype=np.cdouble)):
                rr_projection.append(row)
        rr_projection = np.array(rr_projection)

    choice = 0
    # FIXME - this is a choice
    orthogonal_rows = gram_schmidt(rr_projection)
    # orthogonal_rows = rr_projection
    for operator_index, operator_vector in enumerate(orthogonal_rows):
        # FIXME - add this back
        operator_vector = operator_vector/np.linalg.norm(operator_vector)
        if np.allclose(zero_operator, operator_vector):
            continue
        dict_key = f'row {mu} choice {choice}'
        choice += 1
        all_irrep_states[dict_key] = {}
        for c, value in enumerate(operator_vector):
            if abs(value) > 1e-8:
                if abs(value.imag) < 1e-8:
                    value = value.real
                # going from elemental state indices to the actual spin indices
                elemental_string = elemental_states[c]
                try:
                    all_irrep_states[dict_key][elemental_string] += value
                except KeyError:
                    all_irrep_states[dict_key][elemental_string] = value
    return all_irrep_states
