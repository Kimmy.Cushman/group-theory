import numpy as np
import os
import sys

Nc3_isospins = [
    # '123',
    '13_2',
    # 'Morn_N',
    # '12_3'
]
Nc3_irreps = [
    'G1g_rep',
    # 'Hg_rep',
]

Nc4_isospins = [
    '1234',
    # '134_2',
    # '124_3',
    # '123_4',
    # '12_34',
    # '13_24'
]
Nc4_irreps = [
    # 'A1g_rep',
    'Eg_rep',
    # 'T1g_rep',
    # 'T2g_rep'
]

# basis = 'Dirac_Euclidean'
basis = 'DR_Euclidean'
unitarize = 0
choice = sys.argv[1]

def submit(Nc, isospins, irreps):
    for isospin in isospins:
        for rep in irreps:
            print(f'{Nc} {isospin} {rep} {basis}')
            os.system(f'python diagonalize.py {Nc} {isospin} {rep} {basis} {unitarize} {choice}')
            print()
        print()

# submit(3, Nc3_isospins, Nc3_irreps)
submit(4, Nc4_isospins, Nc4_irreps)