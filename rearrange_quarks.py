import numpy as np
import itertools
import sympy

FLAVOR_WAVEFUNCTIONS = {
    '123': [['uuu'], [1]],
    '13_2': [['udu', 'duu'], [1, -1]],
    'Morn_N': [['uud', 'duu'], [1, -1]],
    '12_3': [['uud', 'udu', 'duu'], [2, -1, -1]],
    '1234': [['uuuu'], [1]],
    '134_2': [['uduu', 'duuu'], [1, -1]],
    '124_3': [['uudu', 'uduu', 'duuu'], [2, -1, -1]],
    '123_4': [['uuud', 'uudu', 'uduu', 'duuu'], [3, -1, -1, -1]],
    '13_24': [['udud', 'uddu', 'duud', 'dudu'], [1, -1, -1, 1]],
    '12_34': [['uudd', 'dduu', 'udud', 'uddu', 'duud', 'dudu'], [2, 2, -1, -1, -1, -1]]
}


def sort_uudd(spins):
    s1, s2, s3, s4 = spins
    s1_s2 = [s1, s2]
    s3_s4 = [s3, s4]
    s1_s2.sort()
    s3_s4.sort()
    return ''.join(s1_s2 + s3_s4)

def sort_uuud(spins):
    s1, s2, s3, s4 = spins
    s1_s2_s3 = [s1, s2, s3]
    s1_s2_s3.sort()
    return ''.join(s1_s2_s3 + [s4])


def sort_uud(spins):
    s1, s2, s3 = spins
    s1_s2 = [s1, s2]
    s1_s2.sort()
    return ''.join(s1_s2 + [s3])


def get_symmetric_spins(spins):
    spins = [int(s) for s in spins]
    spins.sort()
    return ''.join([str(s) for s in spins])


def get_uudd(flavors, spins):
    a, b, c, d = spins
    # convert flavor -> uudd
    if flavors == 'udud':
        term = sort_uudd([a, c, b, d])
    elif flavors == 'uddu':
        term = sort_uudd([a, d, b, c])
    elif flavors == 'duud':
        term = sort_uudd([b, c, d, a])
    elif flavors == 'dudu':
        term = sort_uudd([d, b, a, c])
    elif flavors == 'dduu':
        term = sort_uudd([c, d, a, b])
    else: # flavor = 'uudd'
        term = sort_uudd([a, b, c, d])
    return term


def get_uuud(flavors, spins):
    a, b, c, d = spins
    if flavors == 'uuud':
        term = sort_uuud([a, b, c, d])
    elif flavors == 'uudu':
        term = sort_uuud([a, b, d, c])
    elif flavors == 'uduu':
        term = sort_uuud([a, c, d, b])
    else: # flavors = 'duuu'
        term = sort_uuud([b, c, d, a])
    return term


def get_uud(flavors, spins):
    a, b, c = spins
    if flavors == 'uud':
        term = sort_uud([a, b, c])
    elif flavors == 'udu':
        term = sort_uud([a, c, b])
    else:  # flavors == 'duu'
        term = sort_uud([b, c, a])
    return term


def get_ordered_spins(flavor, elemental, isospin_rep):
    if isospin_rep in ['13_2', '12_3', 'Morn_N']:
        return get_uud(flavor, elemental)
    elif isospin_rep in ['134_2', '124_3', '123_4']:
        return get_uuud(flavor, elemental)
    elif isospin_rep in ['13_24', '12_34']:
        return get_uudd(flavor, elemental)
    elif isospin_rep in ['123', '1234']:
        return get_symmetric_spins(elemental)
    else:
        print('Isospin not recognized')
        exit()


def get_primitives_from_elemental(elemental, isospin_rep):
    flavors, extra_factors = FLAVOR_WAVEFUNCTIONS[isospin_rep]
    terms = []
    for flavor in flavors:
        terms.append(get_ordered_spins(flavor, elemental, isospin_rep))
    return terms, extra_factors


def get_primtive_op_from_elemenal_op(dict, isospin_rep):
    operator_simple = {}
    for elemental, value in dict.items():
        terms, extra_factors = get_primitives_from_elemental(elemental, isospin_rep)
        for term, extra_factor in zip(terms, extra_factors):
            try:
                operator_simple[term] += extra_factor * value
            except KeyError:
                operator_simple[term] = extra_factor * value
    operator_return = operator_simple.copy()
    for key, val in operator_simple.items():
        if abs(val) < 1e-8:
            del operator_return[key]
    return operator_return


def get_vector_from_operator_list(all_ops_list):
    all_ops_dict = {}
    for op in all_ops_list:
        all_ops_dict.update(op)

    # may be elementals or primitives
    elems_list = []
    for ops_dict in all_ops_list:
        elems_list.extend(list(ops_dict.keys()))
    elems_list = list(set(elems_list))
    total_num_elems = len(elems_list)
    vecs = []
    for op in all_ops_list:
        vec = np.zeros(total_num_elems, dtype=np.cdouble)
        # may be elemental or primitive
        for elemental, val in op.items():
            vec[elems_list.index(elemental)] += val
        vecs.append(vec)
    return vecs


def get_elementals_from_isospin_rep(Nd, Nc, isospin_rep):
    elementals = []
    all_spins_list = [list(np.arange(Nd))] * Nc
    for spins in itertools.product(*all_spins_list):
        elementals.append(''.join([str(spin) for spin in spins]))
    elementals_needed = []
    all_ops_list = []
    print(f'checking independence of {len(elementals)} elementals')
    for i, elemental in enumerate(elementals):
        if i%10 == 0:
            print(f'{i}/{len(elementals)}')
        dict = {elemental: 1}
        operator_simple = get_primtive_op_from_elemenal_op(dict, isospin_rep)

        all_ops_list.append(operator_simple)
        vecs = get_vector_from_operator_list(all_ops_list)
        vecs_rref, rows_rref = sympy.Matrix(vecs).rref()
        if len(rows_rref) == len(vecs):
            independent = 1
        else:
            independent = 0
        if independent:
            elementals_needed.append(elemental)
        else:
            all_ops_list = all_ops_list[:-1]  # drop the operator for future consideration
    return elementals_needed


def get_sorted_primitives(primitives_op):
    grouped_primitives_dict = {}
    for primitive in primitives_op.keys():
        prim = list(primitive)
        prim.sort()
        prim = ''.join(prim)
        try:
            grouped_primitives_dict[prim].append(primitive)
        except KeyError:
            grouped_primitives_dict[prim] = [primitive]
    return grouped_primitives_dict


def get_matching_primitives_and_elementals(prim_content, original_prims, elemental_states, rep):
    possible_primitives_vector = []
    possible_elementals_vector = []
    for original_prim in original_prims:
        possible_primitives_vector.append(original_prim)
    for elem in elemental_states:
        elem_sorted = list(elem)
        elem_sorted.sort()
        elem_sorted = ''.join(elem_sorted)
        if elem_sorted == prim_content:
            elem_prim_list, elem_factor_list = get_primitives_from_elemental(elem, rep)
            possible_primitives_vector.extend(elem_prim_list)
            possible_elementals_vector.append(elem)
    possible_primitives_vector = list(set(possible_primitives_vector))
    return possible_primitives_vector, possible_elementals_vector


def get_operator_as_elementals_from_possible_primitives_and_elementals(operator_as_elementals,
                                                                       possible_primitives_vector,
                                                                       possible_elementals_vector,
                                                                       original_prims,
                                                                       prim_content,
                                                                       primitives_op,
                                                                       elemental_states,
                                                                       rep):
    operator_vector = np.zeros(len(possible_primitives_vector), dtype=np.cdouble)
    for original_prim in original_prims:
        original_prim_index = possible_primitives_vector.index(original_prim)
        operator_vector[original_prim_index] = primitives_op[original_prim]
    elems_vectors = []
    for elem in elemental_states:
        elem_sorted = list(elem)
        elem_sorted.sort()
        elem_sorted = ''.join(elem_sorted)
        if elem_sorted == prim_content:
            elem_vector = np.zeros(len(possible_primitives_vector))
            elem_prim_list, elem_factor_list = get_primitives_from_elemental(elem, rep)
            for elem_prim, elem_factor in zip(elem_prim_list, elem_factor_list):
                elem_prim_index = possible_primitives_vector.index(elem_prim)
                elem_vector[elem_prim_index] += elem_factor
            elems_vectors.append(elem_vector)
    elems_vectors_matrix = np.array(elems_vectors).transpose()
    solution, resid, _, _= np.linalg.lstsq(elems_vectors_matrix, operator_vector, rcond=None)
    if resid > 1e-8:
        print(' ~~~~~~~~~ problem converting primitives to elemental ~~~~~~~~~~~')
        exit()
    for factor, elemental in zip(solution, possible_elementals_vector):
        try:
            operator_as_elementals[elemental] += round(factor, 8)
        except KeyError:
            operator_as_elementals[elemental] = round(factor, 8)
    return operator_as_elementals


def get_operator_as_elementals(primitives_op, elemental_states, rep):
    values = list(primitives_op.values())
    if np.allclose(np.zeros(len(primitives_op.values())), values):
        return {}
    operator_as_elementals = {}  # final result goes here
    # first, group the primitives by their spin content via sorting, i.e. 0032 -> 0023
    grouped_primitives_dict = get_sorted_primitives(primitives_op)
    # loop over each group of primitives,
    # find linear combination of elementals to reproduce this section of the original operator
    for prim_content, original_prims in grouped_primitives_dict.items():
        # get the list of elementals and their corresponding primitives that match this prim_content
        possible_primitives_vector, possible_elementals_vector = \
            get_matching_primitives_and_elementals(prim_content, original_prims, elemental_states, rep)
        # find the linear combination of these possible elementals that reproduces this section of original op
        operator_as_elementals = \
            get_operator_as_elementals_from_possible_primitives_and_elementals(operator_as_elementals,
                                                                               possible_primitives_vector,
                                                                               possible_elementals_vector,
                                                                               original_prims,
                                                                               prim_content,
                                                                               primitives_op,
                                                                               elemental_states, rep)
    operator_return = {}
    for key, val in operator_as_elementals.items():
        if abs(val) < 1e-8:
            continue
        operator_return[key] = val
    return operator_return


