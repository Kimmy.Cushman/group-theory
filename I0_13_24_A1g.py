import pickle
from baryon_oo import save_matrix_from_dict


def get_operators_13_24_A1g(basis, choice):
    ops = []
    if basis == 'DR_Euclidean':
        if choice == 0:
            ops = {0: {'0101': 1, '2323': 1}}
        if choice == 1:
            ops = {0: {'0103': 1, '0112': -1, '0323': 1, '1223': -1}}
        if choice == 2:
            ops = {0: {'0213': 2, '0303': -1, '1212': -1}}
        if choice == 3:
            ops = {0: {'0123': 1.0}}
    return ops


def main():
    Nc = 4
    isospin_rep = '13_24'
    rep = 'A1g'
    basis = 'DR_Euclidean'
    Sz_list = [0]
    ops_dict = {}
    for choice in [0,1,2,3]:
        ops_dict[choice] = {}
        for Sz in Sz_list:
            ops_dict[choice][Sz] = get_operators_13_24_A1g(basis, choice)[Sz]
            filename = f'Operators/{basis}/gamma_matrix_Nc{Nc}_isospin{isospin_rep}_{rep}_Sz{Sz}_choice{choice}.txt'
            save_matrix_from_dict(ops_dict[choice][Sz], filename)
    with open(f'Operators/{basis}/diagonalized_13_24_A1g.pkl', 'wb') as f:
        pickle.dump(ops_dict, f)


if __name__ == '__main__':
    main()
