import pickle
from baryon_oo import save_matrix_from_dict


def get_operators_12_34_A1g(basis, choice):
    ops = []
    if basis == 'DR_Euclidean':
        if choice == 0:
            ops = {0: {'0011': 1, '2233': 1}}
        if choice == 1:
            ops = {0: {'0013': 1, '0112': -2, '0233': 1, '1223': -2}}

        if choice == 2:
            ops = {0: {'0033': 1, '0213': 4, '1122': 1}}
        if choice == 3:
            ops = {0: {'0123': 1, '0213': 2}}
    return ops


def main():
    Nc = 4
    isospin_rep = '12_34'
    rep = 'A1g'
    basis = 'DR_Euclidean'
    Sz_list = [0]
    ops_dict = {}
    for choice in [0,1,2,3]:
        ops_dict[choice] = {}
        for Sz in Sz_list:
            ops_dict[choice][Sz] = get_operators_12_34_A1g(basis, choice)[Sz]
            filename = f'Operators/{basis}/gamma_matrix_Nc{Nc}_isospin{isospin_rep}_{rep}_Sz{Sz}_choice{choice}.txt'
            save_matrix_from_dict(ops_dict[choice][Sz], filename)
    with open(f'Operators/{basis}/diagonalized_12_34_A1g.pkl', 'wb') as f:
        pickle.dump(ops_dict, f)


if __name__ == '__main__':
    main()