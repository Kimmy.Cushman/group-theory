import numpy as np
import sympy
import pickle
from LaTex.ops_to_table import get_George_op
from rearrange_quarks import FLAVOR_WAVEFUNCTIONS
from general_tools import readable_row


def get_flavor_spin_op(op, isospin):
    flavor_spin_op = {}
    for elem, val in op.items():
        flavors, factors = FLAVOR_WAVEFUNCTIONS[isospin]
        for flavor, factor in zip(flavors, factors):
            flavor_spin = ''
            for s, f in zip(elem, flavor):
                flavor_spin += f'{f}{s} '
            flavor_spin = flavor_spin[:-1]  # get rid of extra space at the end
            try:
                flavor_spin_op[flavor_spin] += val * factor
            except KeyError:
                flavor_spin_op[flavor_spin] = val * factor
    return flavor_spin_op


def swap_spins(flavor_spin_op, swap):
    if swap == '':
        return flavor_spin_op

    swap_op = {}
    for flavor_spin, val in flavor_spin_op.items():
        Nc = len(flavor_spin.split(' '))
        if Nc == 3:
            a, b, c = flavor_spin.split(' ')
            if swap == '01':
                elem_swap = f'{b} {a} {c}'
            elif swap == '02':
                elem_swap = f'{c} {b} {a}'
            elif swap == '12':
                elem_swap = f'{a} {c} {b}'
            else:
                print('want index1 < index2')
                exit()
                return
        else:  # Nc == 4:
            a, b, c, d = flavor_spin.split(' ')
            if swap == '01':
                elem_swap = f'{b} {a} {c} {d}'
            elif swap == '02':
                elem_swap = f'{c} {b} {a} {d}'
            elif swap == '03':
                elem_swap = f'{d} {b} {c} {a}'

            elif swap == '12':
                elem_swap = f'{a} {c} {b} {d}'
            elif swap == '13':
                elem_swap = f'{a} {d} {c} {b}'

            elif swap == '23':
                elem_swap = f'{a} {b} {d} {c}'
            else:
                print('want index1 < index2')
                exit()
                return
        try:
            swap_op[elem_swap] += val
        except KeyError:
            swap_op[elem_swap] = val
    return swap_op


def get_all_terms(op_dict):
    all_terms = []
    for isospin, choice_dict in op_dict.items():
        for choice, op_swaps_dict in choice_dict.items():
            for swap, op in op_swaps_dict.items():
                for elem, val in op.items():
                    if elem not in all_terms:
                        all_terms.append(elem)
    return all_terms


def recheck_linear_combination(swap_list, swapped_isospin_dict, all_terms, linear_combination, columns_indices):
    linear_combination_swapped = {}
    for swap in swap_list:
        op = {}
        for i, factor in enumerate(linear_combination):
            if abs(factor) < 1e-8:
                continue
            FS, _, choice = columns_indices[i].split(' ')
            choice = int(choice)
            op_i = swapped_isospin_dict[FS][choice][swap]
            # op += factor_i * op_i
            for term, val in op_i.items():
                try:
                    op[term] += val * factor
                except KeyError:
                    op[term] = val * factor
        linear_combination_swapped[swap] = op

    pass_swap_test = 1
    for swap in swap_list:
        for term in all_terms:
            try:
                original_val = linear_combination_swapped[''][term]
            except KeyError:
                original_val = 0
            try:
                swapped_val = linear_combination_swapped[swap][term]
            except KeyError:
                swapped_val = 0

            if abs(original_val - swapped_val) > 1e-8:  # rounded sols to 1e-12
                print(term, original_val-swapped_val)
                pass_swap_test = 0
                break

    return pass_swap_test


def check_if_row_in_matrix(row, matrix, printing):
    found_row_in_matrix = 0
    for row_compare in matrix:
        if np.allclose(row, row_compare) or np.allclose(row, -row_compare):
            if printing:
                print('found row in matrix')
                print(list(np.around(row, 3)))
                print(list(np.around(row_compare, 3)))
            found_row_in_matrix = 1
            break
    return found_row_in_matrix


def get_totally_symmetric_solutions(solutions, swap_list, swapped_isospin_dict, all_terms):
    print('should be symmetric under exchange of first two -> should be identity for F=S')
    print(solutions['01'])

    print('checking that solutions are the same for every type of swap -> TOTALLY symmetric')
    skip_swap = ['', '01']
    sols_list = []
    for i, swap_i in enumerate(swap_list):
        if swap_i in skip_swap:
            continue
        sols_i = np.around(solutions[swap_i], 12)
        for sol_i in sols_i:
            found_sol_i_in_all_swaps = 1
            # must find this sol in all swaps
            for swap_j in swap_list:
                if swap_j in skip_swap or swap_j == swap_i:
                    continue
                sols_j = np.around(solutions[swap_j], 12)
                found_sol_i_in_swap_j = check_if_row_in_matrix(row=sol_i, matrix=sols_j, printing=False)
                if not found_sol_i_in_swap_j:
                    found_sol_i_in_all_swaps = 0
                    break
            if found_sol_i_in_all_swaps:
                if not check_if_row_in_matrix(sol_i, sols_list, printing=False):
                    sols_list.append(sol_i)

    # for printing readable linear combinations
    columns_indices = {}
    column_index = 0
    for FS_key in swapped_isospin_dict.keys():
        for choice in swapped_isospin_dict[FS_key].keys():
            columns_indices[column_index] = f'{FS_key} choice {choice}'
            column_index += 1

    for sol in sols_list:
        passed_check = recheck_linear_combination(swap_list, swapped_isospin_dict,
                                                  all_terms, sol, columns_indices)
        if passed_check:
            print('Found solution :)')
        else:
            print('invalid solution upon recheck')

        min_elem = min([abs(elem) for elem in sol if abs(elem) > 1e-8])
        sol = sol / min_elem
        sol_round = np.around(sol, 3)
        if 1.333 in sol_round or -1.333 in sol_round:
            sol = sol * 3
        if 1.5 in sol_round:
            sol = sol * 2
        for i, elem in enumerate(sol):
            if abs(elem) > 1e-8:
                print(f'{round(elem, 1)} * {columns_indices[i]}')
        print()


def get_null_space_of_swaps(swap_list, swapped_isospin_dict, all_terms):
    """
    We want to find the linear combinations coefficients alpha_i such that
    sum_i( alpha_i op_i[term] ) = sum_i( alpha_i swap(op_i)[term])
    for every flavor/spin term of op_i,
    where op_i is the operator associated with the ith isospin/choice.
    Note that we have swap(op_i)[term], term evaluated after swap.
    Thus,  sum( alpha_i*( op_i[term1] - swap(op_i)[term]) ) = 0
    Thus,   sum( alpha_i*( op_i[term2] - swap(op_i)[term1]) ) = 0
            sum( alpha_i*( op_i[term3] - swap(op_i)[term2]) ) = 0
            sum( alpha_i*( op_i[term4] - swap(op_i)[term3]) ) = 0
            ...
    Thus,  | op_1[term1] - swap(op_1)[term1]     op_2[term1] - swap(op_2)[term1]   ...  |   | alpha_1 |       | 0 |
           | op_1[term2] - swap(op_1)[term2]     op_2[term2] - swap(op_2)[term2]   ...  |   | alpha_2 |       | 0 |
           | op_1[term3] - swap(op_1)[term3]     op_2[term3] - swap(op_2)[term3]   ...  |   | alpha_3 |  =    | 0 |
           | op_1[term4] - swap(op_1)[term4]     op_2[term4] - swap(op_2)[term4]   ...  |   | alpha_4 |       | 0 |
           | op_1[term5] - swap(op_1)[term5]     op_2[term5] - swap(op_2)[term5]   ...  |   | alpha_5 |       | 0 |
    Thus, the solution vectors (alpha_i) define the null space of the matrix
    A_ij = op_i[term_j] - swap(op_i)[term_j]
    The final solutions will be those that hold for all swaps
    """
    all_swaps_solutions = {}
    for swap in swap_list:
        # if swap == '':  # don't include the identity swap
        #     continue
        matrix_of_ops_minus_swap_ops = []
        # terms make up the rows of the matrix
        for i, term in enumerate(all_terms):
            term_row_of_matrix = []
            # Flavor Spin wavefunctions, choice make up columns of the matrix
            for FS_key in swapped_isospin_dict.keys():
                for choice in swapped_isospin_dict[FS_key].keys():
                    operator_original = swapped_isospin_dict[FS_key][choice]['']
                    operator_swapped = swapped_isospin_dict[FS_key][choice][swap]
                    try:
                        original_val = operator_original[term]
                    except KeyError:
                        original_val = 0
                    try:
                        swapped_val = operator_swapped[term]
                    except KeyError:
                        swapped_val = 0
                    orig_minus_swap = original_val - swapped_val
                    term_row_of_matrix.append(orig_minus_swap)
            term_row_of_matrix = np.array(term_row_of_matrix)
            matrix_of_ops_minus_swap_ops.append(term_row_of_matrix)
        matrix_of_ops_minus_swap_ops = np.array(matrix_of_ops_minus_swap_ops)

        null = sympy.Matrix(matrix_of_ops_minus_swap_ops).nullspace()
        null = np.array(null).astype(np.double)
        normed_solutions = []
        for solution in null:
            solution = solution[:, 0]
            normed_solutions.append(solution/np.linalg.norm(solution))
        normed_solutions = np.array(normed_solutions)
        all_swaps_solutions[swap] = normed_solutions
    return all_swaps_solutions


def main():
    Nc = 3
    if Nc == 4:
        basis = 'DR_Euclidean'
        choice_list = [0]
        isospin_list = ['12_34', '13_24']
        swap_list = ['', '01', '02', '03', '12', '13', '23']
        irrep = 'A1g'
        Sz = 0
    else:
        basis = 'Dirac_Euclidean'
        choice_list = [0, 1, 2]
        isospin_list = ['12_3', '13_2']
        swap_list = ['', '01', '02', '12']
        irrep = 'G1g'
        Sz = -0.5

    swapped_isospin_dict = {}
    for wavefunction_flavor in isospin_list:
        for wavefunction_spin in isospin_list:  #
            FS_key = f'{wavefunction_flavor}F_{wavefunction_spin}S'
            swapped_isospin_dict[FS_key] = {}
            for choice in choice_list:
                swapped_isospin_dict[FS_key][choice] = {}
                filename = f'Operators/{basis}/diagonalized_{wavefunction_flavor}_{irrep}.pkl'
                operators_dict = {}
                with open(filename, 'rb') as f:
                    print(f'loading {filename}')
                    operators_dict.update(pickle.load(f))
                for key, val in operators_dict.items():
                    print(key,val)
                print()
                op = operators_dict[choice][Sz]
                # op_symmetrized = get_George_op(op, wavefunction_spin)
                op_symmetrized = op
                flavor_spin_op = get_flavor_spin_op(op_symmetrized, wavefunction_flavor)
                for swap in swap_list:
                    swapped_isospin_dict[FS_key][choice][swap] = swap_spins(flavor_spin_op, swap)

    all_terms = get_all_terms(swapped_isospin_dict)  # all terms for all choices and isospins and swaps
    solutions = get_null_space_of_swaps(swap_list, swapped_isospin_dict, all_terms)
    get_totally_symmetric_solutions(solutions, swap_list, swapped_isospin_dict, all_terms)

    # operator is symm under exchange
    # O_ab = O_ab^T
    # so O_ab = 1/2(O_ab + O_ba)
    # if not symm
    # O_ab != 1/2(O_ab + O_ba)
    # (udu - duu) 123 = uud(132 - 231)   # these are not equal !!!! :(
    # not true in general for products of tensors, may be okay here - pretend it is true and then it becomes true
    # true if left side and right side are shorthand for sum over all permutations
    #                 = (u1 u3 d2 - u2 u3 d1)
    #                 != all permutations


if __name__ == '__main__':
    main()
