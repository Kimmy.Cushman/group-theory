

def get_operators_12_3_Hg_row0(choice):
    ops = []
    if choice == 0:
        ops = [
            {'022': 1.0}
        ]
    return ops

def get_operators_12_3_Hg_row1(choice):
    ops = []
    if choice == 0:
        ops = [
            {'023': 1, '032': 1, '122': 1}
        ]
    return ops

def get_operators_12_3_Hg_row2(choice):
    ops = []
    if choice == 0:
        ops = [
            {'033': 1, '123': 1, '132': 1}
        ]
    return ops

def get_operators_12_3_Hg_row3(choice):
    ops = []
    if choice == 0:
        ops = [
            {'133': 1.0}
        ]
    return ops
