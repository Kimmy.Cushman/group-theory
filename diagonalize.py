import itertools

import numpy as np
import os

import scipy.linalg
import sympy
import sys
import pickle
from Group_theory_definitions.octahedral import get_octahedral_group_spinor_rotations
from baryon_oo import get_elementals_and_linear_combinations, get_W
from Group_theory_definitions.octahedral import OCTAHEDRAL_MULTIPLICATION_TABLE
from Group_theory_definitions.double_point import DOUBLE_POINT_MULTIPLICATION_TABLE
from general_tools import readable_row, readable_row_nonzero
from get_W_helper import unitarize_W
from projections import projection_setup, gram_schmidt
from LaTex.ops_to_table import get_operator_latex, get_George_op
from baryon_oo import write_by_rows_latex_pdf
np.set_printoptions(suppress=True,linewidth=np.nan)
from I0_12_34_A1g import get_operators_12_34_A1g
from I0_13_24_A1g import get_operators_13_24_A1g
from I1_134_2_T1g import get_operators_134_2_T1g
from I2_1234_Eg import get_operators_1234_Eg
from I2_1234_T2g import get_operators_1234_T2g
from I0p5_13_2_G1g import get_operators_13_2_G1g
from I0p5_12_3_G1g import get_operators_12_3_G1g


IRREPS_TO_SPINS = {
    'G1g_rep': 0.5,
    'Hg_rep': 1.5,
    'A1g_rep': 0,
    'T1g_rep': 1,
    'Eg_rep': 2,
    'T2g_rep': 2
}


def get_group_eigenvalue(rep):
    Spin = IRREPS_TO_SPINS[rep]
    Sz_list = [-Spin + m for m in range(int(2*Spin + 1))]

    Rz_list = [np.round(np.exp(1j * np.pi/2 * Sz), 6) for Sz in Sz_list]
    print(Rz_list)
    Rz_to_Sz = {}
    for Rz, Sz in zip(Rz_list, Sz_list):
        Rz_to_Sz[Rz] = Sz

    return Rz_to_Sz


def print_evecs_evals(Sz_operator_basis, evals, evecs, Rz_to_Sz_evals):
    if np.count_nonzero(Sz_operator_basis - np.diag(np.diagonal(Sz_operator_basis))) == 0:
        print('---- SZ   I S   A L R E A D Y    D I A G O N A L ! ----')
        for wi, vi in zip(evals, evecs.T):
            wi = np.round(wi, 6)
            print(f'GROUP Eigenvalue : {wi}')
            try:
                Sz_eval = Rz_to_Sz_evals[wi]
                print(f'ALGEBRA Eigenvalue: {Sz_eval}')
            except KeyError:
                pass
            print()
    else:
        for wi, vi in zip(evals, evecs.T):
            wi = np.round(wi, 6)
            # print(f'Eigenvector: {vi}')
            print(f'GROUP Eigenvalue : {wi}')
            try:
                Sz_eval = Rz_to_Sz_evals[wi]
                print(f'ALGEBRA Eigenvalue: {Sz_eval}')
            except KeyError:
                pass
            print()


def check_rogue_rotation(W_R, operator_basis, elemental_states):
    rogue = 0
    # N = len(elemental_states)
    # First compute the matrix Pi, whose columns are the operators of an irrep in the elemental basis
    Pi = []
    for row_choice, op in operator_basis.items():
        op_vector = np.zeros(len(elemental_states), dtype=np.cdouble)
        for key, val in op.items():
            num_permutations_elem = len(set(itertools.permutations(key)))  # THIS IS A NECESSARY NORMALIZATION
            op_vector[elemental_states.index(key)] += val #/ np.sqrt(num_permutations_elem)
        op_vector_normed = op_vector/np.linalg.norm(op_vector)
        Pi.append(op_vector_normed)
    K = len(Pi)
    Pi = gram_schmidt(Pi)
    Pi = np.array(Pi).transpose() # N x K by taking transpose
    Pi_dag = np.conj(Pi.transpose())  # Pi_dag is K x N matrix

    # now rotate those vectors via W(R)
    # W_Pi is a N x K matrix, with N=num elementals, K=num operators in this irrep
    W_Pi = np.matmul(W_R, Pi)

    # this is not necessary, but it makes sense to me.
    # each column of solution matrix is the solution for rotated operator
    # Pi (Solutions) = W Pi
    # -> Solutions = Pi^dag W Pi = R
    solutions = []
    for col in W_Pi.transpose():
        solution, resid, _, _  = scipy.linalg.lstsq(Pi, col)
        solutions.append(solution)
        if resid > 1e-8:
            rogue = 1
            for i, element in enumerate(col):
                if abs(element) > 1e-8:
                    print(elemental_states[i])
            print()
    solutions = np.array(solutions).transpose()

    # (K x N) x (N x K) matrix gives K x K matrix, DETERMINANT SHOULD BE 1 if no elementals go rogue!
    rotation_in_irrep_basis = np.matmul(Pi_dag, W_Pi)

    check1 = 1
    check2 = 1
    check3 = 0
    # CHECK # 1
    if check1:
        Pi_dag_Pi = np.matmul(Pi_dag, Pi)
        if np.allclose(np.identity(K), Pi_dag_Pi):
            pass
            # print('Check 1 pass')
        else:
            rogue = 1
            print('Check 1   --  FAIL  --')
            for row in Pi_dag_Pi:
                print(readable_row(row, 1, 3))
            print()

    R = rotation_in_irrep_basis
    if np.allclose(R, solutions):
        pass
    else:
        rogue = 1
    # CHECK # 2
    if check2:
        if abs(abs(np.linalg.det(R)) -1) < 1e-8:
            pass
            # print('Check 2 pass')
        else:
            rogue = 1
            # print('Check 2   --  FAIL  --')

    # CHECK # 3
    if check3:
        # FIXME dagger vs transpose
        RR_dag = np.matmul(R, R.transpose())
        if np.allclose(np.identity(len(R)), RR_dag):
            pass
            # print('Check 3 pass')
        else:
            rogue = 1
            # print('Check 3   --  FAIL  --')

    return rogue


def linear_combination_of_ops(ops, linear_combination, elemental_states):
    ops_vectors = []
    # ops = [
    #     {'0033': (-0.25155473 + 0j), '0123': (0.76880577 + 0j), '0213': (0.53139263 + 0j), '1122': (-0.25155473 + 0j)},
    #     {'0033': (-0.35920456 + 0j), '0123': (0.31819079 + 0j), '0213': (-0.80043663 + 0j), '1122': (-0.35920456 + 0j)},
    #     {'0011': (0.70710678 + 0j), '2233': (0.70710678 + 0j)},
    #     {'0013': (-0.31622777 + 0j), '0112': (0.63245554 + 0j), '0233': (-0.31622777 + 0j), '1223': (0.63245554 + 0j)}
    # ]
    for op in ops:
        op_vector = np.zeros(len(elemental_states))
        for elem, val in op.items():
            op_vector[elemental_states.index(elem)] += val
        ops_vectors.append(op_vector)
    new_ops_vectors = np.matmul(linear_combination, ops_vectors)
    new_ops = []
    for op_vector in new_ops_vectors:
        if np.linalg.norm(op_vector) == 0:
            continue
        op_vector = op_vector/np.linalg.norm(op_vector)
        new_op = {}
        for i, elem in enumerate(op_vector):
            if abs(elem) > 1e-8:
                new_op[elemental_states[i]] = elem
        new_ops.append(new_op)
    return new_ops


def get_rotation_in_operator_basis(gamma_basis, get_W_function, operator_basis, elemental_states, rotation):
    spinor_rotations = get_octahedral_group_spinor_rotations(gamma_basis).copy()
    W_R_full_matrix = get_W_function(spinor_rotations[rotation])
    # R_ij = <O_i | W(R) | O_j >
    #      = sum_n sum_m a_n^i a_m^j < El_n | W(R) | El_m >, El elementals
    #                                \~~ matrix element ~~/
    num_ops = len(operator_basis)
    R_operator_basis = np.zeros((num_ops, num_ops), dtype=np.cdouble)


    for j, op_j in enumerate(operator_basis):
        O_j = op_j.copy()
        for m, (Elem_m, val_m_j) in enumerate(O_j.items()):
            Elem_m_index = elemental_states.index(Elem_m)
            for i, op_i in enumerate(operator_basis):
                O_i = op_i.copy()
                for n, (Elem_n, val_n_i) in enumerate(O_i.items()):
                    Elem_n_index = elemental_states.index(Elem_n)
                    n_W_R_m = W_R_full_matrix[Elem_n_index, Elem_m_index]
                    R_operator_basis[i, j] += val_n_i * val_m_j * n_W_R_m

    return R_operator_basis, 0


def get_new_basis(evecs, operator_basis):
    operator_list = []
    for evec in evecs.T:
        operator = {}
        for i, factor in enumerate(evec):
            operator_i = operator_basis[i]
            for elem, val in operator_i.items():
                if abs(val * factor) > 1e-7:
                    if val * factor - np.real(val*factor) < 1e-7:
                        mult = np.real(val * factor)
                    else:
                        mult = val * factor
                    try:
                        operator[elem] += np.round(mult, 3)
                    except KeyError:
                        operator[elem] = np.round(mult, 3)
        operator_list.append(operator)
    return operator_list


def get_similar_matrix(M, evecs):
    v_dag = np.conj(evecs.T)
    M_sim = np.matmul(np.matmul(v_dag, M), evecs)
    return M_sim


def check_rogue_all_rotations(gamma_basis, operators_dict, get_W_function,
                              elemental_states, isospin_rep, rep, unitarize):

    # use generators of rotations to get all W(R) for all R in double point group
    # need to check W is closed under full group
    _, all_elements, _, W_mult_rules, _, _, M \
        = projection_setup(gamma_basis, rep, get_W_function, elemental_states)

    rogue_pass = 1
    for element in all_elements:
        W_element = W_mult_rules(element)
        if unitarize:
            W_element = unitarize_W(W_element, M)

        rogue = check_rogue_rotation(W_element, operators_dict, elemental_states)
        if rogue:
            rogue_pass = 0
            # print(f'rotation: {rotation} FAILED  :(   :(')
            break
        # else:
        #     print(f'rotation: {rotation} PASSED')
    return rogue_pass


def get_diagonalized_operators_latex(evecs, operator_basis, elemental_states, rep, isospin_rep, gamma_basis):
    operators_vectors = []
    for op in operator_basis:
        operator_vector = np.zeros(len(elemental_states), dtype=np.cdouble)
        for elem, val in op.items():
            operator_vector[elemental_states.index(elem)] += val
        operators_vectors.append(operator_vector)
    operators_vectors = np.array(operators_vectors)

    diagonalized_operator_vectors = np.matmul(np.linalg.inv(evecs), operators_vectors)
    diagonalized_operators = {}
    for i, op_vec in enumerate(diagonalized_operator_vectors):
        operator = {}
        for index, val in enumerate(op_vec):
            if abs(val) > 1e-8:
                operator[elemental_states[index]] = np.around(val, 8)
        diagonalized_operators[f'row {i} choice 0'] = operator

    write_by_rows_latex_pdf(rep, diagonalized_operators, isospin_rep, elemental_states, gamma_basis,
                            George_compare=False, basis_change='', filename='diagonalized')




def diagonalize_Sz(gamma_basis, operator_basis, get_W_function, elemental_states, rep, isospin_rep):
    # < O_i | Sz | O_j >
    rotation = 'C4z'
    Sz_operator_basis, rogue = get_rotation_in_operator_basis(gamma_basis,
                                                              get_W_function,
                                                              operator_basis,
                                                              elemental_states,
                                                              rotation)

    print('Sz before diagonalization')
    for row in Sz_operator_basis:
        print(readable_row(row, 1, 3))
    # exit()
    Rz_to_Sz = get_group_eigenvalue(rep)
    evals, evecs = np.linalg.eig(Sz_operator_basis)
    print_evecs_evals(Sz_operator_basis, evals, evecs, Rz_to_Sz)
    print('eigenvectors')
    for row in evecs:
        # print(readable_row(row, 1, 3))
        print(list(np.real(row)))
    print('eigenvectors inverse')
    for row in np.linalg.inv(evecs):
        print(list(np.real(row)))
        # print(readable_row(row, 1, 3))
    print()
    get_diagonalized_operators_latex(evecs, operator_basis, elemental_states, rep, isospin_rep, gamma_basis)

    v_inv = np.linalg.inv(evecs)
    Sz_in_diag_basis = np.matmul(v_inv, np.matmul(Sz_operator_basis, evecs))
    print('Sz in new basis')
    for row in Sz_in_diag_basis:
        print(readable_row(row, factor=1))
    return evecs




def main():
    Nc = int(sys.argv[1])
    isospin_rep = sys.argv[2]
    rep = sys.argv[3]
    basis = sys.argv[4]
    unitarize = bool(int(sys.argv[5]))
    choice = int(sys.argv[6])
    diag_Sz = 1
    check_rogue = 1

    filename = f'Operators/{basis}/Nc{Nc}_isospin{isospin_rep}_{rep}_elemental_operator'
    if unitarize:
        filename += '_unitarized'
    filename += '.pkl'
    operators_dict = {}
    with open(filename, 'rb') as f:
        print(f'loading {filename}')
        operators_dict.update(pickle.load(f))

    elemental_states, linear_combinations = get_elementals_and_linear_combinations(Nc, isospin_rep)
    get_W_function = get_W(Nc, elemental_states, linear_combinations)
    operator_basis = list(operators_dict.values())


    # operator_basis = get_operators_12_34_A1g(basis, choice)
    # operator_basis = get_operators_13_24_A1g(basis, choice)
    # operator_basis = get_operators_134_2_T1g(basis, choice)
    # operator_basis = get_operators_1234_Eg(basis, choice)
    # operator_basis = get_operators_1234_T2g(basis, choice)
    # operator_basis = get_operators_12_3_G1g(basis, choice)
    # operator_basis = list(get_operators_13_2_G1g(basis, choice).values())

    operator_basis = [get_George_op(op, isospin_rep) for op in operator_basis]
    rescale = np.identity(len(operator_basis))
    operator_basis = linear_combination_of_ops(operator_basis, rescale, elemental_states)

    num_ops = len(operator_basis)
    operators_subspace_dict = {}
    for op in range(num_ops):
        operators_subspace_dict[f'row {op} choice 0'] = operator_basis[op]
    print(operators_subspace_dict)
    write_by_rows_latex_pdf(rep, operators_subspace_dict, isospin_rep, elemental_states, basis,
                            George_compare=False, basis_change='', filename='symmetrized_')
    if diag_Sz:
        diagonalize_Sz(basis,
                       operator_basis,
                       get_W_function,
                       elemental_states,
                       rep, isospin_rep)

    if check_rogue:
        rogue_pass = check_rogue_all_rotations(basis,
                                               operators_subspace_dict,
                                               get_W_function,
                                               elemental_states,
                                               isospin_rep, rep, unitarize)
        if rogue_pass:
            print(' ~~~ PASS ~~~~ ')
        else:
            print('Rogue rotations found --- FAIL')

if __name__ == '__main__':
    main()
