import glob
import numpy as np
import sympy
from Group_theory_definitions.octahedral import get_gamma


def write_matrix_to_file(matrix, filename):
    with open(filename, 'w+') as f:
        for alpha in range(4):
            for beta in range(4):
                for sigma in range(4):
                    for delta in range(4):
                        val = matrix[alpha, beta, sigma, delta]
                        if abs(val) > 1e-8:
                            f.write(f'{alpha} {beta} {sigma} {delta} {round(val.real,3)} {round(val.imag,8)} \n')


def save_matrix_from_dict(matrix_dict, filename):
    # {'0213': -1.0, '0303': 1.0, '0312': -1.0, '1212': 1.0}
    gamma = np.zeros((4, 4, 4, 4))
    values = list(matrix_dict.values())
    print(matrix_dict)
    if max(values) < 1e-8:
        return
    min_val = min([abs(val) for val in values if val>1e-8])
    for spins, value in matrix_dict.items():
        alpha, beta, sigma, delta = [int(s) for s in spins]
        gamma[alpha, beta, sigma, delta] = value/min_val

    write_matrix_to_file(gamma, filename)


def get_buchoff_matrix(basis):
    gamma_matrices = get_gamma(basis)
    Gamma1 = np.matmul(gamma_matrices['C'], gamma_matrices['g5'])
    Gamma2 = Gamma1
    matrix = np.zeros((4, 4, 4, 4), dtype=np.cdouble)
    for alpha in range(4):
        for beta in range(4):
            for sigma in range(4):
                for delta in range(4):
                    matrix[alpha, beta, sigma, delta] += Gamma1[alpha, beta] * Gamma2[sigma, delta]
    return matrix


def spin_matrix_file_to_vector(filename):
    spins_matrix_vector = np.zeros((4 * 4 * 4 * 4), dtype=np.cdouble)
    with open(filename, 'r') as f:
        for line in f:
            if 'nan' in line or 'inf' in line:
                print(filename)
                print(line)
                print()
            alpha, beta, sigma, delta, real, imag, _ = line.split(' ')  # last word is \n
            real, imag = float(real), float(imag)
            if abs(real + 1j * imag) > 1e-8:
                index = int(alpha)*4*4*4 + int(beta)*4*4 + int(sigma)*4 + int(delta)
                spins_matrix_vector[index] = real + 1j * imag
    return spins_matrix_vector


def reconstruct_buchoff(basis):
    filelist = glob.glob(f'gamma_matrix_Nc4_Nf2_isospin13_2'
                         f'4_T2g_rep_row_0_choice_*.txt')
    # filelist.append('buchoff.txt')
    print('All files')
    for filename in filelist:
        print(filename)
    print()
    N_compare = len(filelist)
    spin0_basis = []
    for file_index, filename in enumerate(filelist):
        spin0_basis.append(spin_matrix_file_to_vector(filename))

    spin0_basis = np.array(spin0_basis)
    if np.allclose(spin0_basis, spin0_basis.real):
        spin0_basis = spin0_basis.real

    basis_compare = [spin0_basis[0]]
    filelist_independent = [filelist[0]]
    for v in range(1, N_compare):
        try_basis = basis_compare + [spin0_basis[v]]
        try_basis = np.transpose(try_basis)
        _, num_vecs = try_basis.shape
        M = sympy.Matrix(try_basis)
        M_rref, _ = M.rref()
        check_diag = np.array(M_rref[:num_vecs, :num_vecs])
        check_diag = check_diag.astype(float)
        diag = np.identity(num_vecs)
        if np.allclose(check_diag, diag):
            basis_compare.append(spin0_basis[v])
            filelist_independent.append(filelist[v])
    basis_compare = np.array(basis_compare)
    for filename in filelist_independent:
        print(filename)


def main():
    basis = 'Dirac_Euclidean'
    # buchoff_matrix = get_buchoff_matrix(basis)
    # write_matrix_to_file(buchoff_matrix, filename='buchoff.txt')
    reconstruct_buchoff(basis)


if __name__ == "__main__":
    main()