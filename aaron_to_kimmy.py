import numpy as np
import glob
from rearrange_quarks import get_uudd, get_uuud, get_uud, get_symmetric_spins
"""
For now, hardwired for I=2 1 flavor 
"""

basis_spins = {'PARITYPLUS SPINUP': 0,
               'PARITYPLUS SPINDN': 1,
               'PARITYMINUS SPINUP': 2,
               'PARITYMINUS SPINDN': 3}

FLAVOR_DICT = {'UP': 'u',
               'DOWN': 'd'}

REPS_AARON = {'A1g_rep': 'a1p',
              'Eg_rep': 'eep',
              'T1g_rep': 't1p',
              'T2g_rep': 't2p',
              'G1g_rep': 'g1p',
              'Hg_rep': 'hhp',
              'A1u_rep': 'a1m',
              'Eu_rep': 'eem',
              'T1u_rep': 't1m',
              'T2u_rep': 't2m',
              'G1u_rep': 'g1m',
              'Hu_rep': 'hhm'}


def aaron_to_kimmy_permute_spins_flavors(factors, states, isospin_rep):
    primitive_terms = {}
    for factor, state in zip(factors.values(), states.values()):
        flavor, spin = state.split('_')
        # rearrange so spin is in the correct order
        if isospin_rep in ['13_24', '12_34']:
            term = get_uudd(flavor, spin)
        elif isospin_rep in ['123_4', '124_3', '134_2']:
            term = get_uuud(flavor, spin)
        elif isospin_rep in ['12_3', '13_2', 'Morn_N']:
            term = get_uud(flavor, spin)
        elif isospin_rep == '1234':
            spin = get_symmetric_spins(spin)
            term = spin
        else: # isospin_rep == '123'
            spin = get_symmetric_spins(spin)
            term = spin
        try:
            primitive_terms[term] += factor
        except KeyError:
            primitive_terms[term] = factor
    return primitive_terms


def normalize_aaron_op(all_values, value):
    value_max_re = max([abs(val.real) for val in all_values])
    value_max_im = max([abs(val.imag) for val in all_values])
    if abs(value) < 1e-8:
        return 0
    # elif imaginary
    if abs(value - 1j * value.imag) < 1e-8:
        value = value.imag / value_max_im
        value = value.real
    # elif real
    elif abs(value - value.real) < 1e-8:
        value = value.real / value_max_re
    else:
        first_val = all_values[0]
        value = value/first_val
        # check if real
        if abs(value - value.real) < 1e-8:
            pass
        else:
            print(value, first_val, 'not real!')
            exit()
    value = np.around(value, 6)
    return value


def get_all_aaron_operators(isospin_rep, rep, aaron_dir):
    aaron_irreps = {}
    filelist = glob.glob(f'{aaron_dir}/{rep}_*.op')
    for filename in filelist:
        # read file, converting
        # FLAVOR1 SPIN1
        # FLAVOR2 SPIN2
        # FLAVOR3 SPIN3
        # FLAVOR4 SPIN4
        # into s1s2s3s4, flavor is determined from isospin rep
        aaron_irreps[filename] = {}
        states = {}
        factors = {}
        term = 0
        with open(filename, 'r') as f:
            for line in f:
                if 'FACTOR' in line:
                    term += 1
                    factor_real, factor_imag = line.split(' ')[1:]
                    factors[term] = float(factor_real) + 1j * float(factor_imag)
                    states[term] = '_'
                elif 'PARITY' in line:
                    _, flavor, parity, spin = line.split(' ')
                    spin_state = ' '.join([parity, spin[:-1]])
                    old_term = states[term]
                    old_flavor, old_spin = old_term.split('_')
                    new_flavor = old_flavor + FLAVOR_DICT[flavor]
                    new_spin = old_spin + str(basis_spins[spin_state])
                    states[term] = f'{new_flavor}_{new_spin}'
                else:
                    continue
        aaron_primitive_ops = aaron_to_kimmy_permute_spins_flavors(factors, states, isospin_rep)
        for primitive, value in aaron_primitive_ops.items():
            value = normalize_aaron_op(list(aaron_primitive_ops.values()), value)
            aaron_irreps[filename][primitive] = value

    return aaron_irreps


def check_ops_common_factor(op1, op2):
    # check if op1 = common_factor * op2
    # other op = op2
    # op simple = op1
    same = 0
    common_factor = [0]
    if op2.keys() == op1.keys():
        op1_vals = []
        op2_vals = []
        for key, val in op1.items():
            op1_vals.append(val)
            op2_vals.append(op2[key])
        common_factor = np.divide(np.array(op1_vals), np.array(op2_vals))
        if np.allclose(common_factor, [common_factor[0]] * len(common_factor)):
            same = 1
    return same, common_factor[0]


def get_unique_aaron_operators(isospin_rep, rep, aaron_dir):
    rep = REPS_AARON[rep]
    aaron_irreps = get_all_aaron_operators(isospin_rep, rep, aaron_dir)
    unique_aaron_irreps = aaron_irreps.copy()
    file_list = list(aaron_irreps.keys())
    for i, file1 in enumerate(file_list):
        for j, file2 in enumerate(file_list):
            if i < j:
                file1 = file_list[i]
                file2 = file_list[j]
                operator1 = aaron_irreps[file1]
                operator2 = aaron_irreps[file2]
                same, common_factor = check_ops_common_factor(operator1, operator2)
                if same:
                    try:
                        del unique_aaron_irreps[file2]
                    except KeyError:
                        pass

    return unique_aaron_irreps






