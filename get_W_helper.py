import numpy as np
import itertools
from rearrange_quarks import \
    get_primtive_op_from_elemenal_op, \
    get_vector_from_operator_list
from general_tools import negative_op
Nd = 4


def check_if_continue(elemental_test, elementals_indices, elemental_states, n_terms):
    # eg. loop over i,j,k with i < j < k
    if n_terms > 1:
        if elementals_indices[0] >= elementals_indices[1]:
            return 1
    if n_terms == 3:
        if elementals_indices[1] >= elementals_indices[2]:
            return 1

    # test if it is possible for these elementals to be equivalent
    # ex. 023 and 133 could not be because they dont have same spins

    # sort the test elemental
    ordered_elemental_test = [int(s) for s in elemental_test]
    ordered_elemental_test.sort()
    ordered_elemental_test = ''.join([str(s) for s in ordered_elemental_test])

    # sort each of the elementals being considered
    # if looking at indices (7, 14), elemental list might be [023, 213] -> [023, 123]
    elemental_list = [elemental_states[i] for i in elementals_indices]
    ordered_elemental_list = []
    for elemental in elemental_list:
        ordered_elemental = [int(s) for s in elemental]
        ordered_elemental.sort()
        ordered_elemental = ''.join([str(s) for s in ordered_elemental])
        ordered_elemental_list.append(ordered_elemental)

    same_set = [elem_set == ordered_elemental_test for elem_set in ordered_elemental_list]
    if False in same_set:
        return 1

    return 0


def find_linear_combination_of_n_terms(n_terms, elemental_test, elemental_states, Nc, isospin_rep):
    N_elemental = len(elemental_states)
    op_test = get_primtive_op_from_elemenal_op({elemental_test: 1}, isospin_rep)
    found_combination = 0
    elemental_list_return = []
    factors_return = []
    all_elemental_indices_list = [list(np.arange(N_elemental))] * n_terms
    for elementals_indices in itertools.product(*all_elemental_indices_list):
        # continue if indices are not like i < j < k,
        # or if elementals could not be equal, ex 033 cant be expressed with 123
        if check_if_continue(elemental_test, elementals_indices, elemental_states, n_terms):
            continue

        elemental_list = [elemental_states[i] for i in elementals_indices]
        elemental_ops_list = []
        for elem in elemental_list:
            elemental_op = get_primtive_op_from_elemenal_op({elem: 1}, isospin_rep)
            elemental_ops_list.append(elemental_op)

        # get vector space of elemental ops combined with test op
        original_ops_list = elemental_ops_list.copy()
        all_ops_list = original_ops_list
        all_ops_list.append(op_test)
        vecs = get_vector_from_operator_list(all_ops_list)

        # solve Ax = y for x,
        # where A is the matrix with each column the vector of primitives associated with one of the n_term elementals,
        # and y is the vector of primitives associated with element_test
        y_vector = vecs[-1]  # last vec in primitive_vecs is test_op in function
        A_matrix = np.array(vecs[:-1]).transpose()
        x_solution, resid, rank, sing_vals = np.linalg.lstsq(A_matrix, y_vector, rcond=None)
        if resid < 1e-8:
            found_combination = 1
            return found_combination, elemental_list, x_solution
        else:
            continue
    return found_combination, elemental_list_return, factors_return


def get_linear_combination(elemental_test, elemental_states, isospin_rep, Nc):
    op_test = get_primtive_op_from_elemenal_op({elemental_test: 1}, isospin_rep)
    if op_test == {}:
        return [], []
    # first check if equal to negative elemental operator
    for elemental in elemental_states:
        elemental_op = get_primtive_op_from_elemenal_op({elemental: 1}, isospin_rep)
        if op_test == elemental_op:
            return [elemental], [1]
        elif op_test == negative_op(elemental_op):
            return [elemental], [-1]
    # FIXME number of terms hardwired!
    if Nc == 4:
        n_terms_try = [1, 2, 3]
    else:
        n_terms_try = [1, 2]
    for n_terms in n_terms_try:
        found_combination, elemental_list, factors = find_linear_combination_of_n_terms(n_terms,
                                                                                        elemental_test,
                                                                                        elemental_states,
                                                                                        Nc,
                                                                                        isospin_rep)
        if found_combination:
            return elemental_list, factors
    return [], []


def get_all_W_linear_combinations(elemental_states, Nc, isospin_rep):
    print('getting all W linear combinations')
    linear_combinations = {}
    all_spins_list = [list(np.arange(Nd))] * Nc
    for i, spins_ in enumerate(itertools.product(*all_spins_list)):
        if i % 10 == 0:
            print(f'combo {i}/{Nc**Nc}')
        elemental_i = ''.join([str(spin_) for spin_ in spins_])
        if elemental_i in elemental_states:
            baryons_i = [elemental_i]
            factors_i = [1]
        else:
            baryons_i, factors_i = get_linear_combination(elemental_i, elemental_states, isospin_rep, Nc)
        linear_combinations[elemental_i] = [baryons_i, factors_i]
    return linear_combinations


def unitarize_W(W, M):
    evals, U = np.linalg.eigh(M)
    U_dag = U.transpose().conj()
    Md = np.diag(evals)
    U_Md_Udag = np.matmul(np.matmul(U, Md), U_dag)
    Udag_M_U = np.matmul(np.matmul(U_dag, M), U)
    if not np.allclose(M, U_Md_Udag) or not np.allclose(Md, Udag_M_U):
        print('something went wrong')
        exit()
    Md_sqrt_inv = np.linalg.inv(np.diag(np.array([np.sqrt(eval) for eval in evals])))
    V = np.matmul(np.matmul(U, Md_sqrt_inv), U_dag)
    W_unitarized = np.matmul(np.matmul(np.linalg.inv(V), W), V)
    W_unitarized_dag = W_unitarized.transpose().conj()
    Wnew_dag_Wnew = np.matmul(W_unitarized_dag, W_unitarized)
    if np.allclose(np.identity(len(W_unitarized)), Wnew_dag_Wnew):
        return W_unitarized
    else:
        print('W not unitary')
        exit()


def get_W(Nc, elemental_states, all_W_linear_combinations):
    # B_i -(R)-> W_ik(R) B_k
    def get_W_function(spinor_rotation):
        N_elemental_states = len(elemental_states)
        W = np.zeros((N_elemental_states, N_elemental_states), dtype=np.cdouble)
        for j_index, elemental_j in enumerate(elemental_states):
            all_spins_list = [list(np.arange(Nd))] * Nc
            for spins_ in itertools.product(*all_spins_list):
                elemental_i = ''.join([str(spin_) for spin_ in spins_])
                baryons_i, factors_i = all_W_linear_combinations[elemental_i]
                for baryon_i, factor_i in zip(baryons_i, factors_i):
                    i_index = elemental_states.index(baryon_i)
                    spin_matrices_factor = 1.0
                    for c in range(Nc):
                        alpha = int(elemental_j[c])
                        beta = int(elemental_i[c])
                        spin_matrices_factor *= spinor_rotation[alpha, beta]
                    if abs(spin_matrices_factor) > 1e-8:
                        W[i_index, j_index] += factor_i * spin_matrices_factor
        return W

    return get_W_function
