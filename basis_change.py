import numpy as np
import itertools
from Group_theory_definitions.Dirac_Euclidean_basis import get_Dirac_Euclidean_basis
from Group_theory_definitions.Weyl_Euclidean_basis import get_Weyl_Euclidean_basis
from Group_theory_definitions.DR_Euclidean_basis import get_DeGrand_Rossi_Euclidean_basis
from rearrange_quarks import \
    sort_uud, \
    sort_uuud, \
    sort_uudd, \
    get_symmetric_spins, \
    get_operator_as_elementals, \
    get_primitives_from_elemental

def test_basis_change_transform(basis_change, U1, U2):
    U = U1 + U2
    U_dag = np.conj(np.transpose(U))
    # gamma_mu (to basis) = U gamma_mu (from basis) U^dagger
    from_basis, _, to_basis = basis_change.split('_')
    if 'Dirac' in from_basis:
        from_basis_dict = get_Dirac_Euclidean_basis()
    elif 'DR' in from_basis:
        from_basis_dict = get_DeGrand_Rossi_Euclidean_basis()
    elif 'Weyl' in from_basis:
        from_basis_dict = get_Weyl_Euclidean_basis()
    else:
        return 0  # fail

    if 'Dirac' in to_basis:
        to_basis_dict = get_Dirac_Euclidean_basis()
    elif 'DR' in to_basis:
        to_basis_dict = get_DeGrand_Rossi_Euclidean_basis()
    elif 'Weyl' in to_basis:
        to_basis_dict = get_Weyl_Euclidean_basis()
    else:
        return 0  # fail

    pass_test = 1
    for mu in range(4):
        mu += 1  # indexed 1 through 4
        gamma_to = to_basis_dict[f'g{mu}']
        gamma_from = from_basis_dict[f'g{mu}']
        gamma_from_transformed = np.matmul(np.matmul(U, gamma_from), U_dag)
        if not np.allclose(gamma_to, gamma_from_transformed):
            pass_test = 0
            # break
    return pass_test


def get_basis_transform(basis_change):
    Dirac_basis = get_Dirac_Euclidean_basis()
    g2 = Dirac_basis['g2']
    g4 = Dirac_basis['g4']
    g5 = Dirac_basis['g5']
    if basis_change == 'Dirac_to_Weyl':
        # U = 1/sqrt(2) [1 + g5^D g4^D] to go from Dirac to Weyl
        U1 = 1 / np.sqrt(2) * np.identity(4)
        U2 = 1 / np.sqrt(2) * np.matmul(g5, g4)
    elif basis_change == 'Weyl_to_Dirac':
        # U = 1/sqrt(2) [1 - g5^D g4^D], dagger of above
        U1 = 1 / np.sqrt(2) * np.identity(4)
        U2 = - 1 / np.sqrt(2) * np.matmul(g5, g4)
    elif basis_change == 'Dirac_to_DR':
        # as give in https://github.com/JeffersonLab/chroma/blob/master/docs/notes/gamma_conventions.tex
        # U = 1/sqrt(2) [g2 g5 + g4 g2] (Dirac)
        U1 = 1 / np.sqrt(2) * np.matmul(g2, g5)
        U2 = 1 / np.sqrt(2) * np.matmul(g4, g2)
    elif basis_change == 'DR_to_Dirac':
        # U = 1/sqrt(2) [ -g2 g5 - g4 g2], dagger of above
        U1 = - 1 / np.sqrt(2) * np.matmul(g2, g5)
        U2 = - 1 / np.sqrt(2) * np.matmul(g4, g2)
    else:
        print(f'DO NOT recognize basis change {basis_change}')
        U1 = np.identity(4)
        U2 = np.identity(4)

    if test_basis_change_transform(basis_change, U1, U2):
        return U1, U2
    else:
        print('basis transform failed')
        exit()


def get_new_elem(Nc, new_spin_vecs, term_permutation, isospin, elemental_states):
    # hardwired for Nc=3
    spin_vec_quarks = []
    term_as_elemental = ''
    spin_vec_factor = 1
    for c in range(Nc):
        U1_or_U2 = term_permutation[c]
        spin_vec_quark = new_spin_vecs[c][U1_or_U2]
        spin_vec_quarks.append(spin_vec_quark)
        # get index of nonzero element of each spin vector - defines elemental - #FIXME hardwired for unit spin vec
        nonzero_spin_index = np.nonzero(spin_vec_quark)[0][0]
        term_as_elemental += str(nonzero_spin_index)
        # get the multiplicative factor (+/- sqrt(2)) from the transform of the spin vector
        spin_vec_factor *= spin_vec_quark[nonzero_spin_index]

    new_prims_terms, new_prims_factors = get_primitives_from_elemental(term_as_elemental, isospin)
    prim_op = {}
    for term, factor in zip(new_prims_terms, new_prims_factors):
        try:
            prim_op[term] += factor
        except:
            prim_op[term] = factor
    new_elem_as_elemental = get_operator_as_elementals(prim_op, elemental_states, isospin)
    return new_elem_as_elemental, spin_vec_factor


def change_basis(operator, isospin, elemental_states, basis_change):
    if basis_change == '':
        return operator
    U_transform1, U_transform2 = get_basis_transform(basis_change)
    operator_new_basis = {}
    for elem, op_val in operator.items():
        Nc = len(elem)
        new_spin_vecs = []
        for spin in elem:
            spin_vec = np.zeros(4)
            spin_vec[int(spin)] = 1
            # two terms in U transform
            spin_vec_new_basis1 = np.matmul(U_transform1, spin_vec)
            spin_vec_new_basis2 = np.matmul(U_transform2, spin_vec)
            new_spin_vecs.append([spin_vec_new_basis1, spin_vec_new_basis2])
        new_elems = []
        new_elems_factors = []
        # (U1 s1 + U2 s1)(U1 s2 + U2 s2)(U1 s3 U2 s3) 2^Nc = 8 permutatins of terms for Nc=3
        # (U1 s1 + U2 s1)(U1 s2 + U2 s2)(U1 s3 U2 s3)(U1 s4 + U2 s4) 2^Nc = 16 permutations of terms for Nc=4
        U1_or_U2_array = [np.arange(2)]*Nc
        for term_permutation in itertools.product(*U1_or_U2_array):
            new_elem, spin_vec_factor = get_new_elem(Nc, new_spin_vecs, term_permutation, isospin, elemental_states)
            for key, elem_val in new_elem.items():
                new_elems.append(key)
                new_elems_factors.append(spin_vec_factor*elem_val)

        for new_elem, new_factor in zip(new_elems, new_elems_factors):
            try:
                operator_new_basis[new_elem] += op_val * new_factor
            except KeyError:
                operator_new_basis[new_elem] = op_val * new_factor
    return operator_new_basis
