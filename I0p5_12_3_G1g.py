import pickle


def get_operators_12_3_G1g(basis, choice):
    ops = []
    if basis == 'Dirac_Euclidean':
        if choice == 0:
            ops = {
                0.5: {'001': 1},
                -0.5: {'011': 1}
            }
        elif choice == 1:
            ops = {
                0.5: {'023': 1, '122': -1},
                -0.5: {'033': 1, '132': -1}
            }
        elif choice == 2:
            ops = {
                0.5: {'023': 1, '032': -2, '122': 1},
                -0.5: {'033': 1, '123': -2, '132': 1}
            }
    return ops


def main():
    basis = 'Dirac_Euclidean'
    Sz_list = [0.5, -0.5]
    ops_dict = {}
    for choice in [0,1,2]:
        ops_dict[choice] = {}
        for Sz in Sz_list:
            ops_dict[choice][Sz] = get_operators_12_3_G1g(basis, choice)[Sz]
    with open(f'Operators/{basis}/diagonalized_12_3_G1g.pkl', 'wb') as f:
        pickle.dump(ops_dict, f)


if __name__ == '__main__':
    main()
