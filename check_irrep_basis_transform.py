import itertools

import numpy as np
import sys
import pickle
from Group_theory_definitions.octahedral import OCTAHEDRAL_MULTIPLICATION_TABLE, get_octahedral_group_spinor_rotations
from rearrange_quarks import get_primitives_from_elemental
from baryon_oo import get_elementals_and_linear_combinations
from diagonalize import readable_row
from get_W_helper import get_W

'''
Want to check whether the set of 
'''


def check_unitary(matrix):
    matrix_dag = matrix.transpose().conj()

    M_dag_M = np.matmul(matrix_dag, matrix)
    if np.allclose(np.identity(len(matrix)), M_dag_M):
        return 1
    else:
        return 0


def main():
    Nc = 3
    isospin_rep = '123'
    basis = 'Dirac_Euclidean'

    if Nc == 3:
        irrep_list = ['G1g_rep', 'G1u_rep', 'Hg_rep', 'Hu_rep']
    elif Nc == 4:
        irrep_list = ['A1g_rep', 'A1u_rep', 'Eg_rep', 'Eu_rep', 'T1g_rep', 'T1u_rep', 'T2g_rep', 'T2u_rep']
    else:
        print('Problem with Nc')
        exit()
        return

    elemental_states, linear_combinations = get_elementals_and_linear_combinations(Nc, isospin_rep)
    get_W_function = get_W(Nc, elemental_states, linear_combinations)
    N_elementals = len(elemental_states)

    # First compute the matrix Pi, whose columns are the operators of an irrep in the elemental basis
    operators_matrix = []  # Pi = operators_matrix^T
    for irrep in irrep_list:
        if isospin_rep == '1234' and irrep == 'A1u_rep':
            continue
        if 'g' in irrep:
            filename = f'Operators/{basis}/Nc{Nc}_isospin{isospin_rep}_{irrep}_elemental_operator.pkl'
        else:
            filename = f'Operators/{basis}/Nc{Nc}_isospin{isospin_rep}_{irrep}_aaron_elemental_operator.pkl'
        # filename = f'Operators/{basis}/Nc{Nc}_isospin{isospin_rep}_{irrep}_aaron_primitive_operator.pkl'
        operators_dict = {}
        with open(filename, 'rb') as f:
            operators_dict.update(pickle.load(f))

        # print(irrep)
        # print('before')
        # for key, val in operators_dict.items():
        #     print(key, val)

        # new_normalization_operators_dict = {}
        for key, operator in operators_dict.items():
            # new_normalization_operators_dict[key] = {}
            op_vector = np.zeros(N_elementals, dtype=np.cdouble)
            for elem, val in operator.items():
                num_permutations_elem = len(set(itertools.permutations(elem)))
                op_vector[elemental_states.index(elem)] += val /np.sqrt(num_permutations_elem)
                # try:
                #     new_normalization_operators_dict[key][elem] += val/np.sqrt(num_permutations_elem)
                # except KeyError:
                #     new_normalization_operators_dict[key][elem] = val/np.sqrt(num_permutations_elem)
            op_vector = op_vector/np.linalg.norm(op_vector)
            operators_matrix.append(op_vector)

        # print('after')
        # for key, val in new_normalization_operators_dict.items():
        #     print(key, val)

    rogue_pass = 1
    Pi = np.array(operators_matrix).transpose()
    Pi_dag = Pi.transpose().conj()

    Pi_unitary = check_unitary(Pi)
    if Pi_unitary:
        print('Pi dag Pi = 1')
    else:
        print('Pi NOT UNITARY!  :(   :(')

    all_rotations = list(OCTAHEDRAL_MULTIPLICATION_TABLE.keys())
    for rotation in all_rotations:

        spinor_rotations = get_octahedral_group_spinor_rotations(basis).copy()
        W_R = get_W_function(spinor_rotations[rotation])

        # FIXME try changing basis of W such that the elementals are normalize
        normalize_elementals_basis = np.zeros((len(elemental_states), len(elemental_states)))
        for i, elem in enumerate(elemental_states):
            normalize_elementals_basis[i,i] += 1/np.sqrt(len(set(itertools.permutations(elem))))
        Norm = normalize_elementals_basis
        Norm_dag = np.conj(normalize_elementals_basis.transpose())
        W_R_with_norm = np.matmul(np.matmul(Norm_dag, W_R), Norm)

        print(f'W({rotation}) unitary before? {check_unitary(W_R)}')
        print(f'W({rotation}) unitary  after? {check_unitary(W_R_with_norm)}')
        print()

        continue

        # Now, W_Pi rotates the vectors
        W_Pi = np.matmul(W_R, Pi)
        if rotation == 'E':
            if np.allclose(W_Pi, Pi):
                print(f'W(R={rotation}) x Pi = Pi \n')
            else:
                print(f'W(R={rotation}) x Pi != Pi \n')

        rotation_in_isospin_basis = np.matmul(Pi_dag, W_Pi)
        print(f'U(R={rotation})')
        for i, row in enumerate(rotation_in_isospin_basis):
            row_print = readable_row(np.real(row), 1, 3)
            print(row_print)
        print()

        RR_dag = np.matmul(rotation_in_isospin_basis, rotation_in_isospin_basis.transpose().conj())
        if abs(np.linalg.det(RR_dag) -1) > 1e-8:
            rogue_pass = 0
            print(f'Rotation {rotation} FAILED')
        else:
            print('Determinant = 1')

    if rogue_pass:
        print(':) ~~~ all rotations PASSED determinant test ~~~ :)')
    else:
        print(':(  :( some rotations FAILED the determinant test :( :( ')

    print('\n\n')


if __name__ == '__main__':
    main()

