import itertools
import os
import pickle
import numpy as np
from Checks.test_W_homomorphism import \
    test_spinor_octahedral_rotations_W_homomorphism, \
    test_spinor_rotations_unitarity
from basis_change import change_basis
from projections import get_irrep_operators
from aaron_to_kimmy import get_unique_aaron_operators
from rank4_matrices import save_matrix_from_dict
from LaTex.ops_to_table import write_by_rows_latex_pdf
from rearrange_quarks import \
    get_elementals_from_isospin_rep, \
    get_vector_from_operator_list, \
    get_operator_as_elementals, \
    get_primitives_from_elemental
from get_W_helper import get_W, get_all_W_linear_combinations, get_linear_combination

ROUNDING = 15

Nd = 4
SPIN_REPS = {0: 'A1',
             1: 'T1',
             2: 'E T2',
             0.5: 'G1',
             1.5: 'H'}

REP_DIMS = {
    'A1g_rep': 1,
    'Eg_rep': 2,
    'T1g_rep': 3,
    'T2g_rep': 3,
    'G1g_rep': 2,
    'Hg_rep': 4
}


AARON_DIRS = {'123': 'Aaron_group_theory/output_qqq05/i3.2c3.2/pcom000/N000_N000',
              '12_3': 'Aaron_group_theory/output_qqq05/i1.2c1.2/pcom000/N000_N000',
              '13_2': 'Aaron_group_theory/output_qqq05/i1.2c1.2/pcom000/N000_N000',
              'Morn_N': 'Aaron_group_theory/output_qqq05/i1.2c1.2/pcom000/N000_N000',
              '1234': 'Aaron_group_theory/output_dummy05/i2c2/pcom000/N000_N000',
              '123_4': 'Aaron_group_theory/output_dummy05/i1c1/pcom000/N000_N000',
              '134_2': 'Aaron_group_theory/output_dummy05/i1c1/pcom000/N000_N000',
              '124_3': 'Aaron_group_theory/output_dummy05/i1c1/pcom000/N000_N000',
              '12_34': 'Aaron_group_theory/output_dummy05/i0c0/pcom000/N000_N000',
              '13_24': 'Aaron_group_theory/output_dummy05/i0c0/pcom000/N000_N000'}

ISOSPIN_WAVEFUNCTIONS = {
    '123': '$uuu$',
    '13_2': '$\\frac{1}{\\sqrt{2}}(udu - duu)$',
    'Morn_N': '$\\frac{1}{\\sqrt{2}}(uud - duu)$',
    '12_3': '$\\frac{1}{\\sqrt{6}}(2 uud - udu - duu)$',
    '1234': '$uuuu$',
    '134_2': '$\\frac{1}{\\sqrt{2}}(uduu - duuu)$',
    '124_3': '$\\frac{1}{\\sqrt{6}}(2 uudu - uduu - duuu)$',
    '123_4': '$\\frac{1}{\\sqrt{12}}(3 uuud - uudu - uduu - duuu)$',
    '13_24': '$\\frac{1}{2}(ud - du)(ud - du)$',
    '12_34': '$\\frac{1}{12}(2(uudd + dduu) - (ud + du)(ud + du))$',
}

"""
See George's notes for review of -- 2 flavor -- Young Tableau
###############################
J = 2 reps
1 2 3 4  ----- denoted 1234
###############################
J = 1 reps
1 2 3
4      ----- denoted 123_4
1 2 4
3      ----- denoted 124_3
1 3 4
2      ----- denoted 134_2
##############################
J = 0 reps
1 2
3 4   ----- denoted 12_34
1 3
2 4   ----- denoted 13_24
##############################
"""


def find_kimmy_aaron_linear_combination(kimmy_ops, aaron_ops, printing):
    if printing:
        print(f'Trying to reconstruct kimmys operators from basis of Aaron operators')
        print()

    pass_check = 1
    for row_choice, kimmy_operator in kimmy_ops.items():
        all_ops = list(aaron_ops.values())
        all_ops.append(kimmy_operator)
        aaron_kimmy_vecs = get_vector_from_operator_list(all_ops)
        aaron_vecs = aaron_kimmy_vecs[:-1]
        kimmy_vec = aaron_kimmy_vecs[-1]
        # solve Ax = y for x, where y is kimmy operator, and A is the matrix whose columns are aaron operators
        A_matrix = np.array(aaron_vecs).transpose()
        y_vector = kimmy_vec
        solution, resid, rank, single_vals = np.linalg.lstsq(A_matrix, y_vector, rcond=None)
        if resid < 1e-8:
            if printing:
                print(f'row_choice {row_choice}, {kimmy_operator}')
                print('can be written as sum of')
                for i, val in enumerate(solution):
                    if abs(val) > 1e-8:
                        print(f'{round(val,8)} * {all_ops[i]}')
                print()
            else:
                pass
                # print(f'{row_choice} MATCH')
        else:
            pass_check = 0
            # print(f' :(   :( Cannot write kimmy operator for row choice {row_choice} in terms of aaron operators :(')
            # print()

    return pass_check


def get_elementals_and_linear_combinations(Nc, isospin_rep):
    base_filename = f'Nc{Nc}_isospin{isospin_rep}'
    # basis independent
    elemental_states_filename = f'Elementals/{base_filename}_elemental_states'
    lin_combinations_filename = f'Linear_combinations/{base_filename}_linear_combinations'
    os.system(f'mkdir -p Elementals')
    os.system(f'mkdir -p Linear_combinations')
    # basis dependent
    try:
        elemental_states = list(np.load(f'{elemental_states_filename}.npy'))
    except FileNotFoundError:
        elemental_states = get_elementals_from_isospin_rep(Nd, Nc, isospin_rep)
        np.save(elemental_states_filename, elemental_states)
    try:
        with open(f'{lin_combinations_filename}.pkl', 'rb') as f:
            linear_combinations = pickle.load(f)
    except FileNotFoundError:
        print('getting linear combinations')
        linear_combinations = get_all_W_linear_combinations(elemental_states, Nc, isospin_rep)
        with open(f'{lin_combinations_filename}.pkl', 'wb') as f:
            pickle.dump(linear_combinations, f)
    return elemental_states, linear_combinations


def compute_and_save_aaron_ops(Nc, basis, rep, isospin_rep, elemental_states):
    try:
        aaron_dir = AARON_DIRS[isospin_rep]
    except KeyError:
        print('isospin_rep ERROR: choose from list {123, 13_2, 1234_, 134_2, 13_24}')
        aaron_dir = ''
        exit()

    aaron_primitive_ops = get_unique_aaron_operators(isospin_rep, rep, aaron_dir)
    if len(aaron_primitive_ops.keys()) == 0:
        return {}, {}

    aaron_elemental_ops = {}
    for key, operator in aaron_primitive_ops.items():
        operator_spins = {}
        for primitive_spins, val in operator.items():
            operator_spins[primitive_spins] = val
        elemental_op = get_operator_as_elementals(operator_spins, elemental_states, isospin_rep)
        aaron_elemental_ops[key] = elemental_op

    base_filename = f'Nc{Nc}_isospin{isospin_rep}'
    os.system(f'mkdir -p Operators/{basis}')
    ops_elemental_filename = f'Operators/{basis}/{base_filename}_{rep}_aaron_elemental_operator.pkl'
    ops_primitive_filename = f'Operators/{basis}/{base_filename}_{rep}_aaron_primitive_operator.pkl'

    with open(ops_elemental_filename, 'wb') as f:
        pickle.dump(aaron_elemental_ops, f)

    with open(ops_primitive_filename, 'wb') as f:
        pickle.dump(aaron_primitive_ops, f)

    return aaron_elemental_ops, aaron_primitive_ops


def get_primitive_operator_from_elemental_operator(op, isospin_rep):
    op_primitive = {}
    for elem, val in op.items():
        prims, prim_factors = get_primitives_from_elemental(elem, isospin_rep)
        for prim, prim_factor in zip(prims, prim_factors):
            try:
                op_primitive[prim] += val * prim_factor
            except KeyError:
                op_primitive[prim] = val * prim_factor
    return op_primitive


def compute_and_save_kimmy_irreps(Nc, rep, basis, get_W_function,
                                  elemental_states, isospin_rep, unitarize, basis_change):
    elemental_ops = get_irrep_operators(basis, rep, get_W_function, elemental_states, unitarize)
    print(f'num choices = {len(elemental_ops.keys())/REP_DIMS[rep]}')
    print(f'num row choices = {len(elemental_ops.keys())}')
    primitives_ops = {}
    for row_choice, op in elemental_ops.items():
        op_primitive = get_primitive_operator_from_elemental_operator(op, isospin_rep)
        primitives_ops[row_choice] = op_primitive

    base_filename = f'Nc{Nc}_isospin{isospin_rep}'
    os.system(f'mkdir -p Operators/{basis}')
    ops_dict_filename = f'Operators/{basis}/{base_filename}_{rep}_elemental_operator'
    ops_dict_filename_basis_change = f'Operators/{basis_change}/{base_filename}_{rep}_elemental_operator'
    ops_dict_prim_filename = f'Operators/{basis}/{base_filename}_{rep}_primitive_operator'
    if unitarize:
        ops_dict_filename += '_unitarized'
        ops_dict_filename_basis_change += '_unitarized'
        ops_dict_prim_filename += '_unitarized'
    ops_dict_filename += '.pkl'
    ops_dict_filename_basis_change += '.pkl'
    ops_dict_prim_filename += '.pkl'

    with open(ops_dict_prim_filename, 'wb') as f:
        pickle.dump(primitives_ops, f)
    with open(ops_dict_filename, 'wb') as f:
        pickle.dump(elemental_ops, f)
    if basis_change != '':
        os.system(f'mkdir -p Operators/{basis_change}')
        basis_change_ops = {}
        for key, operator in elemental_ops.items():
            basis_change_ops[key] = change_basis(operator, isospin_rep, elemental_states, basis_change)
        with open(ops_dict_filename_basis_change, 'wb') as f:
            pickle.dump(basis_change_ops, f)

    return elemental_ops, primitives_ops


def run_full_irrep(Nc, isospin_rep, rep, basis, unitarize, test_homomorphism,
                   write_pdf, save_kimmy_matrix, printing, basis_change):
    elemental_states, linear_combinations = get_elementals_and_linear_combinations(Nc, isospin_rep)
    print(elemental_states)
    elemental_list, factors = get_linear_combination('0232', elemental_states, isospin_rep, 4)
    print(elemental_list)
    print(factors)


    exit()

    get_W_function = get_W(Nc, elemental_states, linear_combinations)
    print(f'Nc {Nc}, isospin rep {isospin_rep}, rep {rep} basis {basis}')
    # print(f'isospin wavefunction {ISOSPIN_WAVEFUNCTIONS[isospin_rep]}')
    # print('Elemental states')
    # print(elemental_states)
    # print(f'Num elementals: {len(elemental_states)}\n')
    if test_homomorphism:
        test_spinor_octahedral_rotations_W_homomorphism(get_W_function, basis, rep, elemental_states, unitarize)
        return
    kimmy_elemental_ops, kimmy_primitive_ops = compute_and_save_kimmy_irreps(Nc, rep, basis,
                                                                             get_W_function,
                                                                             elemental_states,
                                                                             isospin_rep, unitarize, basis_change)
    if basis == 'Dirac_Euclidean':
        aaron_elemental_ops, aaron_primitive_ops = compute_and_save_aaron_ops(Nc, basis, rep,
                                                                              isospin_rep,
                                                                              elemental_states)
        print('compare kimmy and aaron elemental ops')
        find_kimmy_aaron_linear_combination(kimmy_elemental_ops, aaron_elemental_ops, printing)
        # print('compare kimmy and aaron primitive ops')
        # find_kimmy_aaron_linear_combination(kimmy_primitive_ops, aaron_primitive_ops, printing)

    if write_pdf:
        write_by_rows_latex_pdf(rep, kimmy_elemental_ops, isospin_rep, elemental_states, basis,
                                George_compare=False, basis_change=basis_change, filename=basis_change)
    if save_kimmy_matrix:
        for row_choice, op in kimmy_elemental_ops.items():
            savename = f'Operators/{basis}/gamma_matrix_Nc{Nc}_isospin{isospin_rep}_{rep}_{row_choice}.txt'
            save_matrix_from_dict(op, savename)
    return


def main():
    unitarize = 0
    basis_list = [
        # 'Dirac_Euclidean',
        'DR_Euclidean',        #  QDP basis = DR basis
        # 'Weyl_Euclidean'
    ]
    # basis_change = 'Weyl_to_Dirac'
    basis_change = ''
    for basis in basis_list:
        print(f'\n\n\n ~~~~~~~ !!!!! {basis} with {basis_change} change !!!!! ~~~~~~~~~')
        test_spinor_rotations_unitarity(basis)
        for Nc in [4]:
            if Nc == 3:
                isospin_rep_list = [
                    # '123',
                    # '13_2',
                    '12_3'
                ]
                rep_list = [
                    # 'G1g_rep',
                    'Hg_rep'
                ]
            else:
                isospin_rep_list = [
                    # '1234',
                    # '134_2',
                    # '124_3',
                    # '123_4',
                    # '12_34',
                    '13_24'
                ]
                rep_list = [
                    'A1g_rep',
                    # 'Eg_rep',
                    # 'T1g_rep',
                    # 'T2g_rep',
                ]
            for isospin_rep in isospin_rep_list:
                for rep in rep_list:
                    print('\n\n')
                    run_full_irrep(Nc, isospin_rep, rep, basis, unitarize,
                                   test_homomorphism=0,
                                   write_pdf=1,
                                   save_kimmy_matrix=0,
                                   printing=0,
                                   basis_change=basis_change)


if __name__ == "__main__":
    main()
