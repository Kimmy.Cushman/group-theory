import pickle
from baryon_oo import save_matrix_from_dict


def get_operators_1234_Eg(basis, choice):
    ops = []
    if basis == 'DR_Euclidean':
        if choice == 0:
            ops = {
                2: {'0000': 1, '1111': 1, '2222': 1, '3333': 1},  # Eg
                0: {'0011': 1, '2233': 1},  # Eg
            }
        if choice == 1:
            ops = {
                2: {'0002': 1, '0222': 1, '1113': 1, '1333': 1},
                0: {'0013': 1, '0112': 1, '0233': 1, '1223': 1}
            }
        if choice == 2:
            ops = {
                2: {'0022': 1, '1133': 1},
                0: {'0033': 1, '0123': 4, '1122': 1}
            }

    return ops


def main():
    Nc = 4
    isospin_rep = '1234'
    rep = 'Eg'
    basis = 'DR_Euclidean'
    Sz_list = [2, 0]
    ops_dict = {}
    for choice in [0, 1, 2]:
        ops_dict[choice] = {}
        for Sz in Sz_list:
            ops_dict[choice][Sz] = get_operators_1234_Eg(basis, choice)[Sz]
            filename = f'Operators/{basis}/gamma_matrix_Nc{Nc}_isospin{isospin_rep}_{rep}_Sz{Sz}_choice{choice}.txt'
            save_matrix_from_dict(ops_dict[choice][Sz], filename)
    with open(f'Operators/{basis}/diagonalized_1234_Eg.pkl', 'wb') as f:
        pickle.dump(ops_dict, f)


if __name__ == '__main__':
    main()

